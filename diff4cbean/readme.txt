        /~~~~~~\
      /'    -s- ~~~~\
     /'dHHb      ~~~~
    /'dHHHA     :
   /' VHHHHaadHHb:
  /'   `VHHHHHHHHb:
 /'      `VHHHHHHH:			         _   _                          
/'        dHHHHHHH:		 __| o _|_ _|_  __   __   __  _|_   __  
|        dHHHHHHHH:		(__| |  |   |  |  ' (__( (___  |_, (__) 
|       dHHHHHHHHH:
|       VHHHHHHHHH:
|   b    HHHHHHHHV:
|   Hb   HHHHHHHV'
|   HH  dHHHHHHV'
|   VHbdHHHHHHV'
|    VHHHHHHHV'
 \    VHHHHHHH:
  \oodboooooodH
HHHHHHHHHHHHHHHHHHHHH
                                                              
01/03/2006	Developped by ICA TEAM


DESCRIPTION
-----------
This application is a front end for control & acquisition of Diffractometers

DEPENDENCES
-----------
This source code release is based on :
	* JDK 1.4.2
	* ATKCore-2.5.0.jar
	* ATKWidget-2.5.0.jar
	* TangORB-4.9.0.jar
    * SoleilUtils-1.2.1.jar

The application has dependance with :
	* Device ds_DiffractometerEulerian4Circles 2.0
	* Device ds_DiffractometerDevice 2.0 ( class : Eulerian4C and Kappa6C )
	
PLATEFORM
---------
Any operating system where run a JVM.

WHAT'S INSIDE
-------------

SOURCE : /src
	* source files

DOC : /doc
	TODO
	
	
PREREQUISITES
-------------

Before compiling and installing this application you need to install :
	* see the prerequisites of tango distribution
	* JRE 1.4.2 or more

MAKE
----
TODO ant compilation DONE

INSTALLING
----------
No installation needed.

CONFIGURING
-----------
The devices can be changed into script file.

RUNNING
-------
Windows :
	!!!  not from network (ie "\\...") !!!
	* Click on start-salsa.bat
Linux :
	* Launch start-salsa.sh with or without devices address

QUESTIONS
---------

You will definitely have some ! Send questions to :
	vincent.hardion@synchrotron-soleil.fr

URL
---



CHANGES
-------
	Version 2.1 --> Compatible with Kappa 6 Circles
