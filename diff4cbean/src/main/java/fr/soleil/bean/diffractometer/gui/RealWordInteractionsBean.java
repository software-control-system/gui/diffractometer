/**
 * 
 */
package fr.soleil.bean.diffractometer.gui;

import java.awt.FlowLayout;
import java.util.prefs.Preferences;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.border.TitledBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.bean.diffractometer.model.ContextModel;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.swing.StringButton;

/**
 * @author HARDION
 * 
 */
public class RealWordInteractionsBean extends AbstractTangoBox {

    private static final long serialVersionUID = 3352730994968373874L;

    private static final String PREPARE_PILOT = "Prepare → Pilot";
    private static final String PILOT_PREPARE = "Pilot → Prepare";

    private static final Logger LOGGER = LoggerFactory.getLogger(RealWordInteractionsBean.class);

    private StringButton updateButton = null;
    private JButton realGotoButton;
    private DiffractometerBean parentPanel;
    private final static String UPDATE_FROM_REAL_WORLD = "UpdateFromRealWorld";

    /**
     * 
     */
    public RealWordInteractionsBean(DiffractometerBean parentPanel) {
        super();
        this.parentPanel = parentPanel;
        initialize();
    }

    /**
     * This method initializes this
     * 
     */
    private void initialize() {
        setSize(new java.awt.Dimension(177, 72));
        setBorder(new TitledBorder(null, "Interactions with Pilot device", TitledBorder.DEFAULT_JUSTIFICATION,
                TitledBorder.DEFAULT_POSITION, null, null));
        FlowLayout layout = new FlowLayout();
        setLayout(layout);
        add(getUpdateButton());
        add(getRealGoToButton());
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.soleil.bean.AbstractBean#clearGUI()
     */
    @Override
    protected void clearGUI() {
        cleanWidget(getUpdateButton());
//        cleanWidget(getApplyCalculatedAnglesToRealWorldButton());
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.soleil.bean.AbstractBean#refreshGUI()
     */
    @Override
    protected void refreshGUI() {
        setWidgetModel(getUpdateButton(), stringBox, generateCommandKey(UPDATE_FROM_REAL_WORLD));
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.soleil.bean.AbstractBean#onConnectionError()
     */
    @Override
    protected void onConnectionError() {
        LOGGER.error("Connection error");

    }

    /**
     * This method initializes autoUpdateFromProxiesModeScalarViewer
     * 
     */
    private StringButton getUpdateButton() {
        if (updateButton == null) {
            updateButton = generateStringButton();
            updateButton.setText(PILOT_PREPARE);
        }
        return updateButton;
    }

    /**
     * 
     * @return Button
     */
    private JButton getRealGoToButton() {
        if (realGotoButton == null) {
            realGotoButton = new JButton(PREPARE_PILOT);
            realGotoButton.addActionListener((e) -> {
                JDialog dialog = parentPanel.getRealAxesDialog();
                dialog.setVisible(true);
                dialog.setModal(true);
            });
        }
        return realGotoButton;
    }

    @Override
    protected void loadPreferences(final Preferences arg0) {
        // NOT USE
    }

    @Override
    protected void savePreferences(final Preferences arg0) {
        // NOT USE
    }

    public void updateGUI() {
        if (ContextModel.getInstance().isVirtual()) {
            updateButton.setText("Virtual ← Pilot");
            realGotoButton.setVisible(false);
        }

    }
}