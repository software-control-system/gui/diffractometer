package fr.soleil.bean.diffractometer.model;

import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.DeviceProxyFactory;
import fr.soleil.lib.project.ObjectUtils;

public class DiffractometerModel {

    private static final String[][] NO_DATA = new String[0][0];

    private static final String PSEUDO_AXES_PROXIES = "pseudoAxesProxies";
    private static final String AXES_NAMES = "AxesNames";
    private static final String REAL_AXIS_PROXIES = "RealAxisProxies";
    private static final String DIFFRACTOMETER_TYPE = "DiffractometerType";
    private static final String OPERATING_STATUS = "OperatingStatus";
//    private static final String CRYSTALMANAGER = "CRYSTALMANAGER";
    public static final String PREPARE_STATUS = "PREPARE";
    public static final String PILOT_STATUS = "PILOT";
    public static final String VIRTUAL_STATUS = "VIRTUAL";

    private static final Logger LOGGER = LoggerFactory.getLogger(DiffractometerModel.class);

    private String diffractometer = ObjectUtils.EMPTY_STRING;

    private String[][] realAxis = NO_DATA;

    private String[][] realAxisProxies = NO_DATA;

    private String[][] pseudoAxis = NO_DATA;

    private String[][] eulerianPseudoAxis = NO_DATA;

    private String type = ObjectUtils.EMPTY_STRING;
    private String status = ObjectUtils.EMPTY_STRING;

    public DiffractometerModel() {
        super();

    }

    /**
     * @return Returns the diffractometer.
     */
    public String getDiffractometer() {
        return diffractometer;
    }

    /**
     * @param diffractometer The diffractometer to set.
     * @throws ConnectionException
     */
    public void setDiffractometer(final String diffractometer) {
//        if ((diffractometer != null) && diffractometer.toUpperCase().contains(CRYSTALMANAGER)) {
//            RuntimeException e = new RuntimeException();
//            e.printStackTrace();
//            System.out.println();
//        }
        if (this.diffractometer.isEmpty() || !this.diffractometer.equals(diffractometer)) {
            if (!this.diffractometer.isEmpty()) {
                // Unregistering listener from model
                realAxis = null;
                pseudoAxis = null;
                eulerianPseudoAxis = null;
            }

            this.diffractometer = diffractometer == null ? ObjectUtils.EMPTY_STRING : diffractometer;

            if (!this.diffractometer.isEmpty()) {
                // Registering listener from model
                try {

                    final DeviceProxy diffracto = DeviceProxyFactory.get(this.diffractometer);

                    DeviceAttribute statusAttribute = diffracto.read_attribute(OPERATING_STATUS);
                    if (statusAttribute != null) {
                        status = statusAttribute.extractString();
                    }
                    DbDatum datum = diffracto.get_property(REAL_AXIS_PROXIES);
                    if (datum != null) {
                        final String[] realAxisProxiesValues = datum.extractStringArray();
                        realAxisProxies = new String[realAxisProxiesValues.length][2];
                        for (int i = 0; i < realAxisProxiesValues.length; i++) {
                            final StringTokenizer tokens = new StringTokenizer(realAxisProxiesValues[i], ":");
                            realAxisProxies[i][0] = tokens.nextToken();
                            realAxisProxies[i][1] = tokens.nextToken();
                        }
                    }

                    // set the type of diffractometer to control (== class)
                    // fix #7071
                    statusAttribute = diffracto.read_attribute(DIFFRACTOMETER_TYPE);
                    if (datum != null) {
                        type = datum.extractString();
                    }

                    try {
                        final DeviceAttribute attr = diffracto.read_attribute(AXES_NAMES);
                        final String[] axesNames = attr.extractStringArray();
                        realAxis = new String[axesNames.length][2];
                        for (int i = 0; i < axesNames.length; i++) {
                            // substring "axis" + AxisName
                            if (axesNames[i].length() > 4) {
                                realAxis[i][0] = axesNames[i].substring(4);
                                realAxis[i][1] = axesNames[i];
                            }
                        }
                    } catch (final DevFailed e) {
                        LOGGER.error(DevFailedUtils.toString(e), e);
                    }

                    final DeviceAttribute attr = diffracto.read_attribute(PSEUDO_AXES_PROXIES);
                    pseudoAxis = new String[attr.extractStringArray().length][2];

                    for (int i = 0; i < pseudoAxis.length; i++) {
                        pseudoAxis[i][0] = "Pseudo " + i;
                        pseudoAxis[i][1] = attr.extractStringArray()[i];
                    }

                } catch (final DevFailed e) {
                    LOGGER.error(DevFailedUtils.toString(e), e);
                }
            } // end if (!this.diffractometer.isEmpty())
        } // end if (this.diffractometer.isEmpty() || !this.diffractometer.equals(diffractometer))
    }

    /**
     * @return the realAxis
     */
    public String[][] getRealAxis() {
        return realAxis;
    }

    /**
     * @param realAxis the realAxis to set
     */
    public void setRealAxis(final String[][] proxies) {
        this.realAxis = proxies;
    }

    /**
     * @return the pseudoAxis
     */
    public String[][] getPseudoAxis() {
        return pseudoAxis;
    }

    /**
     * @return the eulerianPseudoAxis
     */
    public String[][] getEulerianPseudoAxis() {
        return eulerianPseudoAxis;
    }

    /**
     * @param pseudoAxis the pseudoAxis to set
     */
    public void setPseudoAxis(final String[][] pseudoAxis) {
        this.pseudoAxis = pseudoAxis;
    }

    /**
     * 
     * @return String[][]
     */
    public String[][] getRealAxisProxies() {
        return realAxisProxies;
    }

    /**
     * 
     * @param realAxisProxies
     */
    public void setRealAxisProxies(final String[][] realAxisProxies) {
        this.realAxisProxies = realAxisProxies;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isVirtual() {
        return VIRTUAL_STATUS.equals(status);
    }
}