package fr.soleil.bean.diffractometer.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.prefs.Preferences;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import fr.soleil.bean.diffractometer.gui.util.AutoScrolledFormattedTextField;
import fr.soleil.bean.diffractometer.gui.util.FormattedLabel;
import fr.soleil.bean.diffractometer.gui.util.TangoConstants;
import fr.soleil.bean.diffractometer.model.AxesFormat;
import fr.soleil.bean.motor.UserMotorBean;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.AutoScrolledTextField;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.scalar.INumberTarget;

/**
 * @author ADIOUF
 */
public class ReachTargetBean extends AbstractTangoBox implements TangoConstants {

    private static final long serialVersionUID = 2827325298651014413L;

    public final static int REAL_AXES_TYPE = 1;
    public final static int PSEUDO_AXES_TYPE = 2;

    private FormattedLabel pViewers = null;
    private AutoScrolledTextField pViewersTextField = null;
    private double pViewersLabelValues = 0;
    private Label labels = null;
    private JPanel panel = null;
    private JPanel panelMotor = null;
    private UserMotorBean motorBean = null;
    private final Label errorLabel = new Label();
    private String targetlabel = "";
    private int type = 0;
    private String motorBeanModel = "";
    private String attribute = null;

    // private final NumberScalarBox numberBox = new NumberScalarBox();

    /**
     * This is the default constructor
     */
    public ReachTargetBean() {
        super();
        this.initialize();
    }

    /**
     * This method initializes this
     * 
     * @return void
     */
    private void initialize() {
        this.setLayout(new GridBagLayout());
        final GridBagConstraints fullConstraints = new GridBagConstraints();
        fullConstraints.fill = GridBagConstraints.BOTH;
        fullConstraints.gridx = 0;
        fullConstraints.gridy = 0;
        fullConstraints.weightx = 1;
        fullConstraints.weighty = 1;
        this.add(this.errorLabel, fullConstraints);
        this.errorLabel.setText("Problem with Axes Attributes");
    }

    @Override
    protected void clearGUI() {
        this.removeAll();
        this.cleanWidget(this.pViewersTextField);
        if (this.pViewers != null) {
            this.cleanWidget(this.pViewers);
            this.pViewers = null;
        }
    }

    @Override
    protected void refreshGUI() {
        this.remove(this.errorLabel);

        this.motorBean = new UserMotorBean(true);
        this.motorBean.setModel(this.motorBeanModel);

        this.motorBean.start();

        this.panel = new JPanel();
        this.panel.setLayout(new GridBagLayout());

        this.labels = new Label();
        this.labels.setHorizontalAlignment(JLabel.CENTER);
        this.labels.setText(this.getTargetlabel());

        final GridBagConstraints viewerPanelConstraints1 = new GridBagConstraints();
        viewerPanelConstraints1.fill = GridBagConstraints.BOTH;
        viewerPanelConstraints1.gridx = 0;
        viewerPanelConstraints1.gridy = 1;
        viewerPanelConstraints1.weightx = 1;
        this.panel.add(this.labels, viewerPanelConstraints1);

        if (this.type == REAL_AXES_TYPE) {
            this.pViewers = new FormattedLabel();
            this.pViewers.setFormat(AxesFormat.getInstance().getFormat(targetlabel));
            this.pViewers.setPreferredSize(new Dimension(60, 60));
            this.pViewers.setMinimumSize(new Dimension(60, 60));
            this.pViewers.setHorizontalTextPosition(SwingConstants.CENTER);
            this.pViewers.setToolTipText("Target Motor Position Value");

            final TangoKey stringScalarKey = new TangoKey();
            TangoKeyTool.registerAttribute(stringScalarKey,
                    this.attribute.substring(0, this.attribute.lastIndexOf(TANGO_SEPARATOR)),
                    this.attribute.substring(this.attribute.lastIndexOf(TANGO_SEPARATOR) + 1));
            this.setWidgetModel(this.pViewers, this.stringBox, stringScalarKey);

            final INumberTarget positionTarget = new INumberTarget() {
                double old = 0;

                @Override
                public void removeMediator(final Mediator<?> mediator) {
                    // not use

                }

                @Override
                public void addMediator(final Mediator<?> mediator) {
                    // not use

                }

                @Override
                public void setNumberValue(final Number value) {
                    final double valueD = value.doubleValue();
                    if (valueD != this.old) {
                        ReachTargetBean.this.motorBean.setTargetValue(valueD);
                        this.old = valueD;
                    }

                }

                @Override
                public Number getNumberValue() {
                    return this.old;
                }
            };
            final TangoKey positionKey = this
                    .generateAttributeKey(this.attribute.substring(this.attribute.lastIndexOf(TANGO_SEPARATOR) + 1));
            this.setWidgetModel(positionTarget, this.numberBox, positionKey);

        }

        if (this.type == PSEUDO_AXES_TYPE) {
            this.getPViewersTextField();
            this.motorBean.setTargetValue(this.pViewersLabelValues);
        }

        final GridBagConstraints viewerPanelConstraints2 = new GridBagConstraints();
        viewerPanelConstraints2.fill = GridBagConstraints.BOTH;
        viewerPanelConstraints2.gridx = 0;
        viewerPanelConstraints2.gridy = 2;
        viewerPanelConstraints2.weightx = 1;

        if (this.type == REAL_AXES_TYPE) {
            this.panel.add(this.pViewers, viewerPanelConstraints2);
        }

        if (this.type == PSEUDO_AXES_TYPE) {
            this.panel.add(this.getPViewersTextField(), viewerPanelConstraints2);
        }

        final GridBagConstraints viewer1Constraints = new GridBagConstraints();
        viewer1Constraints.fill = GridBagConstraints.BOTH;
        viewer1Constraints.gridx = 0;
        viewer1Constraints.gridy = 1;
        viewer1Constraints.weightx = 1;
        viewer1Constraints.insets = new Insets(0, 0, 2, 0);
        this.add(this.panel, viewer1Constraints);

        this.panelMotor = new JPanel();
        this.panelMotor.add(new JButton("panelMotor"));
        final GridBagConstraints panelMotorConstraints = new GridBagConstraints();
        panelMotorConstraints.fill = GridBagConstraints.BOTH;
        panelMotorConstraints.gridx = 0;
        panelMotorConstraints.gridy = 2;
        panelMotorConstraints.weightx = 1;
        panelMotorConstraints.insets = new Insets(0, 0, 2, 0);
        this.add(this.motorBean, panelMotorConstraints);
    }

    public AutoScrolledTextField getPViewersTextField() {
        if (this.pViewersTextField == null) {
            this.pViewersTextField = new AutoScrolledFormattedTextField();
            this.pViewersTextField.setCometeFont(new CometeFont("Dialog", Font.PLAIN, 16));
            this.pViewersTextField.setPreferredSize(new Dimension(60, 60));
            this.pViewersTextField.setMinimumSize(new Dimension(60, 60));
            ((AutoScrolledFormattedTextField) this.pViewersTextField)
                    .setFormat(AxesFormat.getInstance().getFormat(this.targetlabel));
            this.pViewersTextField.setToolTipText("Target Motor Position Value");
            this.pViewersTextField.setBackground(Color.GREEN);
        }
        return this.pViewersTextField;
    }

    public String getMotorBeanModel() {
        return this.motorBeanModel;
    }

    public void setMotorBeanModel(final String motorBeanModel) {
        this.motorBeanModel = motorBeanModel;
    }

    public String getTargetlabel() {
        return this.targetlabel;
    }

    public void setTagetlabel(final String tagetlabel) {
        this.targetlabel = tagetlabel;
    }

    public int getType() {
        return this.type;
    }

    public void setType(final int type) {
        this.type = type;
    }

    public double getPViewersLabelValues() {
        return this.pViewersLabelValues;
    }

    public void setPViewersLabelValues(final double viewersLabelValues) {
        this.pViewersLabelValues = viewersLabelValues;
        this.getPViewersTextField().setText("" + viewersLabelValues);
        this.motorBean.setTargetValue(viewersLabelValues);
    }

    /**
     * 
     * @return UserMotorBean
     */
    public UserMotorBean getMotorBean() {
        return this.motorBean;
    }

    @Override
    protected void onConnectionError() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void loadPreferences(final Preferences arg0) {
        // NOT USE
    }

    @Override
    protected void savePreferences(final Preferences arg0) {
        // NOT USE
    }

    public void setModelAttribute(final String attribute) {
        this.attribute = attribute;
    }

    public String getModelAttribute() {
        return this.attribute;
    }
}
