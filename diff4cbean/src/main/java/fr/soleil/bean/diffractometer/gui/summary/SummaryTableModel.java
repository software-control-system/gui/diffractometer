package fr.soleil.bean.diffractometer.gui.summary;

import java.awt.Color;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.table.AbstractTableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SummaryTableModel extends AbstractTableModel {

    private static final long serialVersionUID = 8055782765811046585L;

    public static final String NAME = "Name";
    public static final String ENGINE = "Engine";
    public static final String PILOT = "Pilot";
    public static final String FORMAT = "Format";
    public static final int ENGINE_INDEX = 0;
    public static final int NAME_INDEX = 1;
    public static final int PILOT_INDEX = 2;
    public static final int PREPARE_READ_INDEX = 3;
    public static final int PREPARE_WRITE_INDEX = 4;
    public static final int FORMAT_INDEX = 5;

    private static final Logger LOGGER = LoggerFactory.getLogger(SummaryTableModel.class);

    private String[] columnNames;
    private final List<String> names = new LinkedList<String>();
    private final List<String> engines = new LinkedList<String>();
    private final List<String> formats = new LinkedList<String>();
    private final Map<String, Integer> indexes = new HashMap<String, Integer>();
    private final List<Double[]> values = new LinkedList<Double[]>();
    private final List<Color> colors = new LinkedList<Color>();

    public SummaryTableModel(String diffractoMode) {
        super();
        columnNames = new String[] { ENGINE, NAME, PILOT, diffractoMode + " read", diffractoMode + " write", FORMAT };
    }

    public void addRow(final Color color, final String engine, final String name, final Double realValue,
            final Double readSim, final Double writeSim, final String format) {
        colors.add(color);
        engines.add(engine);
        names.add(name);
        indexes.put(engine + name, values.size());
        final Double[] value = new Double[3];
        value[0] = realValue;
        value[1] = readSim;
        value[2] = writeSim;
        values.add(value);
        formats.add(format);
        final int rowAdded = names.size() - 1;
        fireTableRowsInserted(rowAdded, rowAdded);
    }

    public Color getColorFor(int rowIndex) {
        return colors.get(rowIndex);
    }

    public String getFormatFor(int rowIndex) {
        return formats.get(rowIndex);
    }

    public int indexOf(final String engine, final String name) {
        Integer result = indexes.get(engine + name);
        if (result == null) {
            return -1;
        } else {
            return result.intValue();
        }
    }

    @Override
    public int getRowCount() {
        return names.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(final int column) {
        return columnNames[column];
    }

    @Override
    public Class<?> getColumnClass(final int columnIndex) {
        switch (columnIndex) {
            case ENGINE_INDEX:
                return String.class;
            case NAME_INDEX:
                return String.class;
            case PILOT_INDEX:
            case PREPARE_READ_INDEX:
            case PREPARE_WRITE_INDEX:
                return Double.class;
            case FORMAT_INDEX:
                return String.class;
            default:
                LOGGER.error("Unknown column");
                return null;
        }
    }

    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
        switch (columnIndex) {
            case ENGINE_INDEX:
                return engines.get(rowIndex);
            case NAME_INDEX:
                return names.get(rowIndex);
            case PILOT_INDEX:
            case PREPARE_READ_INDEX:
            case PREPARE_WRITE_INDEX:
                return values.get(rowIndex)[columnIndex - 2];
            case FORMAT_INDEX:
                return getFormatFor(rowIndex);
            default:
                LOGGER.error("Unknown cell");
                return null;
        }
    }

    @Override
    public void setValueAt(final Object aValue, final int rowIndex, final int columnIndex) {
        switch (columnIndex) {
            case ENGINE_INDEX:
                break;
            case NAME_INDEX:
                break;
            case PILOT_INDEX:
            case PREPARE_READ_INDEX:
            case PREPARE_WRITE_INDEX:
                final Double[] value = values.remove(rowIndex);
                value[columnIndex - 2] = (Double) aValue;
                values.add(rowIndex, value);
                fireTableRowsUpdated(rowIndex, rowIndex);
                break;
            default:
                break;
        }
    }

    @Override
    public boolean isCellEditable(final int rowIndex, final int columnIndex) {
        return getColumnName(columnIndex).contains("write");
    }
}