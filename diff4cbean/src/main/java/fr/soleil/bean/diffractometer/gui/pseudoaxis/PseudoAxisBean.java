/**
 * 
 */

package fr.soleil.bean.diffractometer.gui.pseudoaxis;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.prefs.Preferences;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.DeviceProxyFactory;
import fr.soleil.bean.diffractometer.model.ContextModel;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.scalarbox.NumberScalarBox;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.WheelSwitch;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.scalar.IBooleanTarget;

/**
 * 
 * @author ADIOUF
 * 
 */
public class PseudoAxisBean extends AbstractTangoBox {

    private static final long serialVersionUID = 6801503880851716606L;

    private static final String ATTRIBUTE_PSEUDO_AXIS_NAMES = "pseudoAxisNames";

    private static final Logger LOGGER = LoggerFactory.getLogger(PseudoAxisBean.class);

    private Label[] pViewers = null;
    private Label[] pViewersPseudo = null;
    private WheelSwitch[] pEditors = null;
    private final Label errorLabel = new Label();
    private ModeBean modeBean = null;
    private JPanel pseudoBeans = null;
    private String[] pseudoAxisNames;
    private final NumberScalarBox numberBox = new NumberScalarBox();
    final JPanel frozenPanel = new JPanel();

    /**
     * 
     * This is the default constructor
     */
    public PseudoAxisBean() {
        super();
        initialize();
    }

    /**
     * This method initializes this
     * 
     * @return void
     */
    private void initialize() {
        setLayout(new GridBagLayout());

        final GridBagConstraints fullConstraints = new GridBagConstraints();
        fullConstraints.fill = GridBagConstraints.BOTH;
        fullConstraints.gridx = 0;
        fullConstraints.gridy = 0;
        fullConstraints.weightx = 1;
        fullConstraints.weighty = 1;
        add(errorLabel, fullConstraints);
        errorLabel.setText("Problem with Axes Attributes");
    }

    @Override
    protected void clearGUI() {
        removeAll();
        if (pViewers != null) {
            for (int i = 0; i < pViewers.length; i++) {
                if (pViewers[i] != null) {
                    cleanWidget(pViewers[i]);
                }
            }
            pViewers = null;
        }

        if (pViewersPseudo != null) {
            for (int i = 0; i < pViewersPseudo.length; i++) {
                if (pViewersPseudo[i] != null) {
                    cleanWidget(pViewersPseudo[i]);
                }
            }
            pViewersPseudo = null;
        }

        if (pEditors != null) {
            for (int i = 0; i < pEditors.length; i++) {
                if (pEditors[i] != null) {
                    cleanWidget(pEditors[i]);
                }
            }
            pEditors = null;
        }
    }

    private IBooleanTarget generateVirtualBooleanTarget() {
        IBooleanTarget result = new IBooleanTarget() {

            @Override
            public void addMediator(Mediator<?> arg0) {
            }

            @Override
            public void removeMediator(Mediator<?> arg0) {
            }

            @Override
            public boolean isSelected() {
                return true;
            }

            @Override
            public void setSelected(boolean arg0) {
                if (arg0) {
                    frozenPanel.add(new JLabel(ContextModel.getRealDiffractometer() + " is frozen"));
                } else {
                    frozenPanel.removeAll();
                }
            }

        };
        return result;

    }

    @Override
    protected void refreshGUI() {
        final String deviceName = getModel();
        String name = ContextModel.getRealDiffractometer();

        TangoKey freezeKey = new TangoKey();
        TangoKeyTool.registerAttribute(freezeKey, name, "freeze");
        IBooleanTarget virtualTarget = generateVirtualBooleanTarget();
        booleanBox.connectWidget(virtualTarget, freezeKey);

        remove(errorLabel);
        DeviceProxy dp = null;
        try {
            dp = DeviceProxyFactory.get(getModel());
            final DeviceAttribute attr = dp.read_attribute(ATTRIBUTE_PSEUDO_AXIS_NAMES);
            pseudoAxisNames = attr.extractStringArray();
            pViewers = new Label[pseudoAxisNames.length];
            pViewersPseudo = new Label[pseudoAxisNames.length];
            pEditors = new WheelSwitch[pseudoAxisNames.length];
            final Label[] labels = new Label[pseudoAxisNames.length];
            String deviceTitle;
            Border titleBorder;
            try {
                deviceTitle = ApiUtil.get_db_obj().get_alias_from_device(deviceName);

            } catch (final DevFailed e) {
                deviceTitle = deviceName;
            }

            titleBorder = new TitledBorder(BorderFactory.createLineBorder(Color.BLACK), deviceTitle, TitledBorder.LEFT,
                    TitledBorder.TOP, new Font("Dialog", 1, 12), new Color(0, 0, 0));

            setBorder(titleBorder);

            getPseudoBeans().removeAll();
            if (pseudoAxisNames != null) {

                for (int i = 0; i < pseudoAxisNames.length; i++) {
                    String pseudoAxisName = pseudoAxisNames[i];
                    Label label = new Label();
                    labels[i] = label;
                    label.setHorizontalAlignment(JLabel.LEFT);
                    label.setText(pseudoAxisName + " ");

                    final Dimension dimensionlabels = new Dimension(10, 10);
                    label.setSize(dimensionlabels);
                    label.setPreferredSize(dimensionlabels);
                    label.setMinimumSize(dimensionlabels);

                    Label viewer = generateLabel();
                    pViewers[i] = viewer;
                    final TangoKey tangoKeyLabel = generateAttributeKey(pseudoAxisName);
                    setWidgetModel(viewer, stringBox, tangoKeyLabel);

                    final Dimension dimensionpViewers = new Dimension(60, 10);
                    viewer.setSize(dimensionpViewers);
                    viewer.setPreferredSize(dimensionpViewers);
                    viewer.setMinimumSize(dimensionpViewers);

                    WheelSwitch editor = new WheelSwitch();
                    pEditors[i] = editor;
                    editor.setBackground(getBackground());
                    final TangoKey tangoKeyEditor = generateWriteAttributeKey(pseudoAxisName);
                    setWidgetModel(editor, numberBox, tangoKeyEditor);

                    final Dimension dimensionpEditors = new Dimension(60, 10);
                    editor.setSize(dimensionpEditors);
                    editor.setPreferredSize(dimensionpEditors);
                    editor.setMinimumSize(dimensionpEditors);

                    final JPanel panel = new JPanel();
                    panel.setLayout(new GridLayout(1, 3));
                    panel.add(label);
                    panel.add(viewer);
                    panel.add(editor);

                    final GridBagConstraints panelConstraints = new GridBagConstraints();
                    panelConstraints.fill = GridBagConstraints.BOTH;
                    panelConstraints.gridx = 0;
                    panelConstraints.gridy = i + 1;
                    panelConstraints.weightx = 1.0d / 3.0d;
                    panelConstraints.weighty = 0.4;
                    panelConstraints.insets = new Insets(2, 20, 2, 0);
                    add(panel, panelConstraints);
                }

                final GridBagConstraints modeBeanConstraints = new GridBagConstraints();
                modeBeanConstraints.fill = GridBagConstraints.BOTH;
                modeBeanConstraints.gridx = 0;
                modeBeanConstraints.gridy = pseudoAxisNames.length + 1;
                modeBeanConstraints.gridwidth = pseudoAxisNames.length;
                modeBeanConstraints.weightx = 1.0d / 3.0d;
                modeBeanConstraints.weighty = 1;

                final GridBagConstraints frozenPanelConstraints = new GridBagConstraints();
                frozenPanelConstraints.fill = GridBagConstraints.BOTH;
                frozenPanelConstraints.gridx = 0;
                frozenPanelConstraints.gridy = 0;
//                modeBeanConstraints.gridwidth = pseudoAxisNames.length;
                frozenPanelConstraints.weightx = 1.0d / 3.0d;
                frozenPanelConstraints.weighty = 1;
                add(frozenPanel, frozenPanelConstraints);

                // modeBeanConstraints.insets = new Insets(2, 20, 2, 0);

                add(getModeBean(), modeBeanConstraints);

                modeBean.setModel(deviceName);
                modeBean.start();
            }

            if (getComponentCount() == 0) {
                final GridBagConstraints fullConstraints = new GridBagConstraints();
                fullConstraints.fill = GridBagConstraints.BOTH;
                fullConstraints.gridx = 0;
                fullConstraints.gridy = 0;
                fullConstraints.weightx = 1;
                fullConstraints.weighty = 1;
                add(errorLabel, fullConstraints);
            }
        } catch (final DevFailed e1) {
            LOGGER.error(DevFailedUtils.toString(e1), e1);
        } catch (final Throwable t) {
            LOGGER.error("error", t);
        }
    }

    public JPanel getPseudoBeans() {
        if (pseudoBeans == null) {
            pseudoBeans = new JPanel();
            pseudoBeans.setLayout(new GridBagLayout());
        }
        return pseudoBeans;
    }

    @Override
    protected void onConnectionError() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * This method initializes modeBean
     * 
     * @return fr.soleil.bean.diffractometer.ModeBean
     */
    private ModeBean getModeBean() {
        if (modeBean == null) {
            modeBean = new ModeBean();
            // final Dimension preferredSize = new Dimension(320, 100);
            // modeBean.setSize(preferredSize);
            // modeBean.setPreferredSize(preferredSize);
        }
        return modeBean;
    }

    @Override
    protected void loadPreferences(final Preferences arg0) {
        // NOT USE
    }

    @Override
    protected void savePreferences(final Preferences arg0) {
        // NOT USE
    }
}
