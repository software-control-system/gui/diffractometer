/*
 * Created on 10 juin 2005
 * with Eclipse
 */
package fr.soleil.bean.diffractometer.gui.action;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.DeviceProxyFactory;
import fr.soleil.comete.swing.Label;

public class UseCrystalAction extends AbstractAction {

    private static final long serialVersionUID = -1311059361707575335L;

    private static final Logger LOGGER = LoggerFactory.getLogger(UseCrystalAction.class);

    private DeviceProxy realDiffractometerProxy = null;
    private DeviceProxy crystalManagerProxy = null;
    private Label realCrystalLabel;

    /**
     * @throws DevFailed
     * 
     */
    public UseCrystalAction(String realModel, String crystalManagerModel, Label realCrystalLabel) {
        super();
        try {

            this.realDiffractometerProxy = DeviceProxyFactory.get(realModel);
            this.crystalManagerProxy = DeviceProxyFactory.get(crystalManagerModel);
            this.realCrystalLabel = realCrystalLabel;
            // This is an instance initializer; it is executed just after the
            // constructor of the superclass is invoked

            // The following values are completely optional
            this.putValue(Action.NAME, "Use Crystal in Pilot");
            // Set tool tip text
            this.putValue(Action.SHORT_DESCRIPTION, "Use Crystal");

            // This text is not directly used by any Swing component;
            // however, this text could be used in a help system
            this.putValue(Action.LONG_DESCRIPTION, "Use Crystal");
        } catch (DevFailed e) {
            JOptionPane.showMessageDialog(null, DevFailedUtils.toString(e), "Use Crystal", JOptionPane.WARNING_MESSAGE);
        }
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        if (this.realDiffractometerProxy != null) {

            try {
                DeviceAttribute currentCrystalAttr = crystalManagerProxy.read_attribute("Crystal");
                String crystal = currentCrystalAttr.extractString();
                DeviceAttribute crystalForPilot = realDiffractometerProxy.read_attribute("Crystal");
                crystalForPilot.insert(crystal);
                realDiffractometerProxy.write_attribute(crystalForPilot);
                realCrystalLabel.setBackground(Color.GREEN);
            } catch (DevFailed e1) {
                LOGGER.error(DevFailedUtils.toString(e1), e1);
                JOptionPane.showMessageDialog((Component) e.getSource(), DevFailedUtils.toString(e1), "Use Crystal",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }
}