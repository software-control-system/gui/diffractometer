package fr.soleil.bean.diffractometer.gui.pseudoaxis;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.prefs.Preferences;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.DeviceProxyFactory;
import fr.soleil.bean.diffractometer.model.ContextModel;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.scalarbox.BooleanScalarBox;
import fr.soleil.comete.swing.CheckBox;
import fr.soleil.comete.swing.ComboBox;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.WheelSwitch;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.IKey;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.scalar.IBooleanTarget;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.file.BatchExecutor;

/**
 * This bean represents the configuration of calculation mode for a given diffractometer
 * 
 * @author HARDION
 * 
 */
public class ModeBean extends AbstractTangoBox {

    private static final long serialVersionUID = -2939447031977783560L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ModeBean.class);

    private static final String ATTRIBUTE_PARAMETERS_NAMES = "parametersNames";
    private static final String ATTRIBUTE_MODE_UPDATE = "ModeUpdate";
    private static final String ATTRIBUTE_FREEZE = "freeze";
    private static final String ATTRIBUTE_MODE = "mode";
    private static final String ATTRIBUTE_MODE_NAMES = "modeNames";
    private static final String ATK_TEXT = "AtkPanel";
    private static final String MODE_INIT = "ModeInit";

//    private int index = 0;
    private ComboBox modeComboEditor = null;
    private JPanel northPanel = null;
    private String[] parameterNames;
    private final BooleanScalarBox booleanBox = new BooleanScalarBox();
    private CheckBox enableModeUpdateCheckBox = null;
    private CheckBox freezeCheckBox = null;
    private JPanel parametersPanel = null;
    private List<WheelSwitch> parametersEditor = null;
    private List<ITarget> widgetList = null;
    private String executedBatchFile;
    private JButton controlPanelButton;
    private JButton modeInitButton;
    private BatchExecutor batchExecutor;

    public ModeBean() {
        super();
        batchExecutor = new BatchExecutor("atkpanel");
        initialize();
    }

    /**
     * This method initializes this
     * 
     */
    private void initialize() {
        widgetList = new ArrayList<>();
        parametersEditor = new ArrayList<>();
        setLayout(new BorderLayout());
        add(getNorthPanel(), BorderLayout.NORTH);
        add(getParametersPanel(), BorderLayout.CENTER);

    }

    private JPanel getParametersPanel() {
        if (parametersPanel == null) {
            parametersPanel = new JPanel(new GridBagLayout());
            parametersPanel.setVisible(true);
        }
        return parametersPanel;
    }

    private JButton getModeInit() {
        if (modeInitButton == null) {
            modeInitButton = new JButton();
            modeInitButton.setText(MODE_INIT);
            // modeInitButton.setPreferredSize(new Dimension(50, 25));
            modeInitButton.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    DeviceProxy proxy;
                    try {
                        proxy = DeviceProxyFactory.get(getModel());
                        proxy.command_inout("ModeInit");

                    } catch (DevFailed e1) {
                        LOGGER.error(DevFailedUtils.toString(e1), e1);
                        JOptionPane.showMessageDialog((Component) e.getSource(), DevFailedUtils.toString(e1),
                                "pseudoaxis error", JOptionPane.WARNING_MESSAGE);
                    }

                }
            });
        }
        return modeInitButton;
    }

    /****************** ATK ***********************/

    private JButton getAtkButton() {
        if (controlPanelButton == null) {
            controlPanelButton = new JButton(ATK_TEXT);
            controlPanelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    controlPanelActionPerformed();
                }

                private void controlPanelActionPerformed() {
                    batchExecutor.execute();

                }
            });
        }
        return controlPanelButton;
    }

    public String getExecutedBatchFile() {
        return executedBatchFile;
    }

    public void setExecutedBatchFile(String executedBatchFile) {
        this.executedBatchFile = executedBatchFile;
        String batch = executedBatchFile;
        if (batch != null) {
            if (batch.endsWith("{DEVICE}")) {
                executedBatchFile = batch.substring(0, batch.lastIndexOf("{DEVICE}"));
            }
            if (!batch.trim().isEmpty()) {
                batchExecutor.setBatch(executedBatchFile);
            }
        }
        updateControlPanelButtonVisibility();
    }

    private void updateControlPanelButtonVisibility() {
        getAtkButton().setVisible((executedBatchFile != null) && (!executedBatchFile.trim().isEmpty()));
    }

    /*************************************************/

    @Override
    protected void clearGUI() {
        cleanWidget(modeComboEditor);
        cleanWidget(freezeCheckBox);
        modeComboEditor = null;
    }

    @Override
    protected void refreshGUI() {
        String[] modeList = {};
        DeviceProxy tmpDeviceProxy = null;
        try {
            tmpDeviceProxy = DeviceProxyFactory.get(model);
            final DeviceAttribute dd = tmpDeviceProxy.read_attribute(ATTRIBUTE_MODE_NAMES);
            modeList = dd.extractStringArray();
        } catch (final DevFailed e) {
            LOGGER.error(DevFailedUtils.toString(e));
        }
        batchExecutor.setBatchParameters(Arrays.asList(getModel()));
        modeComboEditor.setValueList((Object[]) modeList);
        modeComboEditor.setSelectedIndex(-1);

        IKey attributeKey = generateAttributeKey(ATTRIBUTE_MODE);
        // register a target to the same attribute to listen for changes
        final ITextTarget acquisitionModeTarget = new ITextTarget() {
            String old = ObjectUtils.EMPTY_STRING;

            @Override
            public void setText(final String text) {
                if (old == null) {
                    old = ObjectUtils.EMPTY_STRING;
                }

                if (text != null && !old.equals(text)) {
                    updateTableVisibility(text);
                    old = text;
                }
            }

            @Override
            public void removeMediator(final Mediator<?> mediator) {
            }

            @Override
            public void addMediator(final Mediator<?> mediator) {
            }

            @Override
            public String getText() {
                return old;
            }
        };
        setWidgetModel(acquisitionModeTarget, stringBox, attributeKey);

        TangoKey tangoKey = generateAttributeKey(ATTRIBUTE_FREEZE);
        setWidgetModel(getFreezeCheckBox(), booleanBox, tangoKey);
        booleanBox.setColorEnabled(getFreezeCheckBox(), false);

        tangoKey = generateAttributeKey(ATTRIBUTE_MODE_UPDATE);
        booleanBox.setColorEnabled(getEnableUpdateCheckBox(), false);
        setWidgetModel(getEnableUpdateCheckBox(), booleanBox, tangoKey);
        setWidgetModel(generateVirtualBooleanTarget(), booleanBox, tangoKey);
    }

    @Override
    protected void onConnectionError() {
        throw new UnsupportedOperationException("Not supported yet");
    }

    private void updateTableVisibility(String mode) {
        DeviceData data;
        try {
            // ComboBox
            modeComboEditor.setSelectedItem(mode);

            // Description
            data = new DeviceData();

            if (mode != null) {
                data.insert(mode);
                final DeviceProxy dp = DeviceProxyFactory.get(model);
                final DeviceAttribute parametersNames = dp.read_attribute(ATTRIBUTE_PARAMETERS_NAMES);
                parameterNames = parametersNames.getType() != 0 ? parametersNames.extractStringArray() : new String[0];

                // Before panel creation
                // 1) Remove all old components
                getParametersPanel().removeAll();
                parametersEditor.clear();
                // 2) Disconnect them from the old key
                for (Iterator<ITarget> iterator = widgetList.iterator(); iterator.hasNext();) {
                    ITarget iTarget = iterator.next();
                    cleanWidget(iTarget);
                    iterator.remove();
                }

                for (int i = 0; i < parameterNames.length; i++) {
                    String paramName = parameterNames[i];
                    String paramNameToUse = "p_" + mode + "." + paramName;

                    Label label = new Label();

                    label.setHorizontalAlignment(JLabel.LEFT);
                    label.setText(paramNameToUse);

                    final Dimension dimensionlabels = new Dimension(200, 20);
                    label.setSize(dimensionlabels);
                    label.setPreferredSize(dimensionlabels);
                    label.setMinimumSize(dimensionlabels);

                    Label viewer = generateLabel();
                    final TangoKey tangoKeyLabel = generateAttributeKey(paramNameToUse);
                    setWidgetModel(viewer, stringBox, tangoKeyLabel);

                    final Dimension dimensionpViewers = new Dimension(60, 20);
                    viewer.setSize(dimensionpViewers);
                    viewer.setPreferredSize(dimensionpViewers);
                    viewer.setMinimumSize(dimensionpViewers);

                    WheelSwitch editor = new WheelSwitch();
                    editor.setBackground(getBackground());
                    final TangoKey tangoKeyEditor = generateWriteAttributeKey(paramNameToUse);
                    setWidgetModel(editor, numberBox, tangoKeyEditor);

                    final Dimension dimensionpEditors = new Dimension(60, 20);
                    editor.setSize(dimensionpEditors);
                    editor.setPreferredSize(dimensionpEditors);
                    editor.setMinimumSize(dimensionpEditors);
                    JPanel singleParamPanel = new JPanel(new FlowLayout());
                    singleParamPanel.add(label);
                    singleParamPanel.add(viewer);
                    singleParamPanel.add(editor);
                    widgetList.add(label);
                    widgetList.add(viewer);
                    widgetList.add(editor);
                    final GridBagConstraints panelConstraints = new GridBagConstraints();
                    panelConstraints.fill = GridBagConstraints.BOTH;
                    panelConstraints.gridx = 0;
                    panelConstraints.gridy = i;
                    panelConstraints.weightx = 1.0d / 3.0d;
                    panelConstraints.weighty = 1;
                    // panelConstraints.ipady = 50;
                    panelConstraints.insets = new Insets(2, 20, 2, 0);
                    getParametersPanel().add(singleParamPanel, panelConstraints);
                    parametersEditor.add(editor);

                }
            }
            revalidate();
        } catch (final DevFailed e1) {
            LOGGER.error(DevFailedUtils.toString(e1), e1);
        }

    }

    private IBooleanTarget generateVirtualBooleanTarget() {
        IBooleanTarget result = new IBooleanTarget() {
            @Override
            public void addMediator(Mediator<?> arg0) {
            }

            @Override
            public void removeMediator(Mediator<?> arg0) {
            }

            @Override
            public boolean isSelected() {
                return false;
            }

            @Override
            public void setSelected(boolean arg0) {
                if (!ContextModel.getInstance().isVirtual()) {
                    modeComboEditor.setEnabled(arg0);
                    for (WheelSwitch editor : parametersEditor) {
                        editor.setEnabled(arg0);
                    }
                }
            }

        };
        return result;
    }

    private CheckBox getEnableUpdateCheckBox() {
        if (enableModeUpdateCheckBox == null) {
            enableModeUpdateCheckBox = new CheckBox();
        }
        return enableModeUpdateCheckBox;
    }

    private CheckBox getFreezeCheckBox() {
        if (freezeCheckBox == null) {
            freezeCheckBox = new CheckBox();
        }
        return freezeCheckBox;
    }

    /**
     * This method initializes modeComboEditor
     * 
     * @return fr.esrf.tangoatk.widget.attribute.StringScalarComboEditor
     */
    private ComboBox getModeComboEditor() {
        if (modeComboEditor == null) {
            modeComboEditor = new ComboBox();

            modeComboEditor.addItemListener(new ItemListener() {
                // boolean initialized = false;
                String old = ObjectUtils.EMPTY_STRING;

                @Override
                public void itemStateChanged(final ItemEvent e) {
                    String value = (String) modeComboEditor.getModel().getSelectedItem();
                    if (old == null) {
                        old = ObjectUtils.EMPTY_STRING;
                    }
                    if (value == null) {
                        value = ObjectUtils.EMPTY_STRING;
                        old = ObjectUtils.EMPTY_STRING;
                    }
                    if (!old.equalsIgnoreCase(value) && !value.isEmpty()) {
                        if (!old.equals(ObjectUtils.EMPTY_STRING)) {
                            try {
                                if (enableModeUpdateCheckBox.isSelected()) {

                                    final DeviceProxy dp = DeviceProxyFactory.get(getModel());
                                    final DeviceAttribute da = new DeviceAttribute(ATTRIBUTE_MODE);
                                    da.insert(value);
                                    dp.write_attribute(da);
                                }
                            } catch (final DevFailed e1) {
                                JOptionPane.showMessageDialog((Component) e.getSource(), DevFailedUtils.toString(e1),
                                        "Mode Update Error", JOptionPane.WARNING_MESSAGE);
                            }
                        }
                        old = value;
                    }
                }
            });

        }
        return modeComboEditor;
    }

    /**
     * This method initializes westPanel
     * 
     * @return javax.swing.JPanel
     */
    private JPanel getNorthPanel() {

        if (northPanel == null) {
            northPanel = new JPanel();
            northPanel.setLayout(new BorderLayout());

            JPanel modePanel = new JPanel();
            modePanel.setLayout(new BorderLayout());
            final Label l = new Label();
            l.setText("Mode: ");
            modePanel.add(l, BorderLayout.WEST);
            modePanel.add(getModeComboEditor(), BorderLayout.CENTER);
            modePanel.add(getModeInit(), BorderLayout.EAST);

            final JPanel myNorthPanel = new JPanel();
            myNorthPanel.setBorder(new EmptyBorder(0, 0, 5, 0));
            myNorthPanel.setLayout(new FlowLayout());
            myNorthPanel.add(new JLabel("Allow Mode Update: "));
            myNorthPanel.add(getEnableUpdateCheckBox());
            myNorthPanel.add(new JLabel("Freeze: "));
            myNorthPanel.add(getFreezeCheckBox());
            myNorthPanel.add(getAtkButton());

            northPanel.add(myNorthPanel, BorderLayout.NORTH);
            northPanel.add(modePanel, java.awt.BorderLayout.CENTER);

        }
        return northPanel;
    }

    @Override
    protected void loadPreferences(final Preferences arg0) {
        // NOT USE
    }

    @Override
    protected void savePreferences(final Preferences arg0) {
        // NOT USE
    }
}
