package fr.soleil.bean.diffractometer.gui.util;

import java.awt.Component;
import java.util.Locale;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.table.TableCellEditor;

import org.apache.commons.lang3.StringUtils;

import fr.soleil.bean.diffractometer.gui.summary.SummaryTableModel;

public class CellDoubleEditor extends AbstractCellEditor implements TableCellEditor {

    private static final long serialVersionUID = -6710333214667600320L;
    protected JTextField jTextField = null;
    protected Double cellValue;

    public CellDoubleEditor() {
        this.jTextField = new JTextField();
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        SummaryTableModel tableModel = (SummaryTableModel) table.getModel();
        String format = tableModel.getFormatFor(row);
        String strValue = "" + value;
        if (format != null) {
            strValue = StringUtils.trim(String.format(Locale.US, format, value));
        }
        jTextField.setText(strValue);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                jTextField.requestFocus();
                jTextField.selectAll();
            }
        });
        return jTextField;
    }

    @Override
    public Object getCellEditorValue() {
        Object result = new Double(0);
        try {
            result = new Double(jTextField.getText());
        } catch (NumberFormatException nfe) {
            //
        }
        return result;
    }
}
