package fr.soleil.bean.diffractometer.gui.util;

import fr.soleil.comete.swing.AutoScrolledTextField;

public class AutoScrolledFormattedTextField extends AutoScrolledTextField {

    private static final long serialVersionUID = -7826737346044818367L;

    private String format;

    public void setFormat(String format) {
        this.format = format;
    }

    @Override
    public void setText(String text) {
        if (text != null && format != null) {
            String formattedValue = text;
            Double dValue = null;
            try {
                dValue = Double.valueOf(text);
                formattedValue = String.format(format, dValue);
            } catch (Exception e) {
            }
            super.setText(formattedValue);
        } else {
            super.setText(text);
        }

    }
}
