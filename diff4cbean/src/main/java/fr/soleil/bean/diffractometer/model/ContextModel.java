package fr.soleil.bean.diffractometer.model;

public class ContextModel {

    private String realDiffractometer = null;
    private String prepareDiffractometer = null;
    private String crystalManager = null;
    private boolean isVirtual = false;
    public static String VIRTUAL_LABEL = "Virtual";
    public static String PILOT_LABEL = "Pilot";
    public static String PREPARE_LABEL = "Prepare";

    private static final ContextModel INSTANCE = new ContextModel();

    public static ContextModel getInstance() {
        return ContextModel.INSTANCE;
    }

    public ContextModel() {
    }

    public static String getRealDiffractometer() {
        return getInstance().realDiffractometer;
    }

    public void setRealDiffractometer(String realDiffractometer) {
        this.realDiffractometer = realDiffractometer;
    }

    public static String getPrepareDiffractometer() {
        return getInstance().prepareDiffractometer;
    }

    public void setPrepareDiffractometer(String prepareDiffractometer) {
        this.prepareDiffractometer = prepareDiffractometer;
    }

    public static String getCrystalManager() {
        return getInstance().crystalManager;
    }

    public void setCrystalManager(String crystalManager) {
        this.crystalManager = crystalManager;
    }

    public boolean isVirtual() {
        return isVirtual;
    }

    public void setVirtual(boolean isVirtual) {
        this.isVirtual = isVirtual;
    }
}