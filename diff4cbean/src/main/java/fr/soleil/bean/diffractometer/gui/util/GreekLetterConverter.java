/**
 * 
 */
package fr.soleil.bean.diffractometer.gui.util;

public class GreekLetterConverter{
	private static String[][] table = new String[][]	
	                                {
			{"gamma","\u03b3"},
			{"delta","\u03b4"},
			{"mu","\u03bc"},
			{"omega","\u03c9"},
			{"komega","\u03c9"},
			{"chi","\u03c7"},
			{"phi","\u03c6"},
			{"kphi","\u03c6"},
			{"kappa","\u03ba"},
			{"twotheta","2\u03b8"},
			{"2theta","2\u03b8"},
			{"tth","2\u03b8"}
										};
	
	public static String convertToUTF(String greekLetterLitteral){
		String result = "";
		for (int i = 0; i < table.length; i++) {
			if(table[i][0].equalsIgnoreCase(greekLetterLitteral)){
				result = table[i][1];
				break;
			}
		}
		
		return result;
	}
}