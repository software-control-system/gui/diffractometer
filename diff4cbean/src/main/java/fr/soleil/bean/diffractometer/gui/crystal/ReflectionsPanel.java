package fr.soleil.bean.diffractometer.gui.crystal;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;

import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.BorderHighlighter;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.HighlighterFactory;
import org.jdesktop.swingx.table.TableColumnExt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.DeviceProxyFactory;
import fr.soleil.bean.diffractometer.gui.ReachTargetBean;
import fr.soleil.bean.diffractometer.gui.TargetAxesBeanPanel;
import fr.soleil.bean.diffractometer.gui.util.AxesBean;
import fr.soleil.bean.diffractometer.gui.util.BoldFontHighlighter;
import fr.soleil.bean.diffractometer.gui.util.TangoConstants;
import fr.soleil.bean.diffractometer.model.ContextModel;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.swing.AutoScrolledTextField;
import fr.soleil.comete.swing.Button;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.TabbedPane;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.lib.project.ObjectUtils;
import net.infonode.gui.border.EdgeBorder;

/**
 * @author HARDION
 * 
 */
public class ReflectionsPanel extends AbstractTangoBox implements TangoConstants {

    private static final long serialVersionUID = 2698470221937805289L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ReflectionsPanel.class);

    private static final String INDEX = "Index";
    private static final String H = "H";
    private static final String K = "K";
    private static final String L = "L";
    private static final String REFLECTIONS_ANGLES = "ReflectionsAngles";
    private static final String LABEL_BUTTON_ADD_REFLECTION_FROM_PILOT = "Add from Pilot";
    private static final String LABEL_BUTTON_ADD_REFLECTION_FROM_PREPARE = "Add from Prepare";
    private static final String LABEL_BUTTON_ADD_REFLECTION_FROM_VIRTUAL = "Add from Virtual";
    private static final String LABEL_BUTTON_REMOVE_REFLECTION = "Remove";
    private static final String LABEL_BUTTON_COMPUTE_U = "Compute U";
    private static final String LABEL_BUTTON_REFINE = "Refine";
    private static final String LABEL_BUTTON_GO_TO_REFLECTION = "Go to Reflection";
    private static final String LABEL_BUTTON_CLOSE = "Close";
    private static final String COMMAND_COMPUTE_U = "ComputeU";
    private static final String COMMAND_AFFINE_CRYSTAL = "AffineCrystal";
    private static final String ATTRIBUTE_REFLECTIONS_COLUMN_TITLES = "ReflectionsColumnTitles";
    private static final String ATTRIBUTE_AXES_NAMES = "AxesNames";
    private static final String PROPERTY_PSEUDO_AXES_PROXIES = "pseudoAxesProxies";
    private static final String ATTRIBUTE_CRYSTAL = "Crystal";
    private static final String COLUMN_PREF = "COLUMN";
    private static final String REFLECTIONS = "Reflections";

    private JScrollPane reflectionTableScrollPane;
    private JPanel reflectionListPanel;
    private TabbedPane reflectionPanel;
    private JPanel reflectionRATablePanel;
    private JXTable reflectionsListTable = null;
    private final List<Integer> selectedRows = new ArrayList<>();
    private ReflectionTableModel reflectionsModel;
    private JToolBar toolBar;
    private Button addFromPrepareButton;
    private Button addFromRealButton;
    private Button removeButton;
    private Button computeUButton;
    private Button refineButton;
    private JToolBar.Separator separator;
    private JSeparator jSeparator;
    public JXTable reflectionsAnglesTable;
    public ReflectionAnglesTableModel reflectionAnglesTableModel;
    private TargetAxesBeanPanel tagetPseudoAxesBean;
    private JDialog pseudoAxesDialog;
    private Button pseudoCloseButton;
    private Button pseudoGotoButton;
    private double pseudoAxesValues[] = new double[0];
    private double gapReflexion;
    private DeviceProxy prepareProxy;
    private AutoScrolledTextField gapTextField;

    /**
     * This is the default constructor
     */
    public ReflectionsPanel(final AxesBean axesBean) {
        super();
        initialize();
    }

    private DeviceProxy getPrepareProxy() {
        try {
            if (prepareProxy == null) {
                prepareProxy = DeviceProxyFactory.get(getModel());
            }
        } catch (DevFailed e) {
            JOptionPane.showMessageDialog(this, DevFailedUtils.toString(e), "Crystal Manager Error",
                    JOptionPane.WARNING_MESSAGE);
            LOGGER.error(DevFailedUtils.toString(e));
        }
        return prepareProxy;
    }

    /**
     * This method initializes this
     * 
     * @return void
     */
    private void initialize() {
        setLayout(new GridBagLayout());
        final GridBagConstraints toolbarConstraints = new GridBagConstraints();
        toolbarConstraints.fill = GridBagConstraints.BOTH;
        toolbarConstraints.gridx = 0;
        toolbarConstraints.gridy = 0;
        toolbarConstraints.weightx = 1;
        toolbarConstraints.weighty = 0;
        add(getToolBar(), toolbarConstraints);

        final GridBagConstraints tableConstraints = new GridBagConstraints();
        tableConstraints.fill = GridBagConstraints.BOTH;
        tableConstraints.gridx = 0;
        tableConstraints.gridy = 1;
        tableConstraints.weightx = 1;
        tableConstraints.weighty = 1;
        add(getReflectionPanel(), tableConstraints);
    }

    /**
     * This method initializes reflectionTableScrollPane
     * 
     * @return ScrollPane
     */
    private JScrollPane getReflectionTableScrollPane() {
        if (reflectionTableScrollPane == null) {
            reflectionTableScrollPane = new JScrollPane(getReflectionTable());
            reflectionTableScrollPane.setViewportView(getReflectionTable());
        }
        return reflectionTableScrollPane;
    }

    /**
     * This method initializes reflectionTableScrollPane
     * 
     * 
     */
    private JPanel getReflectionListPanel() {
        if (reflectionListPanel == null) {
            reflectionListPanel = new JPanel();
            reflectionListPanel.setLayout(new BorderLayout());
            reflectionListPanel.add(getToolBar(), BorderLayout.NORTH);
            reflectionListPanel.add(getReflectionTableScrollPane(), BorderLayout.CENTER);
            reflectionListPanel.add(getPseudoGoToButtonPanel(), BorderLayout.SOUTH);
        }
        return reflectionListPanel;
    }

    /**
     * 
     * @return
     */
    private JPanel getReflectionRATablePanel() {
        if (reflectionRATablePanel == null) {
            reflectionRATablePanel = new JPanel();
            reflectionRATablePanel.setLayout(new BorderLayout());
            reflectionRATablePanel.add(new JScrollPane(getReflectionsAngleTable()), BorderLayout.CENTER);
            setReflectionsPreferencePanel();
        }
        return reflectionRATablePanel;
    }

    /**
     * 
     * @return TabbedPane
     */
    private TabbedPane getReflectionPanel() {
        if (reflectionPanel == null) {
            reflectionPanel = new TabbedPane();
            reflectionPanel.setPreferredSize(new Dimension(100, 50));
            reflectionPanel.add("Reflections List", getReflectionListPanel());
            reflectionPanel.add("Reflections Angles", getReflectionRATablePanel());
        }
        return reflectionPanel;
    }

    /**
     * This method initializes reflectionTable
     * 
     * @return javax.swing.JTable
     */
    private JTable getReflectionTable() {
        if (reflectionsListTable == null) {
            reflectionsListTable = new JXTable();
            // Table parameters
            reflectionsListTable.setColumnControlVisible(true);
            reflectionsListTable.getTableHeader().setReorderingAllowed(false);
            reflectionsListTable.setHighlighters(HighlighterFactory.createAlternateStriping());
            reflectionsListTable.setShowGrid(false, false);
            // set selection color to light blue
            reflectionsListTable.setSelectionBackground(new Color(190, 230, 255));
        }
        return reflectionsListTable;

    }

    @Override
    protected void savePreferences(final Preferences preferences) {
        final Preferences pref = Preferences.userNodeForPackage(getClass());
        try {
            pref.clear();
        } catch (final BackingStoreException e) {
            LOGGER.error(e.getMessage());
        }
        final List<TableColumn> l = reflectionsListTable.getColumns(true);
        final List<TableColumn> ll = reflectionsListTable.getColumns(false);
        final List<Integer> indexList = new ArrayList<>();
        for (final TableColumn tc : ll) {
            final int modelIndex = tc.getModelIndex();
            indexList.add(modelIndex);
        }
        Collections.sort(indexList);
        int k = 0;
        for (int i = 0; i < l.size(); i++) {
            if (!indexList.contains(i)) {
                pref.putInt(COLUMN_PREF + k, i);
                k++;
            }
        }
    }

    @Override
    protected void loadPreferences(final Preferences preferences) {
        for (int i = 0, k = 0; i < reflectionsListTable.getColumnCount(true); i++) {
            final int column = preferences.getInt(COLUMN_PREF + i, -1);
            if (column == -1) {
                break;
            }
            final TableColumnExt columnExt = reflectionsListTable.getColumnExt(column - k);
            columnExt.setVisible(false);
            k++;
        }
    }

    /**
     * This method initializes toolBar
     * 
     * @return javax.swing.JToolBar
     */
    private JToolBar getToolBar() {
        if (toolBar == null) {
            toolBar = new JToolBar();
            toolBar.setFloatable(false);
            toolBar.add(getAddFromPrepareButton());
            toolBar.add(getAddFromRealButton());
            toolBar.add(getRemoveButton());

            toolBar.add(getSeparator());
            toolBar.add(getComputeUButton());
            toolBar.add(getRefineButton());

            toolBar.add(getJSeparator());
        }
        return toolBar;
    }

    /**
     * This method initializes addButton
     * 
     * @return Button
     */
    private Button getAddFromRealButton() {
        if (addFromRealButton == null) {
            addFromRealButton = new Button();
            addFromRealButton.setText(LABEL_BUTTON_ADD_REFLECTION_FROM_PILOT);
            addFromRealButton.addActionListener(new java.awt.event.ActionListener() {
                @Override
                public void actionPerformed(final java.awt.event.ActionEvent e) {
                    if (model != null) {
                        try {
                            DeviceAttribute pseudoAxesProxies = DeviceProxyFactory
                                    .get(ContextModel.getRealDiffractometer())
                                    .read_attribute(PROPERTY_PSEUDO_AXES_PROXIES);
                            String[] proxies = pseudoAxesProxies.extractStringArray();
                            reflectionsModel.addReflection(proxies[0]);
                        } catch (DevFailed arg0) {
                            JOptionPane.showMessageDialog((Component) e.getSource(), DevFailedUtils.toString(arg0),
                                    "Crystal Manager Error", JOptionPane.WARNING_MESSAGE);
                        }
                    }
                }
            });
        }
        return addFromRealButton;
    }

    /**
     * This method initializes addButton
     * 
     * @return Button
     */
    private Button getAddFromPrepareButton() {
        if (addFromPrepareButton == null) {
            addFromPrepareButton = new Button();

            addFromPrepareButton.addActionListener((e) -> {
                if (model != null) {
                    try {
                        DeviceAttribute pseudoAxesProxies = prepareProxy.read_attribute(PROPERTY_PSEUDO_AXES_PROXIES);
                        String[] proxies = pseudoAxesProxies.extractStringArray();
                        reflectionsModel.addReflection(proxies[0]);
                    } catch (DevFailed arg0) {
                        JOptionPane.showMessageDialog((Component) e.getSource(), DevFailedUtils.toString(arg0),
                                "Crystal Manager Error", JOptionPane.WARNING_MESSAGE);
                    }
                }
            });
        }
        return addFromPrepareButton;
    }

    /**
     * This method initializes removeButton
     * 
     * @return Button
     */
    private Button getRemoveButton() {
        if (removeButton == null) {
            removeButton = new Button();
            removeButton.setText(LABEL_BUTTON_REMOVE_REFLECTION);
            removeButton.addActionListener((e) -> {
                if (model != null) {
                    final int index = reflectionsListTable.getSelectedRow();
                    if (index >= 0) {
                        reflectionsModel.removeReflection(index);
                    }
                }
            });
        }
        return removeButton;
    }

    /**
     * This method initializes computeUButton
     * 
     * @return Button
     */
    private Button getComputeUButton() {
        if (computeUButton == null) {
            computeUButton = new Button();
            computeUButton.setText(LABEL_BUTTON_COMPUTE_U);
            computeUButton.setEnabled(false);
            final JComponent parent = this;

            computeUButton.addActionListener((e) -> {
                DeviceData data;
                DeviceProxy dp = null;
                try {
                    dp = DeviceProxyFactory.get(ContextModel.getCrystalManager());
                    data = new DeviceData();
                    int[] selectValues = new int[2];
                    if (reflectionsListTable.getSelectedRowCount() == 2) {
                        selectValues[0] = selectedRows.get(0);
                        selectValues[1] = selectedRows.get(1);
                        data.insert(selectValues);
                        dp.command_inout(COMMAND_COMPUTE_U, data);
                    }
                } catch (final DevFailed e1) {
                    JOptionPane.showMessageDialog(parent, DevFailedUtils.toString(e1));
                    LOGGER.error(DevFailedUtils.toString(e1));
                }
            });
        }
        return computeUButton;
    }

    /**
     * This method initializes affineButton
     * 
     * @return javax.swing.JButton
     */
    private Button getRefineButton() {
        if (refineButton == null) {
            refineButton = new Button();
            refineButton.setText(LABEL_BUTTON_REFINE);
            refineButton.setEnabled(false);
            refineButton.addActionListener((e) -> {
                DeviceData in;
                DeviceProxy dp = null;
                try {
                    dp = DeviceProxyFactory.get(ContextModel.getCrystalManager());
                    in = new DeviceData();
                    final DeviceAttribute crystal = dp.read_attribute(ATTRIBUTE_CRYSTAL);
                    in.insert(crystal.extractString());
                    dp.command_inout(COMMAND_AFFINE_CRYSTAL, in);
                } catch (final DevFailed e1) {
                    LOGGER.error(DevFailedUtils.toString(e1));
                    JOptionPane.showMessageDialog((Component) e.getSource(), DevFailedUtils.toString(e1),
                            "Crystal Manager Error", JOptionPane.WARNING_MESSAGE);
                }
            });
        }
        return refineButton;
    }

    /**
     * This method initializes separator
     * 
     * @return javax.swing.JToolBar.Separator
     */
    private JToolBar.Separator getSeparator() {
        if (separator == null) {
            separator = new JToolBar.Separator();
        }
        return separator;
    }

    /**
     * This method initializes jSeparator
     * 
     * @return javax.swing.JSeparator
     */
    private JSeparator getJSeparator() {
        if (jSeparator == null) {
            jSeparator = new JSeparator();
        }
        return jSeparator;
    }

    /**
     * @return Returns the reflectionsModel.
     */
    public ReflectionTableModel getReflectionsModel() {
        return reflectionsModel;
    }

    /**
     * @param reflectionsModel The reflectionsModel to set.
     */
    public void setReflectionsModel(final ReflectionTableModel reflectionsModel) {
        this.reflectionsModel = reflectionsModel;
    }

    @Override
    protected void clearGUI() {
        removeAll();
        cleanWidget(addFromPrepareButton);
        cleanWidget(refineButton);
        cleanWidget(computeUButton);
        cleanWidget(removeButton);
    }

    @Override
    protected void refreshGUI() {
        if (getModel() == null || getModel().isEmpty()) {
            LOGGER.error("ReflectionPanel.device is null");
        } else {
            try {
                final DeviceProxy dp = getPrepareProxy();
                pseudoAxesValues = new double[dp.read_attribute(ATTRIBUTE_AXES_NAMES).extractStringArray().length];
            } catch (final DevFailed e) {
                LOGGER.error(DevFailedUtils.toString(e));
            }

            try {
                DevFailed df = null;
                String[] titles;
                try {
                    final DeviceProxy crystalManagerProxy = DeviceProxyFactory.get(ContextModel.getCrystalManager());
                    String[] titlesFromDevice = crystalManagerProxy.read_attribute(ATTRIBUTE_REFLECTIONS_COLUMN_TITLES)
                            .extractStringArray();
                    titles = new String[titlesFromDevice.length - 2];
                    int index = 0;
                    titles[index++] = INDEX;
                    titles[index++] = H;
                    titles[index++] = K;
                    titles[index++] = L;
                    for (int i = index; i < titles.length; i++) {
                        titles[i] = titlesFromDevice[i + 2];
                    }
                } catch (DevFailed e) {
                    df = e;
                    titles = new String[] { INDEX, H, K, L };
                }

                reflectionsModel = new ReflectionTableModel(this, ContextModel.getCrystalManager(), REFLECTIONS,
                        titles);
                getReflectionTable().setDefaultRenderer(Double.class, new ReflectionCellDoubleRenderer());
                if (df != null) {
                    throw df;
                }
            } catch (final DevFailed e) {
                LOGGER.error(DevFailedUtils.toString(e));
            }
            reflectionsListTable.setModel(reflectionsModel);

            ListSelectionModel smodel = reflectionsListTable.getSelectionModel();

            smodel.addListSelectionListener(new ListSelectionListener() {

                @Override
                public void valueChanged(ListSelectionEvent e) {
                    int selectedRowsCount = reflectionsListTable.getSelectedRowCount();
                    boolean isComputable = selectedRowsCount == 2;
                    if (selectedRowsCount == 1) {
                        selectedRows.clear();
                        selectedRows.add(reflectionsListTable.getSelectedRows()[0]);
                    } else if (selectedRowsCount > selectedRows.size()) {
                        for (int row : reflectionsListTable.getSelectedRows()) {
                            if (!selectedRows.contains(row)) {
                                selectedRows.add(row);
                            }
                        }
                    } else if (selectedRowsCount < selectedRows.size()) {
                        List<Integer> keptRow = new ArrayList<>();
                        Integer removedRow = -1;
                        for (int row : reflectionsListTable.getSelectedRows()) {
                            if (selectedRows.contains(row)) {
                                keptRow.add(row);
                            }
                        }
                        for (int row : selectedRows) {
                            if (!keptRow.contains(row)) {
                                removedRow = row;
                                break;
                            }
                        }
                        selectedRows.remove(Integer.valueOf(removedRow));
                    }

                    computeUButton.setEnabled(isComputable);
                    String tooltip = isComputable
                            ? new StringBuilder().append("Compute row ").append(selectedRows.get(0))
                                    .append(" with row ").append(selectedRows.get(1)).toString()
                            : null;
                    computeUButton.setToolTipText(tooltip);
                }

            });

            reflectionsModel.addTableModelListener((event) -> {
                final Object object = event.getSource();
                if (object instanceof ReflectionTableModel) {
                    final ReflectionTableModel table = (ReflectionTableModel) object;
                    int rowCount = 0;
                    rowCount = table.getRowCount();
                    refineButton.setEnabled(rowCount >= 3);
                }
            });

            final String[] title = new String[] { INDEX, H, K, L };
            final String[] legend = new String[] { "measurement", "calculate", "Exceeded gap" };

            try {
                reflectionAnglesTableModel = new ReflectionAnglesTableModel(this, title, legend,
                        ContextModel.getCrystalManager(), reflectionsModel, REFLECTIONS_ANGLES);
            } catch (DevFailed e2) {
                LOGGER.error(e2.getMessage());
            }

            reflectionsAnglesTable.setModel(reflectionAnglesTableModel);

            getTargetPseudoAxesBean().setModel(getModel());
            getTargetPseudoAxesBean().start();

            final TangoKey crystalKey = new TangoKey();
            TangoKeyTool.registerAttribute(crystalKey,
                    ContextModel.getCrystalManager() + TANGO_SEPARATOR + ATTRIBUTE_CRYSTAL);
            final ITextTarget acquisitionCrystalTarget = new ITextTarget() {
                String old = ObjectUtils.EMPTY_STRING;

                @Override
                public void addMediator(final Mediator<?> arg0) {
                    // NOT USE
                }

                @Override
                public void removeMediator(final Mediator<?> arg0) {
                    // NOT USE
                }

                @Override
                public String getText() {
                    return old;
                }

                @Override
                public void setText(final String newValue) {
                    if (!old.equals(newValue)) {
                        if (SwingUtilities.isEventDispatchThread()) {
                            reflectionsModel.fireTableStructureChanged();
                            reflectionAnglesTableModel.fireTableStructureChanged();
                        } else {
                            SwingUtilities.invokeLater(() -> {
                                reflectionsModel.fireTableStructureChanged();
                                reflectionAnglesTableModel.fireTableStructureChanged();
                            });
                        }
                    }
                }
            };

            setWidgetModel(acquisitionCrystalTarget, stringBox, crystalKey);
            loadPreferences(Preferences.userNodeForPackage(getClass()));

            String addFromPrepareOrVirtualLabel = LABEL_BUTTON_ADD_REFLECTION_FROM_PREPARE;
            if (ContextModel.getInstance().isVirtual()) {
                addFromPrepareOrVirtualLabel = LABEL_BUTTON_ADD_REFLECTION_FROM_VIRTUAL;
            }
            addFromPrepareButton.setText(addFromPrepareOrVirtualLabel);
        }
    }

    @Override
    protected void onConnectionError() {
        // NOT USE
    }

    /**
     * 
     * @return Panel
     */
    private JPanel getPseudoGoToButtonPanel() {
        final JPanel panel = new JPanel();
        if (pseudoGotoButton == null) {
            pseudoGotoButton = new Button(LABEL_BUTTON_GO_TO_REFLECTION);
            panel.add(pseudoGotoButton);
            pseudoGotoButton.addActionListener((e) -> {
                if (reflectionsListTable.getSelectedRow() >= 0) {
                    for (int i = 0; i < pseudoAxesValues.length; i++) {
                        pseudoAxesValues[i] = (double) (reflectionsListTable.getValueAt(
                                reflectionsListTable.getSelectedRow(),
                                i + reflectionsListTable.getColumnCount(false) - pseudoAxesValues.length));
                    }

                    getTargetPseudoAxesBean().setPseudoAxesValues(pseudoAxesValues);
                    getPseudoAxesDialog().setVisible(true);
                    getPseudoAxesDialog().setModal(true);
                } else {
                    JOptionPane.showMessageDialog(getTargetPseudoAxesBean(), "Select Reflection List Index", "Warning",
                            JOptionPane.WARNING_MESSAGE);
                }
            });
        }
        return panel;
    }

    /**
     * 
     * @return Panel
     */
    private JPanel getPseudoCloseButton() {
        final JPanel panelButton = new JPanel();
        panelButton.setLayout(new BorderLayout());
        if (pseudoCloseButton == null) {
            pseudoCloseButton = new Button(LABEL_BUTTON_CLOSE);
            panelButton.add(pseudoCloseButton, BorderLayout.EAST);
            pseudoCloseButton.addActionListener((e) -> {
                pseudoAxesDialog.setVisible(false);
            });
        }
        return panelButton;
    }

    /**
     * 
     * @return JDialog
     */
    private JDialog getPseudoAxesDialog() {
        if (pseudoAxesDialog == null) {
            pseudoAxesDialog = new JDialog();
            pseudoAxesDialog.setSize(1200, 380);
            pseudoAxesDialog.setPreferredSize(new Dimension(1200, 380));
            pseudoAxesDialog.setTitle("PSeudo Axes Target Parameters");
            pseudoAxesDialog.setLayout(new BorderLayout());
            pseudoAxesDialog.add(getTargetPseudoAxesBean(), BorderLayout.CENTER);
            pseudoAxesDialog.add(getPseudoCloseButton(), BorderLayout.SOUTH);
            pseudoAxesDialog.setModal(true);
            pseudoAxesDialog.setLocationRelativeTo(null);
            pseudoAxesDialog.pack();
        }
        return pseudoAxesDialog;
    }

    /**
     * This method initializes tagetAxesBean
     * 
     * @return fr.soleil.bean.diffractometer.TagetAxesBean
     */
    private TargetAxesBeanPanel getTargetPseudoAxesBean() {
        if (tagetPseudoAxesBean == null) {
            tagetPseudoAxesBean = new TargetAxesBeanPanel();
            tagetPseudoAxesBean.setType(ReachTargetBean.PSEUDO_AXES_TYPE);
            tagetPseudoAxesBean.setBorder(new TitledBorder(null, "Axes", TitledBorder.DEFAULT_JUSTIFICATION,
                    TitledBorder.DEFAULT_POSITION, null, null));
        }
        return tagetPseudoAxesBean;
    }

    private void setReflectionsPreferencePanel() {
        if (gapTextField == null) {
            gapTextField = new AutoScrolledTextField();
            gapTextField.setColumns(10);
            double value = getGapReflexion();
            gapTextField.setText(Double.toString(value));

            gapTextField.getDocument().addDocumentListener(new DocumentListener() {

                @Override
                public void removeUpdate(DocumentEvent e) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void insertUpdate(DocumentEvent e) {
                    Double newGap = Double.valueOf(gapTextField.getText());
                    setGapReflexion(newGap);
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    // TODO Auto-generated method stub
                }

            });
        }
        reflectionRATablePanel.add(reflectionPreferencesPanel(), BorderLayout.SOUTH);
    }

    private JPanel reflectionPreferencesPanel() {
        final JPanel refPanel = new JPanel();
        refPanel.setBorder(BorderFactory.createTitledBorder("Reflection preference"));

        refPanel.add(gapTextField, BorderLayout.CENTER);

        final Label gapLabel = new Label();
        gapLabel.setText("Exceeded gap : ");
        refPanel.add(gapLabel, BorderLayout.CENTER);

        return refPanel;
    }

    private JXTable getReflectionsAngleTable() {
        if (reflectionsAnglesTable == null) {
            reflectionsAnglesTable = new JXTable();
            reflectionsAnglesTable.setAutoStartEditOnKeyStroke(true);
            reflectionsAnglesTable.setTerminateEditOnFocusLost(true);
            reflectionsAnglesTable.setTableHeader(null);
            reflectionsAnglesTable.setEditable(true);
            // reflectionsAnglesTable.setCellEditor(new
            // DefaultCellEditor(textField));

            reflectionsAnglesTable.setHighlighters(new ColorHighlighter(new HighlightPredicate() {

                @Override
                public boolean isHighlighted(final Component renderer, final ComponentAdapter adapter) {

                    return adapter.row < 3 && adapter.column < 3 && !(adapter.row <= 2 && adapter.column < 1); // Empty
                    // area
                }

            }, Color.gray, Color.gray), new ColorHighlighter(new HighlightPredicate() {

                @Override
                public boolean isHighlighted(final Component renderer, final ComponentAdapter adapter) {
                    boolean result = false;
                    if (adapter.row == 3 && adapter.column <= 3) { // Horizontal
                        // title
                        result = true;
                    } else if (adapter.column == 3 && adapter.row <= 3) { // Vertical
                        // Title
                        result = true;
                    } else if (adapter.row >= 3 && adapter.column == 0) { // Vertical
                        // Index
                        result = true;
                    } else if (adapter.column >= 3 && adapter.row == 0) { // Vertical
                        // Index
                        result = true;
                    } else if (adapter.column > 3 && adapter.row > 3) { // Data
                        result = true;
                    }
                    return result;
                }

            }, getBackground(), null)

                    , new ColorHighlighter(new HighlightPredicate() {

                        @Override
                        public boolean isHighlighted(final Component renderer, final ComponentAdapter adapter) {
                            boolean result = false;
                            if (adapter.column == 0 && adapter.row == 1
                                    || adapter.column > 3 && adapter.row > 3 && adapter.row > adapter.column) { // Data
                                result = true;
                            }
                            return result;
                        }

                    }, Color.YELLOW, null)

                    , new ColorHighlighter(new HighlightPredicate() {

                        @Override
                        public boolean isHighlighted(final Component renderer, final ComponentAdapter adapter) {
                            boolean result = false;
                            if (adapter.column == 0 && adapter.row == 0
                                    || adapter.column > 3 && adapter.row > 3 && adapter.row < adapter.column) { // Data
                                result = true;
                            }
                            return result;
                        }

                    }, Color.ORANGE, null)

                    , new BoldFontHighlighter(new HighlightPredicate() {

                        @Override
                        public boolean isHighlighted(final Component renderer, final ComponentAdapter adapter) {
                            boolean result = false;
                            if (adapter.column > 0 && adapter.column <= 3 && adapter.row > 3) {
                                result = true;
                            } else if (adapter.row > 0 && adapter.row <= 3 && adapter.column > 3) {
                                result = true;
                            }

                            return result;
                        }

                    })

                    , new BorderHighlighter(new HighlightPredicate() {

                        @Override
                        public boolean isHighlighted(final Component renderer, final ComponentAdapter adapter) {
                            boolean result = false;
                            if (adapter.column == 3) {
                                result = true;
                            }
                            return result;
                        }

                    }, new EdgeBorder(false, false, false, true))

                    , new BorderHighlighter(new HighlightPredicate() {

                        @Override
                        public boolean isHighlighted(final Component renderer, final ComponentAdapter adapter) {
                            boolean result = false;
                            if (adapter.row == 3) {
                                result = true;
                            }
                            return result;
                        }

                    }, new EdgeBorder(false, true, false, false)), new ColorHighlighter(new HighlightPredicate() {

                        @Override
                        public boolean isHighlighted(final Component arg0, final ComponentAdapter adaptater) {
                            final Object o1 = reflectionsAnglesTable.getModel().getValueAt(adaptater.row,
                                    adaptater.column);

                            final Object o2 = reflectionsAnglesTable.getModel().getValueAt(adaptater.column,
                                    adaptater.row);
                            if (o1 instanceof String && o2 instanceof String) {
                                String string1 = (String) o1;
                                String string2 = (String) o2;

                                try {
                                    double d1 = Double.parseDouble(string1);
                                    double d2 = Double.parseDouble(string2);
                                    final double valueToCompare = getGapReflexion();

                                    if (valueToCompare != Double.NaN && valueToCompare >= 0.0
                                            && (d1 - d2 > valueToCompare || d2 - d1 > valueToCompare)) {
                                        return true;

                                    }
                                } catch (NumberFormatException nfe) {
                                    // Not a number
                                }
                            } else if (adaptater.column == 0 && adaptater.row == 2) {
                                return true; // legend
                            }

                            return false;
                        }
                    }, Color.RED, null));

        }
        return reflectionsAnglesTable;
    }

    public void setGapReflexion(final double gapReflexion) {
        this.gapReflexion = gapReflexion;
        refreshGUI();

    }

    public double getGapReflexion() {
        return gapReflexion;
    }

}