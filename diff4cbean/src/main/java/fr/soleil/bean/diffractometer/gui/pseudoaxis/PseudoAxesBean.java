/**
 * 
 */

package fr.soleil.bean.diffractometer.gui.pseudoaxis;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.prefs.Preferences;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxyFactory;
import fr.soleil.bean.diffractometer.model.DiffractometerModel;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.swing.Label;

/**
 * @author HARDION
 */
public class PseudoAxesBean extends AbstractTangoBox {

    private static final long serialVersionUID = 2317997902992200821L;

    private static final String ATTRIBUTE_AXIS_CURRENT_POSITION = "AxisCurrentPosition";
    private static final Logger LOGGER = LoggerFactory.getLogger(PseudoAxesBean.class);

    private DiffractometerModel model;
    private String[][] pseudoAxis;
    private PseudoAxisBean[] pseudoAxisBean = null;
    private final Label errorLabel = new Label();

    /**
     * This is the default constructor
     */
    public PseudoAxesBean() {
        super();
        initialize();
        errorLabel.setText("Problem with PseudoAxesProxies and EulerianPseudoAxis properties");
    }

    /**
     * This method initializes this
     * 
     * @return void
     */
    private void initialize() {
        setLayout(new GridBagLayout());
        final GridBagConstraints fullConstraints = new GridBagConstraints();
        fullConstraints.fill = GridBagConstraints.BOTH;
        fullConstraints.gridx = 0;
        fullConstraints.gridy = 0;
        fullConstraints.weightx = 1;
        fullConstraints.weighty = 1;
        add(errorLabel, fullConstraints);
    }

    @Override
    protected void clearGUI() {
        removeAll();
        if (pseudoAxisBean != null) {
            for (int i = 0; i < pseudoAxisBean.length; i++) {
                if (pseudoAxisBean[i] != null) {
                    pseudoAxisBean[i].setModel(null);
                    pseudoAxisBean[i] = null;
                }
            }
            pseudoAxisBean = null;
        }
    }

    @Override
    protected void refreshGUI() {
        clearGUI();
        try {
            int pseudoLength = 0;
            if (pseudoAxis != null) {
                pseudoLength = pseudoAxis.length;
            }

            int nextY = 0;
            final int nextX = 0;

            if (pseudoAxis != null) {
                pseudoAxisBean = new PseudoAxisBean[pseudoLength];
                for (int i = 0; i < pseudoLength; i++) {
                    try {
                        pseudoAxisBean[i] = new PseudoAxisBean();
                        pseudoAxisBean[i].setModel(pseudoAxis[i][1]);
                        pseudoAxisBean[i].start();
                        final GridBagConstraints pseudoAxisConstraints = new GridBagConstraints();
                        pseudoAxisConstraints.fill = GridBagConstraints.BOTH;
                        pseudoAxisConstraints.gridx = nextX;
                        pseudoAxisConstraints.gridy = nextY;
                        pseudoAxisConstraints.weightx = 0;
                        pseudoAxisConstraints.weighty = 1;
                        pseudoAxisConstraints.insets = new Insets(0, 0, 30, 0);
                        add(pseudoAxisBean[i], pseudoAxisConstraints);
                        // nextX = (nextX + 1) % 2;
                        // if (nextX == 0) {
                        // nextY++;
                        // }
                        nextY++;
                    } catch (Exception e) {
                        LOGGER.error("error", e);
                    }
                }
            }
            if (getComponentCount() == 0) {
                final GridBagConstraints fullConstraints = new GridBagConstraints();
                fullConstraints.fill = GridBagConstraints.BOTH;
                fullConstraints.gridx = 0;
                fullConstraints.gridy = 0;
                fullConstraints.weightx = 1;
                fullConstraints.weighty = 1;
                add(errorLabel, fullConstraints);
            }
        } catch (final Throwable t) {
            LOGGER.error("error", t);
        }
    }

    @Override
    protected void onConnectionError() {
        throw new UnsupportedOperationException("Not supported yet");
    }

    @Override
    protected String getVersion() {
        String result = super.getVersion();
        if (getModel() != null) {
            model = new DiffractometerModel();

            model.setDiffractometer(getModel());
            pseudoAxis = model.getPseudoAxis();
            if (pseudoAxis != null && pseudoAxis.length > 0) {
                try {
                    final DeviceAttribute attribute = DeviceProxyFactory.get(pseudoAxis[0][1])
                            .read_attribute(ATTRIBUTE_AXIS_CURRENT_POSITION);
                    attribute.extractDouble();
                    result = "1";
                } catch (final DevFailed e) {
                    result = "2";
                }
            }
        }
        return result;
    }

    @Override
    protected void loadPreferences(final Preferences arg0) {
        // NOT USE
    }

    @Override
    protected void savePreferences(final Preferences arg0) {
        // NOT USE
    }

}