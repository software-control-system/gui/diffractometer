package fr.soleil.bean.diffractometer.gui.crystal;

import java.util.Locale;
import java.util.Set;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellRenderer;

import fr.soleil.bean.diffractometer.model.AxesFormat;

/**
 * 
 * @author ADIOUF
 * 
 */
public class ReflectionCellDoubleRenderer extends JTextField implements TableCellRenderer {

    private static final long serialVersionUID = 8459187392189919224L;

    private static final String AXIS_INDEX = "axisindex";

    public ReflectionCellDoubleRenderer() {
        super();
        setOpaque(true);
    }

    @Override
    public JTextField getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected,
            final boolean hasFocus, final int row, final int col) {
        if (isSelected) {
            setBackground(table.getSelectionBackground());
        } else {
            setBackground(table.getBackground());
        }
        final double mon_double = (Double) value;
        String name = "Axis" + table.getColumnName(col);
        Set<String> devicenames = null;
        devicenames = AxesFormat.getInstance().getAxesSet();
        String axesFormat = null;

        if (AXIS_INDEX.equals(name.toLowerCase())) {
            setText((int) mon_double + "");
        } else if (devicenames.contains(name.toLowerCase())) {
            axesFormat = AxesFormat.getInstance().getFormat(name);
            if (axesFormat != null) {
                setText(String.format(Locale.US, axesFormat, mon_double));
            } else {
                setText(mon_double + "");
            }
        } else {
            setText(mon_double + "");
        }

        setHorizontalAlignment(JTextField.RIGHT);
        setEditable(false);
        return this;
    }

}