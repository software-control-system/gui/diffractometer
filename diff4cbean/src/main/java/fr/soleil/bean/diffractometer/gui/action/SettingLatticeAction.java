/*
 * Created on 10 juin 2005
 * with Eclipse
 */
package fr.soleil.bean.diffractometer.gui.action;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.text.ParseException;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.DeviceProxyFactory;
import fr.soleil.bean.diffractometer.gui.crystal.LatticeDialog;

public class SettingLatticeAction extends AbstractAction {

    private static final long serialVersionUID = 3696302991869323365L;

    private static final Logger LOGGER = LoggerFactory.getLogger(SettingLatticeAction.class);

    public static final String LATTICE = "Lattice";
    public static final String LATTICE_STAR = "Lattice Star";

    public DeviceProxy model = null;

    /**
     * @throws DevFailed
     * 
     */
    public SettingLatticeAction(final String device) throws DevFailed {
        this(device, LATTICE);
    }

    public SettingLatticeAction(final String device, final String mode) throws DevFailed {
        super();
        model = DeviceProxyFactory.get(device);
        // This is an instance initializer; it is executed just after the
        // constructor of the superclass is invoked

        // The following values are completely optional
        putValue(Action.NAME, "set " + mode);
        // Set tool tip text
        putValue(Action.SHORT_DESCRIPTION, "Setting " + mode + " in one time");

        // This text is not directly used by any Swing component;
        // however, this text could be used in a help system
        putValue(Action.LONG_DESCRIPTION, "Setting " + mode + " in one time");

        // Set an icon
        // Icon icon = new ImageIcon("icon.gif");
        // putValue(Action.SMALL_ICON, icon);

        // Set a mnemonic character. In most look and feels, this causes the
        // specified character to be underlined This indicates that if the
        // component
        // using this action has the focus and In some look and feels, this
        // causes
        // the specified character in the label to be underlined and
        putValue(Action.MNEMONIC_KEY, new Integer(java.awt.event.KeyEvent.VK_L));

        // Set an accelerator key; this value is used by menu items
        putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("alt shift "));

    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        if (model != null) {
            final LatticeDialog ld = new LatticeDialog(null, true);
            ld.setDevice(model);
            ld.setVisible(true);
            if (ld.getReturnStatus() == LatticeDialog.RET_OK) {

                try {
                    final DeviceData devData = new DeviceData();
                    final double[] arg = new double[6];

                    arg[0] = ld.getAValue();
                    arg[1] = ld.getBValue();
                    arg[2] = ld.getCValue();
                    arg[3] = ld.getAlphaValue();
                    arg[4] = ld.getBetaValue();
                    arg[5] = ld.getGammaValue();

                    devData.insert(arg);
                    model.command_inout("ConfigureCrystal", devData);
                } catch (final ParseException ex) {
                    LOGGER.error(ex.getMessage());
                } catch (final DevFailed exc) {
                    LOGGER.error(DevFailedUtils.toString(exc), exc);
                    JOptionPane.showMessageDialog((Component) e.getSource(), DevFailedUtils.toString(exc),
                            "Crystal Manager Error", JOptionPane.WARNING_MESSAGE);
                } catch (final NumberFormatException exc) {
                    LOGGER.error(exc.getMessage());
                }
            }
        }
    }
}