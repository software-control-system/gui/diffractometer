package fr.soleil.bean.diffractometer.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.AttributeInfo;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.DeviceProxyFactory;

public class AxesFormat {

    private static final Logger LOGGER = LoggerFactory.getLogger(AxesFormat.class);

    private static AxesFormat uniqueInstanceFormat;

    private String format;
    private DeviceProxy diffractometer;
    private final static String AXES_NAMES = "axesNames";
    private Map<String, String> loadedFormats;

    private AxesFormat() {
        loadedFormats = new HashMap<>();
        try {
            diffractometer = DeviceProxyFactory.get(ContextModel.getRealDiffractometer());
            loadMapFormat();
        } catch (DevFailed e) {
            LOGGER.error(DevFailedUtils.toString(e), e);
        }
    }

    public static AxesFormat getInstance() {
        if (uniqueInstanceFormat == null) {
            synchronized (AxesFormat.class) {
                if (uniqueInstanceFormat == null) {
                    uniqueInstanceFormat = new AxesFormat();
                }
            }
        }
        return uniqueInstanceFormat;
    }

    private void loadMapFormat() throws DevFailed {
        String[] axes = getAxesNames();
        for (int i = 0; i < axes.length; i++) {
            AttributeInfo attr = diffractometer.get_attribute_info(axes[i]);
            format = attr.format;
            loadedFormats.put(axes[i].toLowerCase(), format);
            loadedFormats.put(StringUtils.remove(axes[i], "Axis").toLowerCase(), format);
        }
    }

    // the name must begin with "Axis"
    public String getFormat(String axe) {
        return loadedFormats.get(axe.toLowerCase().trim());
    }

    public Set<String> getAxesSet() {
        return loadedFormats.keySet();
    }

    private String[] getAxesNames() throws DevFailed {
        String[] result = null;
        if (diffractometer != null) {
            DeviceAttribute axesNames = diffractometer.read_attribute(AXES_NAMES);
            result = axesNames.extractStringArray();
        }
        return result;

    }

}
