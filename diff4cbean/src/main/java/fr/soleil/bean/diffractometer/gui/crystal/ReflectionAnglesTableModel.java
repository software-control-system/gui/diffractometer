package fr.soleil.bean.diffractometer.gui.crystal;

import java.awt.Component;
import java.util.Locale;

import javax.swing.table.TableModel;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.AttributeInfo;
import fr.esrf.TangoApi.DeviceAttribute;

/**
 * 
 * @author FOURNEAU
 * 
 */
public class ReflectionAnglesTableModel extends ADoubleMatrixAttributeTableModel /*implements TableModelListener*/ {

    private static final long serialVersionUID = 9136793505027759759L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ReflectionAnglesTableModel.class);
    private static final String REFLECTION_ANGLES_MODIFICATION_ERROR = "Reflection Angles Modification Error";

    private final String[] caption;
    private final TableModel hkl;
    private String reflectionFormat;
    private volatile AttributeInfo lastInfo;
    private volatile long lastInfoReadDate;

    public ReflectionAnglesTableModel(final Component parent, final String[] titles, final String[] caption,
            final String device, final TableModel hkl, final String angle) throws DevFailed {
        super(parent, device, angle, titles);
        this.caption = caption;
        this.hkl = hkl;
        this.lastInfo = attrProxy.get_info();
        this.reflectionFormat = lastInfo.format;
    }

    private boolean shouldReadInfo() {
        return System.currentTimeMillis() - lastInfoReadDate > 30000;
    }

    private void checkInfo() {
        if (shouldReadInfo()) {
            synchronized (dataLock) {
                if (shouldReadInfo()) {
                    try {
                        lastInfo = attrProxy.get_info();
                    } catch (DevFailed e) {
                        LOGGER.error(DevFailedUtils.toString(e), e);
                    }
                }
            }
        }
    }

    private int isHKLArea(final int rowIndex, final int columnIndex) {
        int result = 0;
        if (columnIndex < titles.length && rowIndex >= titles.length) { // HKL
            result = 1;
        } else if (rowIndex < titles.length && columnIndex >= titles.length) { // HKL
            result = -1;
        }
        return result;
    }

    private boolean isDataArea(final int rowIndex, final int columnIndex) {
        return rowIndex >= titles.length && columnIndex >= titles.length;
    }

    @Override
    public int getRowCount() {
        checkRead();
        return titles.length + lastData.getDimY();
    }

    @Override
    public int getColumnCount() {
        checkRead();
        return titles.length + lastData.getDimX();
    }

    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
        Object result = null;
        if (rowIndex >= titles.length && columnIndex >= titles.length) { // Angles
            checkRead();
            final double[] data = lastData.getValue();
            final int dimX = lastData.getDimX();
            int rowIndexToUse = rowIndex + 0;
            double value = data[dimX * (rowIndexToUse - titles.length) + columnIndex - titles.length];
            if (reflectionFormat != null) {
                result = String.format(Locale.US, reflectionFormat, value);
            } else {
                result = Double.toString(value);
            }
        } else if (columnIndex < titles.length && rowIndex >= titles.length) { // HKL
            // Column
            result = String.format(Locale.US, reflectionFormat, hkl.getValueAt(rowIndex - titles.length, columnIndex));
        } else if (rowIndex < titles.length && columnIndex >= titles.length) { // HKL
            // row
            result = String.format(Locale.US, reflectionFormat, hkl.getValueAt(columnIndex - titles.length, rowIndex));
        } else if (rowIndex == titles.length - 1) { // Row Title Area
            result = titles[columnIndex];
        } else if (columnIndex == titles.length - 1) { // Column Title Area
            result = titles[rowIndex];
        } else if (caption.length > 0 && rowIndex < caption.length && columnIndex == 0) {
            result = caption[rowIndex];
        }
        return result;
    }

    @Override
    public void setValueAt(final Object value, final int rowIndex, final int columnIndex) {
        final int hklArea = isHKLArea(rowIndex, columnIndex);
        if (hklArea == 1) { // HKL Column
            hkl.setValueAt(value, rowIndex - titles.length, columnIndex);
        } else if (hklArea == -1) { // HKL row
            hkl.setValueAt(value, columnIndex - titles.length, rowIndex);
        } else if (isDataArea(rowIndex, columnIndex)) {
            super.setValueAt(value, rowIndex, columnIndex);
        }
    }

    @Override
    public boolean isCellEditable(final int rowIndex, final int columnIndex) {
        boolean result = false;
        final int hklArea = isHKLArea(rowIndex, columnIndex);
        if (hklArea == 1) { // HKL Column
            result = hkl.isCellEditable(rowIndex - titles.length, columnIndex);
        } else if (hklArea == -1) { // HKL row
            result = hkl.isCellEditable(columnIndex - titles.length, rowIndex);
        } else if (isDataArea(rowIndex, columnIndex)) {
            checkInfo();
            result = lastInfo.writable != AttrWriteType.READ;
        }
        return result;
    }

    @Override
    public Class<?> getColumnClass(final int columnIndex) {
        return String.class;
    }

//    @Override
//    public void tableChanged(final TableModelEvent e) {
//        if (e.getSource() == hkl) {
//            if (e.getFirstRow() == TableModelEvent.HEADER_ROW) {
//                fireTableStructureChanged();
//            } else {
//                fireTableDataChanged();
//            }
//        }
//    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Override
    protected String getWriteErrorMessage() {
        return REFLECTION_ANGLES_MODIFICATION_ERROR;
    }

    @Override
    protected AValueWriter generateValueWriter(Object value, int row, int column) {
        return new AngleWriter(value, row, column);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class AngleWriter extends AValueWriter {

        public AngleWriter(Object value, int row, int column) {
            super(value, row, column);
        }

        @Override
        protected Void doInBackground() throws Exception {
            DeviceAttribute da;
            da = attrProxy.read();
            double[] data = da.extractDoubleArray();
            final int dimX = da.getDimX();
            final int dimY = da.getDimY();
            final int readSize = dimX * dimY;
            // suppress write part
            data = ArrayUtils.subarray(data, 0, readSize);
            data[dimX * (row - titles.length) + column - titles.length] = Double.parseDouble((String) value);
            da.insert(data, dimX, dimY);
            attrProxy.write(da);
            return null;
        }

    }

}
