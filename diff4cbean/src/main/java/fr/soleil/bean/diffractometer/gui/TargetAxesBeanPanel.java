/**
 * 
 */

package fr.soleil.bean.diffractometer.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.prefs.Preferences;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.AttributeProxy;
import fr.soleil.bean.diffractometer.gui.util.TangoConstants;
import fr.soleil.bean.diffractometer.model.DiffractometerModel;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.swing.Button;
import fr.soleil.comete.swing.Label;

/**
 * 
 * @author ADIOUF
 * 
 */
public class TargetAxesBeanPanel extends AbstractTangoBox implements TangoConstants {

    private static final long serialVersionUID = 4476152917781947845L;

    private static final Logger LOGGER = LoggerFactory.getLogger(TargetAxesBeanPanel.class);

    private DiffractometerModel model;
    private String[][] pseudoAxis;
    private String[][] realAxisProxies;
    private JPanel captionPanel = null;
    private ReachTargetBean reachTargetBean[] = null;
    private double pseudoAxesValues[] = new double[6];
    private JPanel[] panel = null;
    private final Label errorLabel = new Label();
    public int pseudoLength = 0;
    public int type = 1;
    private ScheduledExecutorService executor;
    private ScheduledFuture<?> future;

    private Button gotoAllButton = null;
    private Button stopAllButton = null;

    /**
     * This is the default constructor
     */
    public TargetAxesBeanPanel() {
        super();
        initialize();
        errorLabel.setText("Problem with Axes Attributes");
    }

    /**
     * This method initializes this
     * 
     * @return void
     */
    private void initialize() {
        setLayout(new BorderLayout());
        add(errorLabel, BorderLayout.CENTER);
    }

    @Override
    protected void clearGUI() {
        removeAll();
        if (future != null) {
            future.cancel(true);
        }
        if (executor != null) {
            executor.shutdownNow();
        }
    }

    /**
     * 
     * @return JPanel
     */
    private JPanel getGoToAllButtonPanel() {
        final JPanel panel = new JPanel(new BorderLayout());
        if (gotoAllButton == null) {
            gotoAllButton = new Button("Go to all");
            gotoAllButton.setFocusPainted(false);
            final Color saveColor = gotoAllButton.getBackground();
            panel.add(gotoAllButton, BorderLayout.CENTER);
            gotoAllButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    setAllMotorTagetPosirion();
                }
            });
            final Runnable r = new Runnable() {

                @Override
                public void run() {
                    boolean arrived = true;
                    for (final ReachTargetBean rtb : reachTargetBean) {

                        try {
                            final AttributeProxy targetAttr = new AttributeProxy(rtb.getModelAttribute());
                            final AttributeProxy positionAttr = new AttributeProxy(rtb.getMotorBeanModel());
                            final double targetValue = targetAttr.read().extractDouble();
                            final double position = positionAttr.read().extractDouble();

                            final double distance = Math.abs(targetValue - position);
                            arrived = arrived & distance < 0.001;
                        } catch (final DevFailed e) {
                            LOGGER.error(DevFailedUtils.toString(e), e);
                        }
                    }
                    if (!arrived) {
                        gotoAllButton.setBackground(Color.ORANGE);
                    } else {
                        gotoAllButton.setBackground(saveColor);
                    }
                    gotoAllButton.validate();

                }
            };
            executor = Executors.newScheduledThreadPool(1);
            executor.scheduleAtFixedRate(r, 0L, getRefreshingTime(), TimeUnit.MILLISECONDS);

        }

        return panel;
    }

    private JPanel getCaptionPanel() {
        if (captionPanel == null) {
            captionPanel = new JPanel();
            captionPanel.setLayout(new GridBagLayout());
            captionPanel.setBorder(BorderFactory.createTitledBorder("Caption"));
            captionPanel.setPreferredSize(new Dimension(125, 250));
            captionPanel.setMinimumSize(new Dimension(125, 250));
            captionPanel.setMaximumSize(new Dimension(125, 250));
            int y = 0;
            final GridBagConstraints gbc = new GridBagConstraints();
            gbc.fill = GridBagConstraints.BOTH;
            gbc.gridx = 0;
            gbc.ipady = 30;
            gbc.weightx = 1;
            gbc.anchor = GridBagConstraints.WEST;
            gbc.gridwidth = GridBagConstraints.REMAINDER;
            final JLabel targetLabel = new JLabel("Target :");
            captionPanel.add(targetLabel, gbc);
            gbc.ipady = 5;
            y++;
            gbc.insets.top = 10;
            gbc.gridy = y;
            final JLabel devLabel = new JLabel("Device :");
            captionPanel.add(devLabel, gbc);
            gbc.ipady = 30;
            gbc.insets.top = 0;
            y++;
            gbc.gridy = y;
            final JLabel realPosLabel = new JLabel("Real position :");
            captionPanel.add(realPosLabel, gbc);
            gbc.ipady = 30;
            y++;
            gbc.insets.bottom = 60;
            gbc.gridy = y;
            final JLabel setPosLabel = new JLabel("Set position :");
            captionPanel.add(setPosLabel, gbc);
        }
        return captionPanel;
    }

    private JPanel getStopAllButtonPanel() {
        final JPanel panel = new JPanel(new BorderLayout());
        if (stopAllButton == null) {
            stopAllButton = new Button("Stop all");
            stopAllButton.setBorder(BorderFactory.createCompoundBorder(
                    BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED),
                    BorderFactory.createLineBorder(Color.RED, 2)));
            stopAllButton.setBounds(0, 0, 30, 10);
            panel.add(stopAllButton, BorderLayout.CENTER);
            stopAllButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    stopMotorTargetPosition();
                }
            });
        }
        return panel;
    }

    private JPanel getGoAndStopAllButtonPanel() {
        final JPanel panel = new JPanel(new BorderLayout());
        panel.add(getGoToAllButtonPanel(), BorderLayout.CENTER);
        panel.add(getStopAllButtonPanel(), BorderLayout.AFTER_LAST_LINE);
        return panel;
    }

    @Override
    protected void refreshGUI() {
        remove(errorLabel);
        final JPanel reachTargetBeanPanel = new JPanel(new GridBagLayout());
        try {

            model = new DiffractometerModel();
            model.setDiffractometer(getModel());
            pseudoAxis = model.getRealAxis();
            realAxisProxies = model.getRealAxisProxies();

            final int nextY = 0;
            int nextX = 0;

            if (pseudoAxis != null) {
                pseudoLength = pseudoAxis.length;
                reachTargetBean = new ReachTargetBean[pseudoLength];

                panel = new JPanel[pseudoLength];
                final GridBagConstraints viewerConstraints = new GridBagConstraints();
                viewerConstraints.fill = GridBagConstraints.BOTH;
                viewerConstraints.gridx = nextX;
                viewerConstraints.gridy = nextY;
                viewerConstraints.weightx = 1;
                viewerConstraints.weighty = 1;
                viewerConstraints.insets = new Insets(0, 0, 2, 0);
                reachTargetBeanPanel.add(getCaptionPanel(), viewerConstraints);
                nextX++;
                for (int i = 0; i < pseudoLength; i++) {
                    panel[i] = new JPanel();
                    panel[i].setLayout(new GridBagLayout());

                    reachTargetBean[i] = new ReachTargetBean();
                    reachTargetBean[i].setType(type);
                    reachTargetBean[i].setTagetlabel(pseudoAxis[i][0] + " ");

                    // reachTargetBean[i].setPViewersLabelValues("Valeur");

                    // Taget AND realAxisProxies Values
                    for (int j = 0; j < pseudoLength; j++) {
                        if (pseudoAxis[i][0].equalsIgnoreCase(realAxisProxies[j][0])) {
                            reachTargetBean[i].setMotorBeanModel(realAxisProxies[j][1]);
                            reachTargetBean[i].setModel(getModel());
                            break;
                        }
                    }

                    reachTargetBean[i].setModelAttribute(getModel() + TANGO_SEPARATOR + pseudoAxis[i][1]);
                    reachTargetBean[i].start();
                    final GridBagConstraints reachTargetBeanConstraints = new GridBagConstraints();
                    reachTargetBeanConstraints.fill = GridBagConstraints.BOTH;
                    reachTargetBeanConstraints.gridx = 0;
                    reachTargetBeanConstraints.gridy = 1;
                    reachTargetBeanConstraints.weightx = 1;
                    panel[i].add(reachTargetBean[i], reachTargetBeanConstraints);

                    final GridBagConstraints viewer1Constraints = new GridBagConstraints();
                    viewer1Constraints.fill = GridBagConstraints.BOTH;
                    viewer1Constraints.gridx = nextX;
                    viewer1Constraints.gridy = nextY;
                    viewer1Constraints.weightx = 1;
                    viewer1Constraints.weighty = 1;
                    viewer1Constraints.insets = new Insets(0, 0, 2, 0);
                    reachTargetBeanPanel.add(panel[i], viewer1Constraints);
                    nextX++;
                }
                setLayout(new BorderLayout());
                add(reachTargetBeanPanel, BorderLayout.CENTER);
                add(getGoAndStopAllButtonPanel(), BorderLayout.SOUTH);

            }

            if (getComponentCount() == 0) {
                final GridBagConstraints fullConstraints = new GridBagConstraints();
                fullConstraints.fill = GridBagConstraints.BOTH;
                fullConstraints.gridx = 0;
                fullConstraints.gridy = 0;
                fullConstraints.weightx = 1;
                fullConstraints.weighty = 1;
                add(errorLabel, fullConstraints);
            }

        } catch (final Throwable t) {
            LOGGER.error("error", t);
        }
    }

    /**
     * 
     */
    public void setAllMotorTagetPosirion() {
        for (int i = 0; i < pseudoLength; i++) {
            reachTargetBean[i].getMotorBean().gotoTargetPosition();
        }
    }

    public void stopMotorTargetPosition() {
        for (int i = 0; i < pseudoLength; i++) {
            reachTargetBean[i].getMotorBean().stopTargetPosition();
        }
    }

    /**
     * 
     * @return ReachTargetBean[]
     */
    public ReachTargetBean[] getReachTargetBean() {
        return reachTargetBean;
    }

    @Override
    protected void onConnectionError() {
        // NOT USE
    }

    public int getType() {
        return type;
    }

    public void setType(final int type) {
        this.type = type;
    }

    public double[] getPseudoAxesValues() {
        return pseudoAxesValues;
    }

    public void setPseudoAxesValues(final double[] pseudoAxesValues) {
        this.pseudoAxesValues = pseudoAxesValues;
        for (int i = 0; i < pseudoAxesValues.length; i++) {
            reachTargetBean[i].setPViewersLabelValues(pseudoAxesValues[i]);
        }
    }

    @Override
    protected void loadPreferences(final Preferences arg0) {
        // NOT USE
    }

    @Override
    protected void savePreferences(final Preferences arg0) {
        // NOT USE
    }
}
