/**
 * 
 */

package fr.soleil.bean.diffractometer.gui.util;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.prefs.Preferences;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.AttributeProxy;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.DeviceProxyFactory;
import fr.soleil.bean.diffractometer.model.AxesFormat;
import fr.soleil.bean.diffractometer.model.ContextModel;
import fr.soleil.bean.diffractometer.model.DiffractometerModel;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.scalarbox.NumberScalarBox;
import fr.soleil.comete.swing.AutoScrolledTextField;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.WheelSwitch;
import fr.soleil.comete.tango.data.service.TangoKey;

/**
 * @author HARDION
 */
public class AxesBean extends AbstractTangoBox implements TangoConstants {

    private static final long serialVersionUID = 2252456756861244161L;

    private static final Logger LOGGER = LoggerFactory.getLogger(AxesBean.class);

    private static final Font DEFAULT_FONT = new Font("Dialog", Font.PLAIN, 14);
    private static final String SOFT_LIMIT_REAL_AXIS_PROXIES = "SoftLimitRealAxisProxies";
    private ScheduledExecutorService executor;
    private ScheduledFuture<?> future;
    private DiffractometerModel model;
    private String[][] pseudoAxis;
    private AutoScrolledTextField[] pViewers = null;
    private WheelSwitch[] pEditors = null;
    private Label[] pMinLabels = null;
    private Label[] pMaxLabels = null;
    private JPanel[] panels = null;
    private Runnable minMaxRefreshee = null;
    private final Label errorLabel = new Label();
    private final NumberScalarBox numberBox = new NumberScalarBox();
    private Map<String, String> result;

    private class RefreshMinMax implements Runnable {

        private final DecimalFormat decimalFormat = new DecimalFormat("#,##0.0");
        private final String[] namesSimuAttribute;

        public RefreshMinMax(final String[] namesSimuAttribute) throws DevFailed {
            this.namesSimuAttribute = namesSimuAttribute;
        }

        private Map<String, String> buildMinMaxAttributeMap() {
            String diffractoDeviceName = ContextModel.getRealDiffractometer();
            String[] values = null;
            try {
                DeviceProxy dp = DeviceProxyFactory.get(diffractoDeviceName);
                DbDatum dbDatum = dp.get_property(SOFT_LIMIT_REAL_AXIS_PROXIES);
                values = dbDatum.extractStringArray();
            } catch (DevFailed e) {
                LOGGER.error(DevFailedUtils.toString(e), e);
            }
            if (values != null) {
                result = new HashMap<>();
                for (int i = 0; i < values.length; i++) {
                    String value = values[i];
                    String[] val = value.split(":");
                    if (val.length == 2) {
                        result.put(val[0].toLowerCase(), val[1]);
                    }
                }
            }
            return result;
        }

        @Override
        public void run() {
            final int lengthNames = namesSimuAttribute.length;
            AttributeProxy[] simuProxy = new AttributeProxy[lengthNames];
            double maxValue;
            double minValue;
            AttributeProxy att_min = null;
            AttributeProxy att_max = null;

            Map<String, String> minMaxAttributeMap = buildMinMaxAttributeMap();

            for (int i = 0; i < lengthNames; i++) {
                try {
                    simuProxy[i] = new AttributeProxy(namesSimuAttribute[i]);
                    String attrName = simuProxy[i].name().split("Axis")[1];
                    String minAttr = minMaxAttributeMap.get(attrName.toLowerCase() + "_min");
                    String maxAttr = minMaxAttributeMap.get(attrName.toLowerCase() + "_max");
                    att_min = new AttributeProxy(minAttr);
                    att_max = new AttributeProxy(maxAttr);
                    minValue = att_min.read().extractDouble();
                    maxValue = att_max.read().extractDouble();
                    pMinLabels[i].setText(decimalFormat.format(minValue));
                    pMaxLabels[i].setText(decimalFormat.format(maxValue));
                    pEditors[i].setMaxValue(maxValue);
                    pEditors[i].setMinValue(minValue);
                } catch (DevFailed e) {
                    LOGGER.error(DevFailedUtils.toString(e), e);
                }
            }
        }
    }

    /**
     * This is the default constructor
     */
    public AxesBean() {
        super();

        initialize();

    }

    public double[] getAEditorValues() {
        final double[] res = new double[pEditors.length];
        int i = 0;
        for (final WheelSwitch pEditor : pEditors) {
            res[i] = pEditor.getValue();
            i++;
        }
        return res;
    }

    public void setAEditorValues(final double[] res) {
        int i = 0;
        for (final WheelSwitch pEditor : pEditors) {
            pEditor.setValue(res[i], false, true);
            i++;
        }
    }

    /**
     * This method initializes this
     * 
     * @return void
     */
    private void initialize() {
        setLayout(new GridBagLayout());
        final GridBagConstraints fullConstraints = new GridBagConstraints();
        fullConstraints.fill = GridBagConstraints.BOTH;
        fullConstraints.gridx = 0;
        fullConstraints.gridy = 0;
        fullConstraints.weightx = 1;
        fullConstraints.weighty = 1;
        add(errorLabel, fullConstraints);
        errorLabel.setText("Problem with Axes Attributes");
    }

    @Override
    protected void clearGUI() {
        removeAll();
        if (pViewers != null) {
            for (int i = 0; i < pViewers.length; i++) {
                if (pViewers[i] != null) {
                    cleanWidget(pViewers[i]);
                    pViewers[i] = null;
                }
            }
            pViewers = null;
        }

        if (pEditors != null) {
            for (int i = 0; i < pEditors.length; i++) {
                if (pEditors[i] != null) {
                    cleanWidget(pEditors[i]);
                    pEditors[i] = null;
                }
            }
            pEditors = null;
        }
        if (pMinLabels != null) {
            for (int i = 0; i < pMinLabels.length; i++) {
                if (pMinLabels[i] != null) {
                    cleanWidget(pMinLabels[i]);
                    pMinLabels[i] = null;
                }
            }
            pMinLabels = null;
        }
        if (pMaxLabels != null) {
            for (int i = 0; i < pMaxLabels.length; i++) {
                if (pMaxLabels[i] != null) {
                    cleanWidget(pMaxLabels[i]);
                    pMaxLabels[i] = null;
                }
            }
            pMaxLabels = null;
        }
        if (future != null) {
            future.cancel(true);
        }
        if (executor != null) {
            executor.shutdownNow();
        }
    }

    @Override
    protected void refreshGUI() {
        clearGUI();
        validate();
        try {
            model = new DiffractometerModel();
            model.setDiffractometer(getModel());
            pseudoAxis = model.getRealAxis();
            String[] proxiesReal = null;
            String[] proxiesSimu = null;
            int pseudoLength = 0;
            final int eulerianPseudoLength = 0;
            final int nextY = 0;
            int nextX = 0;
            if (pseudoAxis != null) {
                pseudoLength = pseudoAxis.length;
                final Label[] labels = new Label[pseudoLength];

                pViewers = new AutoScrolledTextField[pseudoLength];
                pEditors = new WheelSwitch[pseudoLength];
                pMinLabels = new Label[pseudoLength];
                pMaxLabels = new Label[pseudoLength];

                panels = new JPanel[pseudoLength];

                final String[][] realAxisProxis = model.getRealAxisProxies();
                proxiesReal = new String[realAxisProxis.length];
                proxiesSimu = new String[realAxisProxis.length];
                for (int i = 0; i < pseudoLength; i++) {
                    for (int j = 0; j < realAxisProxis.length; j++) {
                        if (pseudoAxis[i][0].equalsIgnoreCase(realAxisProxis[j][0])) {
                            proxiesReal[i] = realAxisProxis[j][1];
                        }
                    }
                }

                for (int i = 0; i < pseudoLength; i++) {
                    final String nameSimuProxy = getModel() + TANGO_SEPARATOR + pseudoAxis[i][1];
                    proxiesSimu[i] = nameSimuProxy;
                    panels[i] = new JPanel();
                    panels[i].setLayout(new GridBagLayout());
                    final Label minLabel = new Label();
                    minLabel.setText("Min ");
                    final Label maxLabel = new Label();
                    maxLabel.setText("Max ");
                    labels[i] = new Label();
                    labels[i].setHorizontalAlignment(JLabel.CENTER);

                    String displayName = pseudoAxis[i][0];
                    try {
                        String aliasName = ApiUtil.get_db_obj().get_alias_from_attribute(nameSimuProxy);
                        displayName = aliasName;
                    } catch (DevFailed e) {
                        // IGNORE because we already have displayName setted to a default value
                    }
                    labels[i].setText(displayName + " ");
                    final GridBagConstraints viewerPanelConstraints1 = new GridBagConstraints();
                    viewerPanelConstraints1.fill = GridBagConstraints.BOTH;
                    viewerPanelConstraints1.gridx = 0;
                    viewerPanelConstraints1.gridy = 1;
                    viewerPanelConstraints1.weightx = 1;
                    panels[i].add(labels[i], viewerPanelConstraints1);

                    pViewers[i] = new AutoScrolledFormattedTextField();
                    pViewers[i].setAutoscrolls(false);
                    pViewers[i].setFont(DEFAULT_FONT);
                    pViewers[i].setHorizontalAlignment(JTextField.CENTER);

                    final TangoKey vKey = generateAttributeKey(pseudoAxis[i][1]);
                    setWidgetModel(pViewers[i], stringBox, vKey);

                    stringBox.setFormatEnabled(pViewers[i], false);
                    ((AutoScrolledFormattedTextField) pViewers[i])
                            .setFormat(AxesFormat.getInstance().getFormat(pseudoAxis[i][1]));

                    final GridBagConstraints viewerPanelConstraints2 = new GridBagConstraints();
                    viewerPanelConstraints2.fill = GridBagConstraints.BOTH;
                    viewerPanelConstraints2.gridx = 0;
                    viewerPanelConstraints2.gridy = 2;
                    viewerPanelConstraints2.weightx = 1;
                    panels[i].add(pViewers[i], viewerPanelConstraints2);

                    pEditors[i] = new WheelSwitch();
                    pEditors[i].setFont(DEFAULT_FONT);
                    pEditors[i].setBackground(getBackground());
                    final TangoKey eKey = generateWriteAttributeKey(pseudoAxis[i][1]);
                    setWidgetModel(pEditors[i], numberBox, eKey);

                    numberBox.setFormatEnabled(pEditors[i], false);
                    pEditors[i].setFormat(AxesFormat.getInstance().getFormat(pseudoAxis[i][1]));

                    final GridBagConstraints viewerPanelConstraints3 = new GridBagConstraints();
                    viewerPanelConstraints3.fill = GridBagConstraints.NONE;
                    viewerPanelConstraints3.gridx = 0;
                    viewerPanelConstraints3.gridy = 3;
                    viewerPanelConstraints3.weightx = 1;
                    panels[i].add(pEditors[i], viewerPanelConstraints3);
                    final JPanel minMaxPanel = new JPanel();
                    minMaxPanel.setLayout(new BoxLayout(minMaxPanel, BoxLayout.LINE_AXIS));

                    minMaxPanel.add(Box.createHorizontalStrut(10));

                    minLabel.setFont(DEFAULT_FONT);
                    minMaxPanel.add(minLabel);
                    pMinLabels[i] = new Label();
                    pMinLabels[i].setFont(DEFAULT_FONT);
                    pMinLabels[i].setBackground(getBackground());

                    minMaxPanel.add(pMinLabels[i]);
                    minMaxPanel.add(Box.createHorizontalGlue());
                    maxLabel.setFont(DEFAULT_FONT);
                    minMaxPanel.add(maxLabel);

                    pMaxLabels[i] = new Label();
                    pMaxLabels[i].setFont(DEFAULT_FONT);
                    pMaxLabels[i].setBackground(getBackground());

                    minMaxPanel.add(pMaxLabels[i]);
                    minMaxPanel.add(Box.createHorizontalStrut(10));

                    final GridBagConstraints viewerPanelConstraints7 = new GridBagConstraints();
                    viewerPanelConstraints7.fill = GridBagConstraints.BOTH;
                    viewerPanelConstraints7.gridx = 0;
                    viewerPanelConstraints7.gridy = 4;
                    viewerPanelConstraints7.weightx = 1;
                    panels[i].add(minMaxPanel, viewerPanelConstraints7);
                    final GridBagConstraints viewer1Constraints = new GridBagConstraints();
                    viewer1Constraints.fill = GridBagConstraints.BOTH;
                    viewer1Constraints.gridx = nextX;
                    viewer1Constraints.gridy = nextY;
                    viewer1Constraints.weightx = 1;
                    viewer1Constraints.weighty = 2.0d / (pseudoLength + eulerianPseudoLength);
                    viewer1Constraints.insets = new Insets(0, 0, 2, 0);
                    add(new JScrollPane(panels[i]), viewer1Constraints);
                    nextX++;
                    pEditors[i].validate();
                }
            }

            minMaxRefreshee = new RefreshMinMax(proxiesSimu);
            executor = Executors.newScheduledThreadPool(1);
            future = executor.scheduleAtFixedRate(minMaxRefreshee, 0L, getRefreshingTime(), TimeUnit.MILLISECONDS);

            if (getComponentCount() == 0) {
                final GridBagConstraints fullConstraints = new GridBagConstraints();
                fullConstraints.fill = GridBagConstraints.BOTH;
                fullConstraints.gridx = 0;
                fullConstraints.gridy = 0;
                fullConstraints.weightx = 1;
                fullConstraints.weighty = 1;
                add(errorLabel, fullConstraints);
            }
        } catch (final Throwable t) {
            LOGGER.error(t.getMessage(), t);
        }
    }

    @Override
    protected void loadPreferences(final Preferences arg0) {
        // NOT USE
    }

    @Override
    protected void savePreferences(final Preferences arg0) {
        // NOT USE
    }

    @Override
    protected void onConnectionError() {
        // NOT USE
    }
}
