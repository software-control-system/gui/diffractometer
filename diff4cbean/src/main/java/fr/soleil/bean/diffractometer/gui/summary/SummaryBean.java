package fr.soleil.bean.diffractometer.gui.summary;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.prefs.Preferences;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableCellRenderer;

import org.jdesktop.swingx.JXTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.AttributeProxy;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.DeviceProxyFactory;
import fr.soleil.bean.diffractometer.gui.util.CellDoubleEditor;
import fr.soleil.bean.diffractometer.gui.util.CellDoubleRenderer;
import fr.soleil.bean.diffractometer.gui.util.TangoConstants;
import fr.soleil.bean.diffractometer.model.AxesFormat;
import fr.soleil.bean.diffractometer.model.ContextModel;
import fr.soleil.bean.diffractometer.model.DiffractometerModel;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.scalar.IBooleanTarget;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.tango.clientapi.InsertExtractUtils;

/**
 * 
 * @author fourneau
 * 
 */
public class SummaryBean extends AbstractTangoBox implements TangoConstants {

    private static final long serialVersionUID = -8974948752605104564L;

    private static final String ATTRIBUTE_AXES_NAMES = "AxesNames";
    private static final String PROPERTY_REAL_DIFFRACTOMETER_PROXY = "RealDiffractometerProxy";
    private static final String ATTRIBUTE_REAL_PSEUDO_AXIS_PROXY = "realPseudoAxisProxy";
    private static final String ATTRIBUTE_PSEUDO_AXIS_NAMES = "pseudoAxisNames";
    private static final String ATTRIBUTE_PSEUDO_AXES_PROXIES = "pseudoAxesProxies";

    private static final Logger LOGGER = LoggerFactory.getLogger(SummaryBean.class);

    private JTable resumeTable;
    private final List<String> attributes = new LinkedList<>();
    private ScheduledExecutorService executor;
    private ScheduledFuture<?> future;
    private SummaryTableModel resumeTableModel;
    String realDiffractometer;
    private final Color BLUE_COLOR = new Color(232, 240, 247);

    public SummaryBean() {
        super();

        try {
            initialize();
        } catch (final DevFailed e) {
            add(new JLabel("An error occur"));
            LOGGER.error(DevFailedUtils.toString(e), e);
        }
    }

    private void initialize() throws DevFailed {
        setLayout(new BorderLayout());
        add(new JScrollPane(getResumeTable(), JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
    }

    private JTable getResumeTable() {
        final Color color = new Color(175, 180, 175);

        if (resumeTable == null) {
            resumeTable = new JTable();

            resumeTable.getTableHeader().setReorderingAllowed(false);
            resumeTable.setAutoResizeMode(JXTable.AUTO_RESIZE_ALL_COLUMNS);
            resumeTable.setDefaultRenderer(Double.class, new CellDoubleRenderer() {

                private static final long serialVersionUID = -4396325427608491847L;

                @Override
                public JTextField getTableCellRendererComponent(final JTable table, final Object value,
                        final boolean isSelected, final boolean hasFocus, final int row, final int column) {
                    SummaryTableModel tableModel = (SummaryTableModel) table.getModel();
                    JTextField jtf = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
                            column);
                    jtf.setBorder(null);
                    String formatStr = tableModel.getFormatFor(row);
                    String valueString = String.format(Locale.US, formatStr, value);
                    jtf.setText(valueString);
                    jtf.setToolTipText(valueString);

                    jtf.setBackground(tableModel.getColorFor(row));
                    if (isSelected) {
                        jtf.setBackground(color);
                    }
                    return jtf;
                }

            });

            resumeTable.setDefaultEditor(Double.class, new CellDoubleEditor());

            resumeTable.setDefaultRenderer(String.class, new DefaultTableCellRenderer() {

                private static final long serialVersionUID = 6780721270371212827L;

                @Override
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                        boolean hasFocus, int row, int column) {

                    JLabel comp = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
                            column);
                    comp.setOpaque(true);
                    SummaryTableModel tableModel = (SummaryTableModel) table.getModel();
                    comp.setBackground(tableModel.getColorFor(row));
                    if (isSelected) {
                        comp.setBackground(color);
                    }
                    return comp;
                }

            });
        }
        return resumeTable;
    }

    @Override
    protected void clearGUI() {
        removeAll();
        resumeTable = null;
        if (future != null) {
            future.cancel(true);
        }
        if (executor != null) {
            executor.shutdownNow();
        }
    }

    private IBooleanTarget generateVirtualBooleanTarget() {
        IBooleanTarget result = new IBooleanTarget() {
            @Override
            public void addMediator(Mediator<?> arg0) {
            }

            @Override
            public void removeMediator(Mediator<?> arg0) {
            }

            @Override
            public boolean isSelected() {
                return false;
            }

            @Override
            public void setSelected(boolean arg0) {
                getResumeTable().repaint();
            }

        };
        return result;
    }

    @Override
    protected void refreshGUI() {

        TangoKey freezeKey = new TangoKey();
        realDiffractometer = ContextModel.getRealDiffractometer();

        TangoKeyTool.registerAttribute(freezeKey, realDiffractometer, "freeze");
        IBooleanTarget virtualTarget = generateVirtualBooleanTarget();
        booleanBox.connectWidget(virtualTarget, freezeKey);

        final DeviceProxy dp;
        final String[] pseudoAxesProxies;
        final String[] realAxisProxies;

        DiffractometerModel diffModel = new DiffractometerModel();
        diffModel.setDiffractometer(getModel());
        String headerDefaultModeString = ObjectUtils.EMPTY_STRING;
        if (diffModel.getStatus() != null) {
            headerDefaultModeString = diffModel.getStatus().toLowerCase();
        }
        resumeTableModel = new SummaryTableModel(headerDefaultModeString);

        getResumeTable().setModel(resumeTableModel);
        // hide the first and the last columns
        getResumeTable().removeColumn(resumeTable.getColumnModel().getColumn(SummaryTableModel.FORMAT_INDEX));
        getResumeTable().removeColumn(resumeTable.getColumnModel().getColumn(SummaryTableModel.ENGINE_INDEX));

        resumeTable.getColumnModel().getColumn(SummaryTableModel.PREPARE_READ_INDEX)
                .setCellEditor(new CellDoubleEditor());

        resumeTable.getColumnModel().getColumn(SummaryTableModel.PREPARE_READ_INDEX).getCellEditor()
                .addCellEditorListener(new CellEditorListener() {
                    @Override
                    public void editingStopped(final ChangeEvent e) {
                        final CellDoubleEditor jtf = (CellDoubleEditor) e.getSource();
                        final int selectedRow = resumeTable.getSelectedRow();
                        final String attr = attributes.get(selectedRow);
                        final DeviceAttribute da = new DeviceAttribute(attr.split(TANGO_SEPARATOR)[3]);
                        da.insert(Double.valueOf(jtf.getCellEditorValue().toString()));
                        AttributeProxy ap;

                        try {
                            ap = new AttributeProxy(attr);
                            ap.write(da);
                        } catch (final DevFailed e1) {
                            JOptionPane.showMessageDialog(SwingUtilities.getRootPane(resumeTable),
                                    DevFailedUtils.toString(e1), "Pseudo Axis Error", JOptionPane.WARNING_MESSAGE);
                            editingCanceled(e);
                        }
                    }

                    @Override
                    public void editingCanceled(final ChangeEvent e) {
                        // NOTHING TO DO
                    }
                });

        Runnable r = null;
        try {
            dp = DeviceProxyFactory.get(getModel());
            final DeviceAttribute daPseudoAxesProxies = dp.read_attribute(ATTRIBUTE_PSEUDO_AXES_PROXIES);
            pseudoAxesProxies = daPseudoAxesProxies.extractStringArray();
            final DeviceAttribute dbd = dp.read_attribute(ATTRIBUTE_AXES_NAMES);
            realAxisProxies = dbd.extractStringArray();
            DbDatum realDiffProperty = dp.get_property(PROPERTY_REAL_DIFFRACTOMETER_PROXY);
            String realDiff = realDiffProperty.extractString();
            // realDiff = PIL/K6C/DIFFRACTOMETER
            final DeviceProxy realDiffProxy = DeviceProxyFactory.get(realDiff);
            final boolean isRealDiffExported = realDiffProxy.get_info().exported;
            final DeviceProxy diffProxy = isRealDiffExported ? realDiffProxy : DeviceProxyFactory.get(getModel());
            if (!isRealDiffExported) {
                getResumeTable()
                        .removeColumn(resumeTable.getColumnModel().getColumn(SummaryTableModel.PILOT_INDEX - 1));
            }

            r = new Runnable() {

                @Override
                public void run() {
                    // COLOR STUFF
                    List<String> pseudoAxisList = new ArrayList<>(Arrays.asList(pseudoAxesProxies));
                    final Color[] colorsToUse = new Color[pseudoAxisList.size() + 1];
                    for (int i = 0; i < colorsToUse.length; i++) {
                        if (i % 2 == 1) {
                            colorsToUse[i] = Color.WHITE;
                        } else {
                            colorsToUse[i] = BLUE_COLOR;
                        }
                    }
                    int colorIndex = -1;

                    for (final String pseudoAxesProxy : pseudoAxesProxies) {
                        try {
                            final DeviceProxy dev = DeviceProxyFactory.get(pseudoAxesProxy);
                            final DeviceAttribute daPseudoAxisNames = dev.read_attribute(ATTRIBUTE_PSEUDO_AXIS_NAMES);
                            final String[] pseudoAxisNames = daPseudoAxisNames.extractStringArray();
                            colorIndex++;
                            for (String pseudoAxisName : pseudoAxisNames) {

                                try {
                                    double realValue = Double.NaN;
                                    double readSim = Double.NaN, writeSim = Double.NaN;
                                    final String pseudoAxisNameAttribute = pseudoAxesProxy + TANGO_SEPARATOR
                                            + pseudoAxisName;
                                    final AttributeProxy pa = new AttributeProxy(pseudoAxisNameAttribute);
                                    readSim = (Double) InsertExtractUtils.extractRead(pa.read(), AttrDataFormat.SCALAR);
                                    writeSim = (Double) InsertExtractUtils.extractWrite(pa.read(),
                                            AttrWriteType.READ_WRITE, AttrDataFormat.SCALAR);

                                    DbDatum realPseudoAxisProperty = dev.get_property(ATTRIBUTE_REAL_PSEUDO_AXIS_PROXY);
                                    String realPseudoAxis = realPseudoAxisProperty.extractString();
                                    AttributeProxy paReal = new AttributeProxy(
                                            realPseudoAxis + TANGO_SEPARATOR + pseudoAxisName);
                                    String format = AxesFormat.getInstance().getFormat(pseudoAxisName);
                                    if (format == null) {
                                        format = pa.get_info_ex().format;
                                    }
                                    DeviceProxy realPseudoAxisProxy = DeviceProxyFactory.get(realPseudoAxis);
                                    DbDatum engineNameProperty = realPseudoAxisProxy.get_property("EngineName");
                                    String mode = engineNameProperty.extractString();
                                    realValue = paReal.read().extractDouble();
                                    final int index = resumeTableModel.indexOf(mode, pseudoAxisName);
                                    if (index == -1) {
                                        attributes.add(pseudoAxisNameAttribute);
                                        resumeTableModel.addRow(colorsToUse[colorIndex], mode, pseudoAxisName,
                                                realValue, readSim, writeSim, format);
                                    } else {

                                        Double oldValue = (Double) resumeTableModel.getValueAt(index,
                                                SummaryTableModel.PILOT_INDEX);
                                        if (oldValue.doubleValue() != realValue) {
                                            resumeTableModel.setValueAt(realValue, index,
                                                    SummaryTableModel.PILOT_INDEX);
                                        }
                                        oldValue = (Double) resumeTableModel.getValueAt(index,
                                                SummaryTableModel.PREPARE_READ_INDEX);
                                        if (oldValue.doubleValue() != readSim) {
                                            resumeTableModel.setValueAt(readSim, index,
                                                    SummaryTableModel.PREPARE_READ_INDEX);
                                        }
                                        oldValue = (Double) resumeTableModel.getValueAt(index,
                                                SummaryTableModel.PREPARE_WRITE_INDEX);
                                        if (oldValue.doubleValue() != writeSim) {
                                            resumeTableModel.setValueAt(writeSim, index,
                                                    SummaryTableModel.PREPARE_WRITE_INDEX);
                                        }
                                    }
                                } catch (final DevFailed e) {
                                    LOGGER.error(DevFailedUtils.toString(e));
                                }
                            }
                        } catch (final DevFailed e) {
                            LOGGER.error(DevFailedUtils.toString(e));
                        }
                    }

                    colorIndex++;
                    // AxisMu-Komega-kappa-Kphi-Gamma-Delta-Mu
                    for (final String realAxisProxy : realAxisProxies) {
                        try {
                            double realValue = Double.NaN, readSim = Double.NaN, writeSim = Double.NaN;
                            String displayName = ObjectUtils.EMPTY_STRING;
                            String axisName = ObjectUtils.EMPTY_STRING;
//                            final String[] split = realAxisProxy.split(":");

                            final DeviceAttribute axisAttribute = dp.read_attribute(realAxisProxy);
                            axisName = axisAttribute.getName();
                            readSim = (Double) InsertExtractUtils.extractRead(axisAttribute, AttrDataFormat.SCALAR);
                            writeSim = (Double) InsertExtractUtils.extractWrite(axisAttribute, AttrWriteType.READ_WRITE,
                                    AttrDataFormat.SCALAR);
                            DeviceAttribute realValueAttr = diffProxy.read_attribute(realAxisProxy);
                            String format = AxesFormat.getInstance().getFormat(realAxisProxy);
                            if (format == null) {
                                AttributeProxy paReal = new AttributeProxy(getModel() + TANGO_SEPARATOR + axisName);
                                format = paReal.get_info_ex().format;
                            }

                            realValue = realValueAttr.extractDouble();
                            displayName = diffProxy.get_attribute_info(realAxisProxy).label;
                            final int index = resumeTableModel.indexOf(ObjectUtils.EMPTY_STRING, displayName);
                            if (index == -1) {
                                attributes.add(getModel() + TANGO_SEPARATOR + axisName);
                                resumeTableModel.addRow(colorsToUse[colorIndex], ObjectUtils.EMPTY_STRING, displayName,
                                        realValue, readSim, writeSim, format);
                            } else {
                                if ((Double) resumeTableModel.getValueAt(index,
                                        SummaryTableModel.PILOT_INDEX) != realValue) {
                                    resumeTableModel.setValueAt(realValue, index, SummaryTableModel.PILOT_INDEX);

                                }
                                if ((Double) resumeTableModel.getValueAt(index,
                                        SummaryTableModel.PREPARE_READ_INDEX) != readSim) {
                                    resumeTableModel.setValueAt(readSim, index, SummaryTableModel.PREPARE_READ_INDEX);
                                }
                                if ((Double) resumeTableModel.getValueAt(index,
                                        SummaryTableModel.PREPARE_WRITE_INDEX) != writeSim) {
                                    resumeTableModel.setValueAt(writeSim, index, SummaryTableModel.PREPARE_WRITE_INDEX);
                                }
                            }
                        } catch (final DevFailed e) {
                            LOGGER.error(DevFailedUtils.toString(e));
                        }
                    }

                }
            };
        } catch (final DevFailed e1) {
            LOGGER.error("devfailed", e1);
            LOGGER.error(DevFailedUtils.toString(e1));
        }
        executor = Executors.newScheduledThreadPool(1);
        if (r != null) {
            future = executor.scheduleAtFixedRate(r, 0L, getRefreshingTime(), TimeUnit.MILLISECONDS);
        }
    }

    @Override
    protected void onConnectionError() {
        throw new UnsupportedOperationException("Not supported yet.");

    }

    @Override
    protected void loadPreferences(final Preferences arg0) {
        // NOT USE
    }

    @Override
    protected void savePreferences(final Preferences arg0) {
        // NOT USE
    }
}
