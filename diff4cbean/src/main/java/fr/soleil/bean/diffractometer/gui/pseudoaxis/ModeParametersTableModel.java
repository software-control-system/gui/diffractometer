package fr.soleil.bean.diffractometer.gui.pseudoaxis;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.AttributeProxy;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.soleil.bean.diffractometer.gui.util.TangoConstants;

/**
 * 
 * This class shows data of reflection from 1 crystal.
 * 
 * Only compatible with Diffractometer device v2
 * 
 * @author Fourneau
 * @version 3.0
 * 
 */

public class ModeParametersTableModel extends AbstractTableModel implements TangoConstants {

    private static final long serialVersionUID = 2704007100986352619L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ModeParametersTableModel.class);

    private final String[] titles;
    private final AttributeProxy attrProxy;

    public ModeParametersTableModel(final String device, final String shortNameAttribute, final String[] titles)
            throws DevFailed {
        this.attrProxy = new AttributeProxy(device + TANGO_SEPARATOR + shortNameAttribute);
        this.titles = titles;
    }

    @Override
    public int getRowCount() {
        return 1;
    }

    @Override
    public int getColumnCount() {
        return this.titles.length;
    }

    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
        DeviceAttribute da;
        try {
            da = this.attrProxy.read();
            final double[] data = da.extractDoubleArray();
            final int dimX = da.getDimX();
            String result;
            try {
                int columnIndexToUse = columnIndex;

                Object dataValue = data[dimX * rowIndex + columnIndexToUse];
                result = String.valueOf(dataValue);

            } catch (IndexOutOfBoundsException iobe) {
                result = null;
            }
            return result;

        } catch (final DevFailed e) {
            LOGGER.error(DevFailedUtils.toString(e), e);
        }
        return null;
    }

    @Override
    public void setValueAt(final Object aValue, final int rowIndex, final int columnIndex) {
        try {
            final DeviceAttribute da = this.attrProxy.read();
            double[] data = da.extractDoubleArray();
            final int dimX = da.getDimX();

            data = ArrayUtils.subarray(data, 0, dimX);
            data[columnIndex] = Double.parseDouble((String) aValue);
            da.insert(data);

            attrProxy.write(da);
            this.fireTableDataChanged();
        } catch (final DevFailed e) {
            JOptionPane.showMessageDialog(null, DevFailedUtils.toString(e), "Mode Modification Error",
                    JOptionPane.WARNING_MESSAGE);
        }
    }

    @Override
    public String getColumnName(final int column) {
        if (column < this.titles.length) {
            return this.titles[column];
        }
        return "";
    }

    @Override
    public boolean isCellEditable(final int rowIndex, final int columnIndex) {
        return true;
    }

}
