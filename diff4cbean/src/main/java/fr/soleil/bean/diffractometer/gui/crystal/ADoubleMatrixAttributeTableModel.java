package fr.soleil.bean.diffractometer.gui.crystal;

import java.awt.Component;
import java.lang.ref.WeakReference;
import java.util.concurrent.ExecutionException;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import javax.swing.table.AbstractTableModel;

import org.slf4j.Logger;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.AttributeProxy;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.soleil.bean.diffractometer.gui.util.TangoConstants;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.WindowSwingUtils;
import fr.soleil.lib.project.swing.dialog.ProgressDialog;

public abstract class ADoubleMatrixAttributeTableModel extends AbstractTableModel implements TangoConstants {

    private static final long serialVersionUID = 2461008184299090335L;

    private static final String THE_INPUTED_VALUE_IS_NOT_VALID = "The inputed value is not valid";
    private static final String THE_INPUTED_VALUE = "The inputed value ";
    private static final String IS_NOT_VALID = " is not valid";
    protected static final String WRITING_ATTRIBUTE = "Writing %s";
    private static final String UNEXPECTED_ERROR = "Unexpected error while writing %s";
    private static final String UNEXPECTED_ERROR_WHILE_WRITING_ATTRIBUTE = UNEXPECTED_ERROR + ":\n%s";
    protected static final double[] NO_VALUE = new double[0];

    protected final WeakReference<Component> parentRef;
    protected ProgressDialog progressDialog;
    protected final String writingAttribute;
    protected final String[] titles;
    protected final AttributeProxy attrProxy;
    protected final String device;
    protected volatile AValueWriter lastValueWriter;
    protected volatile boolean forceRead;
    protected final Object dataLock;
    protected volatile TableData lastData;

    public ADoubleMatrixAttributeTableModel(final Component parent, final String device,
            final String shortNameAttribute, final String... titles) throws DevFailed {
        this.device = device;
        this.attrProxy = new AttributeProxy(device + TANGO_SEPARATOR + shortNameAttribute);
        this.titles = titles;
        this.parentRef = parent == null ? null : new WeakReference<>(parent);
        this.writingAttribute = String.format(WRITING_ATTRIBUTE, attrProxy.fullName());
        this.lastValueWriter = null;
        this.forceRead = false;
        this.dataLock = new Object();
        this.lastData = new TableData(0, 0, 0, NO_VALUE);
    }

    protected abstract Logger getLogger();

    protected abstract String getWriteErrorMessage();

    protected boolean shouldRead(TableData lastData) {
        return forceRead || (lastData == null) || (System.currentTimeMillis() - lastData.getReadDate() > 1000);
    }

    protected void checkRead() {
        if (shouldRead(lastData)) {
            synchronized (dataLock) {
                if (shouldRead(lastData)) {
                    try {
                        DeviceAttribute da = attrProxy.read();
                        this.lastData = new TableData(System.currentTimeMillis(), da.getDimX(), da.getDimY(),
                                da.extractDoubleArray());
                    } catch (final DevFailed e) {
                        this.lastData = new TableData(System.currentTimeMillis(), 0, 0, NO_VALUE);
                        getLogger().error(DevFailedUtils.toString(e), e);
                    } finally {
                        forceRead = false;
                    }
                }
            }
        }
    }

    protected ProgressDialog getProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(
                    WindowSwingUtils.getWindowForComponent(ObjectUtils.recoverObject(parentRef)));
            progressDialog.setTitle(writingAttribute);
            progressDialog.setMainMessage(writingAttribute);
            progressDialog.setProgressIndeterminate(true);
        }
        return progressDialog;
    }

    protected abstract AValueWriter generateValueWriter(Object value, int row, int column);

    @Override
    public int getRowCount() {
        checkRead();
        return lastData.getDimY();
    }

    @Override
    public void setValueAt(final Object value, final int rowIndex, final int columnIndex) {
        AValueWriter lastWriter = this.lastValueWriter;
        if (lastWriter != null) {
            lastWriter.setCanceled(true);
        }
        ProgressDialog progressDialog = getProgressDialog();
        if (progressDialog.isVisible()) {
            progressDialog.setVisible(false);
        }
        progressDialog.setProgressIndeterminate(true);
        AValueWriter writer = generateValueWriter(value, rowIndex, columnIndex);
        this.lastValueWriter = writer;
        progressDialog.setCancelable((ICancelable) writer);
        progressDialog.setMainMessage(writingAttribute);
        progressDialog.pack();
        progressDialog.setSize(progressDialog.getWidth(), Math.max(progressDialog.getHeight(), 150));
        progressDialog.setLocationRelativeTo(ObjectUtils.recoverObject(parentRef));
        progressDialog.setVisible(true);
        writer.execute();
    }

    @Override
    public void fireTableDataChanged() {
        forceRead = true;
        super.fireTableDataChanged();
    }

    @Override
    public void fireTableRowsDeleted(int firstRow, int lastRow) {
        forceRead = true;
        super.fireTableRowsDeleted(firstRow, lastRow);
    }

    @Override
    public void fireTableStructureChanged() {
        forceRead = true;
        super.fireTableStructureChanged();
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class TableData {
        private int dimX, dimY;
        private double[] value;
        private long readDate;

        public TableData(long readDate, int dimX, int dimY, double... value) {
            super();
            this.readDate = readDate;
            this.dimX = dimX;
            this.dimY = dimY;
            this.value = value;
        }

        public int getDimX() {
            return dimX;
        }

        public int getDimY() {
            return dimY;
        }

        public double[] getValue() {
            return value;
        }

        public long getReadDate() {
            return readDate;
        }

        @Override
        protected void finalize() {
            value = null;
        }
    }

    protected abstract class AValueWriter extends SwingWorker<Void, Void> implements ICancelable {

        protected Object value;
        protected int row, column;
        protected volatile boolean canceled;

        public AValueWriter(Object value, int row, int column) {
            super();
            this.value = value;
            this.row = row;
            this.column = column;
            this.canceled = false;
        }

        @Override
        protected void done() {
            if (!canceled) {
                if (getProgressDialog().isVisible()) {
                    getProgressDialog().setVisible(false);
                }
                try {
                    get();
                    fireTableDataChanged();
                } catch (InterruptedException e) {
                    // not managed
                } catch (ExecutionException e) {
                    Throwable error = e.getCause();
                    if (error instanceof NumberFormatException) {
                        String message = THE_INPUTED_VALUE + value + IS_NOT_VALID;
                        getLogger().error(message, error);
                        JOptionPane.showMessageDialog(ObjectUtils.recoverObject(parentRef), message,
                                THE_INPUTED_VALUE_IS_NOT_VALID, JOptionPane.WARNING_MESSAGE);
                    } else if (error instanceof DevFailed) {
                        getLogger().error(getWriteErrorMessage(), error);
                        JOptionPane.showMessageDialog(ObjectUtils.recoverObject(parentRef),
                                DevFailedUtils.toString((DevFailed) error), getWriteErrorMessage(),
                                JOptionPane.WARNING_MESSAGE);
                    } else {
                        getLogger().error(String.format(UNEXPECTED_ERROR, attrProxy.fullName()), error);
                        JOptionPane.showMessageDialog(ObjectUtils.recoverObject(parentRef),
                                String.format(UNEXPECTED_ERROR_WHILE_WRITING_ATTRIBUTE, attrProxy.fullName(),
                                        ObjectUtils.printStackTrace(error)),
                                getWriteErrorMessage(), JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        }

        @Override
        public boolean isCanceled() {
            return canceled;
        }

        @Override
        public void setCanceled(boolean canceled) {
            this.canceled = canceled;
        }

        @Override
        protected void finalize() {
            value = null;
        }
    }

}
