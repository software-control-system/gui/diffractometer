package fr.soleil.bean.diffractometer.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.util.Arrays;
import java.util.prefs.Preferences;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import javax.swing.plaf.SplitPaneUI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.DeviceProxyFactory;
import fr.soleil.bean.diffractometer.gui.action.CopyCrystalAction;
import fr.soleil.bean.diffractometer.gui.action.DeleteCrystalAction;
import fr.soleil.bean.diffractometer.gui.action.NewCrystalAction;
import fr.soleil.bean.diffractometer.gui.action.UseCrystalAction;
import fr.soleil.bean.diffractometer.gui.crystal.CrystalPanel;
import fr.soleil.bean.diffractometer.gui.pseudoaxis.PseudoAxesBean;
import fr.soleil.bean.diffractometer.gui.summary.SummaryBean;
import fr.soleil.bean.diffractometer.gui.util.AutoSelectText;
import fr.soleil.bean.diffractometer.gui.util.AxesBean;
import fr.soleil.bean.diffractometer.gui.util.SolutionTable;
import fr.soleil.bean.diffractometer.gui.util.TangoConstants;
import fr.soleil.bean.diffractometer.model.ContextModel;
import fr.soleil.bean.diffractometer.model.DiffractometerModel;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.box.matrixbox.StringMatrixBox;
import fr.soleil.comete.box.scalarbox.NumberScalarBox;
import fr.soleil.comete.box.util.DeviceLabel;
import fr.soleil.comete.swing.Button;
import fr.soleil.comete.swing.ComboBox;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.TabbedPane;
import fr.soleil.comete.swing.WheelSwitch;
import fr.soleil.comete.tango.data.service.AttributePropertyType;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.IKey;
import fr.soleil.data.target.matrix.ITextMatrixTarget;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.ArrayUtils;

/**
 * This code was generated using CloudGarden's Jigloo SWT/Swing GUI Builder,
 * which is free for non-commercial use. If Jigloo is being used commercially
 * (ie, by a corporation, company or business for any purpose whatever) then you
 * should purchase a license for each developer using Jigloo. Please visit
 * www.cloudgarden.com for details. Use of Jigloo implies acceptance of these
 * licensing terms. ************************************* A COMMERCIAL LICENSE
 * HAS NOT BEEN PURCHASED for this machine, so Jigloo or this code cannot be
 * used legally for any corporate or commercial purpose.
 * *************************************
 */
public class DiffractometerBean extends AbstractTangoBox implements TangoConstants {

    private static final long serialVersionUID = 5262364428652431398L;

    public static final String ATTRIBUTE_SOLUTIONS = "solutions";
    public static final String ATTRIBUTE_CRYSTAL_NAMES = "CrystalNames";
    public static final String ATTRIBUTE_CRYSTAL = "crystal";
    public static final String ATTRIBUTE_SOLUTIONS_IDX = "solutionsIdx";
    public static final String PROPERTY_REAL_DIFFRACTOMETER_PROXY = "RealDiffractometerProxy";
    public static final String PROPERTY_CRYSTAL_MANAGER_PROXY_NAME = "CrystalManagerProxyName";

    private static final String BEAN_STARTUP_ERRORS = "Bean Startup Errors";
    private static final String PILOT_CRYSTAL = "Pilot Crystal";
    private static final String VIRTUAL_CRYSTAL = "Virtual Crystal";
    private static final String CRYSTAL_LIST = "Crystal List";
    private static final String TEXT = "text";
    private static final String INDEX = "Index";
    private static final String WAVELENGTH = "Wavelength";

    private static final Logger LOGGER = LoggerFactory.getLogger(DiffractometerBean.class);

    public static Frame parent;

    private JPanel sourcePanel;
    private WheelSwitch simpleScalarViewerLambda;
    private JPanel leftPanel;
    private JPanel configPanel;
    private DeviceLabel diffractometerStatusBar;
    private CrystalPanel crystalPanel;
    private JPanel completeCrystalPanel;
    private JToolBar hklToolBar;
    private JPanel controlPanel;
    private TabbedPane controlTabbedPane;
    private JPanel solutionsPanel;
    private JDialog realAxesDialog;
    private Button realcloseButton;
    private ComboBox crystalComboBox;
//    private DefaultComboBoxModel<Object> comboModel;
    private SolutionTable solutionTable;
    private JScrollPane jScrollPaneLeftPanel;
    private JScrollPane jPanelComputeAngles;

    private String[] titles;
    private JPanel mainPanel;
    private JSplitPane rightSplitPane;
    private RealWordInteractionsBean autoUpdateFromProxiesModeBean;
    private AxesBean axesBean;
    private TargetAxesBeanPanel targetRealAxesBean;
    private PseudoAxesBean pseudoAxesBean;

    protected final static String RIGHT_SPLIT_LOCATION_KEY = "Diff4cJPanel.rightSplitLocation";
    protected boolean isFirstRefresh = true;
    protected Preferences preferences;

    private DiffractometerModel diffractometer;
    private double gapReflexion = Double.NaN;
    private SummaryBean resumeBean;
    private TabbedPane pseudoBean;
    private final StringMatrixBox stringMatrixBox = new StringMatrixBox();
    private final NumberScalarBox numberBox = new NumberScalarBox();
    private final NumberMatrixBox numberMatrixBox = new NumberMatrixBox();

    public static final String DIFFRACTOMETER_PREPARE_DEVICE = "DiffractometerPrepareDevice";
    public String[][] realAxis;
    public String[][] pseudoAxis;
    public String firstPseudoAxis;
    public WheelSwitch indexSelector;
    public DeviceProxy modelProxy;
    private JLabel pilotCrystalTitle, virtualCrystalTitle, crystalListTitle;
    private Label virtCrystalLabel;
    private Label realCrystalLabel;
    private Label lambdaUnit;
    private JPanel westPanel;

    public DiffractometerBean() {
        diffractometer = new DiffractometerModel();
        initialize();
        AutoSelectText.install();
    }

    @Override
    public void setModel(String model) {
        ContextModel.getInstance().setPrepareDiffractometer(model);
        try {
            modelProxy = DeviceProxyFactory.get(model);
            final String crystalManagerModel = modelProxy.get_property(PROPERTY_CRYSTAL_MANAGER_PROXY_NAME)
                    .extractString();
            final String realModel = modelProxy.get_property(PROPERTY_REAL_DIFFRACTOMETER_PROXY).extractString();
            ContextModel.getInstance().setPrepareDiffractometer(model);
            ContextModel.getInstance().setRealDiffractometer(realModel);
            ContextModel.getInstance().setCrystalManager(crystalManagerModel);
            diffractometer.setDiffractometer(model);
            ContextModel.getInstance().setVirtual(diffractometer.isVirtual());
            super.setModel(model);
            updateGUI();
        } catch (DevFailed e) {
            JOptionPane.showMessageDialog(this, DevFailedUtils.toString(e), BEAN_STARTUP_ERRORS,
                    JOptionPane.WARNING_MESSAGE);
        }
    }

    private void updateGUI() {
        autoUpdateFromProxiesModeBean.updateGUI();
    }

    @Override
    protected void clearGUI() {
        cleanWidget(simpleScalarViewerLambda);
        cleanWidget(solutionTable);
        cleanWidget(indexSelector);
    }

    protected void updateRealCrystalLabelBackground() {
        SwingUtilities.invokeLater(() -> {
            if ((realCrystalLabel != null) && (crystalComboBox != null)) {
                realCrystalLabel.setBackground(ObjectUtils.sameObject(String.valueOf(crystalComboBox.getSelectedItem()),
                        realCrystalLabel.getText()) ? Color.GREEN : Color.RED);
            }
        });
    }

    @Override
    protected void refreshGUI() {
        hklToolBar.removeAll();

        try {

            realAxis = diffractometer.getRealAxis();
            pseudoAxis = diffractometer.getPseudoAxis();
            firstPseudoAxis = pseudoAxis == null || pseudoAxis.length == 0 ? null : pseudoAxis[0][1];

            final IKey realCrystalKey = new TangoKey();
            TangoKeyTool.registerAttribute(realCrystalKey,
                    ContextModel.getRealDiffractometer() + TANGO_SEPARATOR + ATTRIBUTE_CRYSTAL);
            setWidgetModel(realCrystalLabel, stringBox, realCrystalKey);

            hklToolBar.add(pilotCrystalTitle, BorderLayout.WEST);

            hklToolBar.add(realCrystalLabel, BorderLayout.CENTER);

            if (diffractometer.isVirtual()) {
                hklToolBar.add(virtualCrystalTitle);
                final IKey virtCrystalKey = new TangoKey();
                TangoKeyTool.registerAttribute(virtCrystalKey,
                        ContextModel.getPrepareDiffractometer() + TANGO_SEPARATOR + ATTRIBUTE_CRYSTAL);
                setWidgetModel(virtCrystalLabel, stringBox, virtCrystalKey);
                hklToolBar.add(virtCrystalLabel);
            }

            // Crystal Selection
            hklToolBar.add(crystalListTitle);
            hklToolBar.add(getCrystalComboEditor());

            // Actions on CrystalManager
            JButton useInPilotButton = new JButton(new UseCrystalAction(ContextModel.getRealDiffractometer(),
                    ContextModel.getCrystalManager(), realCrystalLabel));

            hklToolBar.add(useInPilotButton);
            hklToolBar.add(new JButton(new NewCrystalAction(ContextModel.getCrystalManager())));
            hklToolBar.add(new JButton(new CopyCrystalAction(ContextModel.getCrystalManager())));
            hklToolBar.add(new JButton(new DeleteCrystalAction(ContextModel.getCrystalManager())));

            if (solutionTable == null) {
                getSolutionsTable();
            }

            // ---------------------------------------------------------------------------
            // CRYSTAL LIST ComboBox auto selection on update
            // ---------------------------------------------------------------------------
            final TangoKey crystalKey = new TangoKey();
            TangoKeyTool.registerAttribute(crystalKey,
                    ContextModel.getCrystalManager() + TANGO_SEPARATOR + ATTRIBUTE_CRYSTAL);
            final ITextTarget acquisitionCrystalTarget = new ITextTarget() {
                private String old = ObjectUtils.EMPTY_STRING;

                @Override
                public void addMediator(final Mediator<?> mediator) {
                    // NOT USE
                }

                @Override
                public void removeMediator(final Mediator<?> mediator) {
                    // NOT USE
                }

                @Override
                public String getText() {
                    return old;
                }

                @Override
                public void setText(final String text) {
                    if (old == null) {
                        old = ObjectUtils.EMPTY_STRING;
                    }
                    if (text != null && !old.equals(text)) {
//                        crystalComboBox.setSelectedItem(text);
                        DiffractometerModel model = new DiffractometerModel();
                        model.setDiffractometer(ContextModel.getPrepareDiffractometer());
                        Color colorToSet;
                        if (DiffractometerModel.VIRTUAL_STATUS.equals(model.getStatus())) {
                            try {
                                modelProxy.write_attribute(new DeviceAttribute(ATTRIBUTE_CRYSTAL, text));
                                colorToSet = Color.GREEN;
                            } catch (DevFailed e) {
//                                JOptionPane.showMessageDialog(DiffractometerBean.this, DevFailedUtils.toString(e),
//                                        "Virtual Diffractometer Error", JOptionPane.WARNING_MESSAGE);
                                colorToSet = Color.RED;
                            }
                            if (virtCrystalLabel != null) {
                                virtCrystalLabel.setBackground(colorToSet);
                            }
                        }
                    }
                }
            };
            setWidgetModel(crystalComboBox, stringBox, crystalKey);
            setWidgetModel(acquisitionCrystalTarget, stringBox, crystalKey);
            // ---------------------------------------------------------------------------
            // END OF CRYSTAL LIST ComboBox auto selection on update
            // ---------------------------------------------------------------------------

            // ---------------------------------------------------------------------------
            // Feed CRYSTAL LIST ComboBox from Attribute
            // ---------------------------------------------------------------------------
            final TangoKey crystalNamesKey = generateAttributeKey(ATTRIBUTE_CRYSTAL_NAMES);
            final ITextMatrixTarget acquisitionCrystalNamesTarget = new ITextMatrixTarget() {

                String[] oldValue = new String[0];

                @Override
                public void removeMediator(final Mediator<?> mediator) {
                    // NOT USE
                }

                @Override
                public void addMediator(final Mediator<?> mediator) {
                    // NOT USE
                }

                @Override
                public int getMatrixDataHeight(final Class<?> clazz) {
                    // NOT USE
                    return 0;
                }

                @Override
                public int getMatrixDataWidth(final Class<?> clazz) {
                    // NOT USE
                    return 0;
                }

                @Override
                public boolean isPreferFlatValues(final Class<?> clazz) {
                    // NOT USE
                    return false;
                }

                @Override
                public String[] getFlatStringMatrix() {
                    // NOT USE
                    return null;
                }

                @Override
                public String[][] getStringMatrix() {
                    // NOT USE
                    return null;
                }

                @Override
                public void setFlatStringMatrix(final String[] value, final int width, final int height) {
                    // NOT USE
                }

                @Override
                public void setStringMatrix(final String[][] value) {
                    final String[] tmp = value[0];
                    if (tmp != null && !Arrays.deepEquals(oldValue, tmp)) {
                        // Object selectedItem = comboModel.getSelectedItem();
                        final String[] newValue = (String[]) ArrayUtils.getCopy(tmp);
                        Object selectedItem = crystalComboBox.getSelectedItem();
                        if (selectedItem == null) {
                            selectedItem = realCrystalLabel.getText();
                        }
                        crystalComboBox.setValueList((Object[]) tmp);
                        crystalComboBox.setSelectedItem(selectedItem);
//                        if (comboModel == null) {
//                            comboModel = new DefaultComboBoxModel<>(tmp);
//                        }
//                        Object selectedItem = comboModel.getSelectedItem();
//                        comboModel.removeAllElements();
//                        for (int i = 0; i < arg0.length; i++) {
//                            comboModel.addElement(tmp[i]);
//                        }
//                        // Connect combobox Selection to Crystal Attribute
//                        stringBox.disconnectWidgetFromAll(crystalComboBox);
//                        setWidgetModel(crystalComboBox, stringBox, crystalKey);
//                        crystalComboBox.setModel(comboModel);
//                        crystalComboBox.setValueList((Object[]) tmp);
//                        if (selectedItem != null) {
//                            comboModel.setSelectedItem(selectedItem);
//                        }

                        oldValue = newValue;
                    }
                }
            };
            setWidgetModel(acquisitionCrystalNamesTarget, stringMatrixBox, crystalNamesKey);
            // ---------------------------------------------------------------------------
            // END OF Feed CRYSTAL LIST ComboBox from Attribute
            // ---------------------------------------------------------------------------

            final int numberOfAxis = realAxis.length;
            final TangoKey solutionsKey = new TangoKey();
            TangoKeyTool.registerAttribute(solutionsKey, firstPseudoAxis + TANGO_SEPARATOR + ATTRIBUTE_SOLUTIONS);

            titles = new String[1 + numberOfAxis];
            titles[0] = INDEX;

            for (int i = 1; i < titles.length; i++) {
                titles[i] = realAxis[i - 1][0];
            }

            solutionTable.setCustomColumnNames(titles);
            setWidgetModel(solutionTable, numberMatrixBox, solutionsKey);

            setStatusModel();
            setStateModel();

            final TangoKey indexKey = generateReadOnlyAttributeKey(ATTRIBUTE_SOLUTIONS_IDX);
            TangoKeyTool.registerAttribute(indexKey, firstPseudoAxis + TANGO_SEPARATOR + ATTRIBUTE_SOLUTIONS_IDX);

            numberBox.connectWidget(indexSelector, indexKey);
            numberBox.connectWidget(solutionTable, indexKey);

            if (solutionTable == null) {
                getSolutionsTable();
            }

            // WaveLength
            final TangoKey waveLengthKey = generateAttributeKey(WAVELENGTH);

            final TangoKey waveLengthUnitKey = new TangoKey();
            TangoKeyTool.registerAttributeProperty(waveLengthUnitKey, getModel(), WAVELENGTH,
                    AttributePropertyType.UNIT);
            setWidgetModel(simpleScalarViewerLambda, stringBox, waveLengthKey);
            setWidgetModel(lambdaUnit, stringBox, waveLengthUnitKey);
            stringBox.setColorEnabled(lambdaUnit, false);
            // Setting model for all beans
            crystalPanel.setModel(getModel());
            crystalPanel.start();
            autoUpdateFromProxiesModeBean.setModel(getModel());
            autoUpdateFromProxiesModeBean.start();
            axesBean.setModel(getModel());
            axesBean.start();
            targetRealAxesBean.setModel(getModel());
            targetRealAxesBean.start();
            pseudoAxesBean.setModel(getModel());
            pseudoAxesBean.start();
            resumeBean.setModel(getModel());
            resumeBean.start();
        } catch (DevFailed e) {
            LOGGER.error(DevFailedUtils.toString(e), e);
        }
    }

    /**
     * This method initializes crystalComboEditor
     * 
     * @return fr.esrf.tangoatk.widget.attribute.StringScalarComboEditor
     */
    private ComboBox getCrystalComboEditor() {
        if (crystalComboBox == null) {
            crystalComboBox = new ComboBox() {
                private static final long serialVersionUID = -8213497902226682937L;

                @Override
                protected void warnMediators() {
                    if (getSelectedValue() != null) {
                        super.warnMediators();
                    }
                }
            };
            crystalComboBox.addItemListener((e) -> {
                if (e.getStateChange() == ItemEvent.SELECTED
                        || (e.getStateChange() == ItemEvent.DESELECTED && crystalComboBox.getItemCount() == 0)) {
                    updateRealCrystalLabelBackground();
                }
            });
        }
        return crystalComboBox;
    }

    @Override
    protected void onConnectionError() {
        // NOT USE
    }

    private void initialize() {
        try {
            setLayout(new BorderLayout());
            setPreferredSize(new java.awt.Dimension(800, 600));

            setMinimumSize(new java.awt.Dimension(400, 300));
            setSize(new java.awt.Dimension(800, 600));
            add(getDiffractometerStatusBar(), BorderLayout.SOUTH);
            add(getHKLToolBar(), BorderLayout.NORTH);
            add(getMainPanel(), BorderLayout.CENTER);

            // initialize iagetAxesBean
            getTargetRealAxesBean();
        } catch (final Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    private JPanel getConfigPanel() {
        if (configPanel == null) {
            configPanel = new JPanel();
            configPanel.setLayout(new GridBagLayout());

            configPanel.add(getConfigurationPanel());
            configPanel.add(getAutoUpdateFromProxiesModeBean());
            final GridBagConstraints configurationConstraints = new GridBagConstraints();
            configurationConstraints.fill = GridBagConstraints.BOTH;
            configurationConstraints.gridx = 0;
            configurationConstraints.gridy = 0;
            configurationConstraints.weightx = 0.5;
            configurationConstraints.weighty = 0;
            configurationConstraints.insets = new Insets(0, 0, 5, 0);
            configPanel.add(getConfigurationPanel(), configurationConstraints);

            final GridBagConstraints autoUpdateFromProxiesConstraints = new GridBagConstraints();
            autoUpdateFromProxiesConstraints.fill = GridBagConstraints.BOTH;
            autoUpdateFromProxiesConstraints.gridx = 1;
            autoUpdateFromProxiesConstraints.gridy = 0;
            autoUpdateFromProxiesConstraints.weightx = 0.5;
            autoUpdateFromProxiesConstraints.weighty = 1;
            autoUpdateFromProxiesConstraints.insets = new Insets(0, 0, 5, 0);
            configPanel.add(getAutoUpdateFromProxiesModeBean(), autoUpdateFromProxiesConstraints);
        }
        return configPanel;
    }

    /**
     * This method initializes centerPanel
     * 
     * @return Panel
     */
    private JPanel getLeftPanel() {
        if (leftPanel == null) {
            leftPanel = new JPanel();
            leftPanel.setLayout(new BorderLayout());
            leftPanel.add(getPseudoBean(), BorderLayout.CENTER);
        }
        return leftPanel;
    }

    private JPanel getWestPanel() {
        if (westPanel == null) {
            westPanel = new JPanel();
            westPanel.setLayout(new BorderLayout());
            westPanel.add(getJScrollLeftPanel(), BorderLayout.CENTER);
            westPanel.add(configPanel, BorderLayout.NORTH);
        }
        return westPanel;
    }

    /**
     * @return
     */
    private JPanel getConfigurationPanel() {
        if (sourcePanel == null) {
            sourcePanel = new JPanel();
            sourcePanel.setBorder(new TitledBorder(null, WAVELENGTH, TitledBorder.LEADING, TitledBorder.TOP,
                    new Font(Font.DIALOG, Font.BOLD, 12), Color.BLACK));
            simpleScalarViewerLambda = new WheelSwitch();
            lambdaUnit = new Label();
            sourcePanel.add(simpleScalarViewerLambda);
            sourcePanel.add(lambdaUnit);
        }
        return sourcePanel;
    }

    /**
     * This method initializes diffractometerStatusBar
     * 
     * @return fr.soleil.widget.DeviceStatusBar
     */
    private DeviceLabel getDiffractometerStatusBar() {
        if (diffractometerStatusBar == null) {
            diffractometerStatusBar = getDeviceStatusLabel();
        }
        return diffractometerStatusBar;
    }

    /**
     * This method initializes crystalPanel
     * 
     * @return Panel
     */
    private JPanel getCompleteCrystalPanel() {
        if (completeCrystalPanel == null) {
            completeCrystalPanel = new JPanel();
            completeCrystalPanel.setLayout(new BorderLayout());
            completeCrystalPanel.add(getHKLToolBar(), BorderLayout.NORTH);
            completeCrystalPanel.add(getCrystalPanel(), BorderLayout.CENTER);
        }
        return completeCrystalPanel;
    }

    /**
     * This method initializes crystalTabbedPane
     * 
     * @return javax.swing.JTabbedPane
     */
    private CrystalPanel getCrystalPanel() {
        if (crystalPanel == null) {
            crystalPanel = new CrystalPanel(getAxesBean());
        }
        return crystalPanel;
    }

    /**
     * This method initializes HKLToolBar
     * 
     * @return javax.swing.JToolBar
     */
    private JToolBar getHKLToolBar() {
        if (hklToolBar == null) {
            hklToolBar = new JToolBar();
            hklToolBar.setLayout(new FlowLayout(FlowLayout.LEFT));
            pilotCrystalTitle = new JLabel(PILOT_CRYSTAL);
            virtualCrystalTitle = new JLabel(VIRTUAL_CRYSTAL);
            crystalListTitle = new JLabel(CRYSTAL_LIST);
            virtCrystalLabel = new Label();
            realCrystalLabel = new Label();
            realCrystalLabel.addPropertyChangeListener((e) -> {
                if (TEXT.equals(e.getPropertyName())) {
                    updateRealCrystalLabelBackground();
                }
            });
        }
        return hklToolBar;
    }

    /**
     * This method initializes controlTabbedPane
     * 
     * @return TabbedPane
     */
    private TabbedPane getControlTabbedPane() {
        if (controlTabbedPane == null) {
            controlTabbedPane = new TabbedPane();
            controlTabbedPane.addTab("Solutions", getSolutionsPanel());
            controlTabbedPane.addTab("Crystal Editor", getCompleteCrystalPanel());
            controlTabbedPane.setSelectedIndex(1);

        }
        return controlTabbedPane;
    }

    /**
     * This method initializes crystalsConfigurationPanel
     * 
     * @return Panel
     */
    private JPanel getControlPanel() {
        if (controlPanel == null) {
            controlPanel = new JPanel();
            controlPanel.setLayout(new BorderLayout());
            controlPanel.add(getRightSplitPane(), java.awt.BorderLayout.CENTER);
        }
        return controlPanel;
    }

    /**
     * This method initializes rsCenterPanel
     * 
     * @return Panel
     */
    private JPanel getSolutionsPanel() {
        if (solutionsPanel == null) {
            solutionsPanel = new JPanel();
            solutionsPanel.setLayout(new BorderLayout());
            indexSelector = generateWheelSwitch();

            JPanel northPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
            northPanel.add(new JLabel("Selected Solution: "));
            northPanel.add(indexSelector);
            solutionsPanel.add(northPanel, BorderLayout.NORTH);
            solutionsPanel.add(getSolutionScrollPanel(), BorderLayout.CENTER);
        }
        return solutionsPanel;
    }

    /**
     * 
     * @return JDialog
     */
    public JDialog getRealAxesDialog() {
        if (realAxesDialog == null) {
            realAxesDialog = new JDialog(SwingUtilities.getWindowAncestor(this));
            realAxesDialog.setSize(1200, 380);
            realAxesDialog.setPreferredSize(new Dimension(1200, 380));
            realAxesDialog.setTitle("Real Axes Target Parameters");
            realAxesDialog.setLayout(new BorderLayout());
            realAxesDialog.add(getTargetRealAxesBean(), BorderLayout.CENTER);
            realAxesDialog.add(getRealCloseButton(), BorderLayout.SOUTH);
            realAxesDialog.setModal(true);
            // realAxesDialog.setLocationRelativeTo(null);
            realAxesDialog.pack();
        }
        return realAxesDialog;
    }

    /**
     * 
     * @return Panel
     */
    private JPanel getRealCloseButton() {
        final JPanel panelButton = new JPanel();
        panelButton.setLayout(new BorderLayout());
        if (realcloseButton == null) {
            realcloseButton = new Button("Close");
            panelButton.add(realcloseButton, BorderLayout.EAST);
            realcloseButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent e) {
                    realAxesDialog.setVisible(false);
                }
            });
        }
        return panelButton;
    }

    public JScrollPane getSolutionScrollPanel() {
        getSolutionsTable().getViewport();
        solutionTable.packAll();
        return getSolutionsTable();
    }

    /**
     * 
     * @return JScrollPane
     */
    private JScrollPane getSolutionsTable() {
        if (solutionTable == null) {
            solutionTable = new SolutionTable(this);
            jPanelComputeAngles = new JScrollPane();
            solutionTable.setEditable(false);
            jPanelComputeAngles.setViewportView(solutionTable);
            solutionTable.setColumnControlVisible(false);
        }
        return jPanelComputeAngles;
    }

    /**
     * This method initializes mainPanel
     * 
     * @return Panel
     */
    private JPanel getMainPanel() {
        if (mainPanel == null) {
            mainPanel = new JPanel();
            mainPanel.setLayout(new BorderLayout());
            mainPanel.add(getAxesBean(), BorderLayout.NORTH);
            mainPanel.add(getControlPanel(), BorderLayout.CENTER);
        }
        return mainPanel;
    }

    /**
     * 
     * @return JScrollPane
     */
    private JScrollPane getJScrollLeftPanel() {
        if (jScrollPaneLeftPanel == null) {
            jScrollPaneLeftPanel = new JScrollPane();
            final Dimension dimension = new Dimension(500, 200);
            jScrollPaneLeftPanel.setSize(dimension);
            jScrollPaneLeftPanel.setPreferredSize(dimension);

            final JPanel panel = new JPanel();
            panel.setLayout(new BorderLayout());
            panel.add(getConfigPanel());
            panel.add(getLeftPanel(), BorderLayout.CENTER);
            jScrollPaneLeftPanel.setViewportView(panel);
        }
        return jScrollPaneLeftPanel;
    }

    /**
     * This method initializes the SplitPane on the right side of screen
     * 
     * @return javax.swing.JSplitPane
     */
    private JSplitPane getRightSplitPane() {
        if (rightSplitPane == null) {
            rightSplitPane = new JSplitPane();
            rightSplitPane.setRightComponent(
                    new JScrollPane(getControlTabbedPane(), ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                            ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED));
            // JScrollPane

            rightSplitPane.setLeftComponent(getWestPanel());
            rightSplitPane.setDividerLocation(200);
            rightSplitPane.setAutoscrolls(false);
            rightSplitPane.setOneTouchExpandable(true);
        }
        return rightSplitPane;
    }

    /**
     * This method initializes autoUpdateFromProxiesModeBean
     * 
     * @return fr.soleil.bean.diffractometer.AutoUpdateFromProxiesModeBean
     */
    private RealWordInteractionsBean getAutoUpdateFromProxiesModeBean() {
        if (autoUpdateFromProxiesModeBean == null) {
            autoUpdateFromProxiesModeBean = new RealWordInteractionsBean(this);
        }
        return autoUpdateFromProxiesModeBean;
    }

    /**
     * This method initializes axesBean
     * 
     * @return fr.soleil.bean.diffractometer.AxesBean
     */
    private AxesBean getAxesBean() {
        if (axesBean == null) {
            axesBean = new AxesBean();
            axesBean.setBorder(new TitledBorder(null, "Axes", TitledBorder.DEFAULT_JUSTIFICATION,
                    TitledBorder.DEFAULT_POSITION, null, null));
        }
        return axesBean;
    }

    /**
     * This method initializes tagetRealAxesBean
     * 
     * @return fr.soleil.bean.diffractometer.TagetAxesBean
     */
    private TargetAxesBeanPanel getTargetRealAxesBean() {
        if (targetRealAxesBean == null) {
            targetRealAxesBean = new TargetAxesBeanPanel();
            targetRealAxesBean.setType(ReachTargetBean.REAL_AXES_TYPE);
            targetRealAxesBean.setBorder(new TitledBorder(null, "Axes", TitledBorder.DEFAULT_JUSTIFICATION,
                    TitledBorder.DEFAULT_POSITION, null, null));
        }
        return targetRealAxesBean;
    }

    private TabbedPane getPseudoBean() {
        if (pseudoBean == null) {
            pseudoBean = new TabbedPane();
            pseudoBean.addTab("Summary", getSummaryBean());
            pseudoBean.addTab("Pseudo Axes", getPseudoAxesBean());
        }
        return pseudoBean;
    }

    private SummaryBean getSummaryBean() {
        if (resumeBean == null) {
            resumeBean = new SummaryBean();
        }
        return resumeBean;
    }

    /**
     * @return the pseudoAxesBean
     */
    private PseudoAxesBean getPseudoAxesBean() {
        if (pseudoAxesBean == null) {
            pseudoAxesBean = new PseudoAxesBean();
            pseudoAxesBean.setBorder(new TitledBorder(null, "Pseudo Axes", TitledBorder.DEFAULT_JUSTIFICATION,
                    TitledBorder.DEFAULT_POSITION, null, null));
        }
        return pseudoAxesBean;
    }

    public static void showTangoError(final DevFailed devfailed) {
        String s = ObjectUtils.EMPTY_STRING;
        for (int i = 0; i < devfailed.errors.length; i++) {
            s = s + "Error n° " + i + " \n";
            s = s + "Desc -> " + devfailed.errors[i].desc + "\n";
            s = s + "Reason -> " + devfailed.errors[i].reason + "\n";
            s = s + "Origin -> " + devfailed.errors[i].origin + "\n";
            s = s + "--------------------------------------------\n";
        }

        if (s.length() > 0) {
            JOptionPane.showMessageDialog(parent, s, "Tango Error", JOptionPane.ERROR_MESSAGE);
        }

    }

    @Override
    public void savePreferences(final Preferences preferences) {
        if (preferences != null) {
            // getCrystalTabbedPane().savePreferences(preferences);
            int rightSplitLocation = getRightSplitPane().getDividerLocation();
            if (getRightSplitPane().getUI() != null && getRightSplitPane().getUI() instanceof SplitPaneUI) {
                rightSplitLocation = getRightSplitPane().getUI().getDividerLocation(getRightSplitPane());
            }
            preferences.putInt(RIGHT_SPLIT_LOCATION_KEY, rightSplitLocation);
        }
    }

    @Override
    public void loadPreferences(final Preferences preferences) {
        if (preferences != null) {
            final int rightSplitLocation = preferences.getInt(RIGHT_SPLIT_LOCATION_KEY, -1);
            if (rightSplitLocation != -1) {
                getRightSplitPane().setDividerLocation(rightSplitLocation);
            }
            this.preferences = preferences;
        }
    }

    public void setGapReflexion(final double gap) {
        if (gap != gapReflexion) {
            gapReflexion = gap;
            getCrystalPanel().setGapReflexion(gapReflexion);
        }
    }

    public double getGapReflexion() {
        return gapReflexion;
    }

}