package fr.soleil.bean.diffractometer.gui.util;

import java.awt.Component;
import java.awt.Font;

import org.jdesktop.swingx.decorator.AbstractHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

public class BoldFontHighlighter extends AbstractHighlighter {

    public BoldFontHighlighter() {
        super();
        // TODO Auto-generated constructor stub
    }

    public BoldFontHighlighter(HighlightPredicate predicate) {
        super(predicate);
        // TODO Auto-generated constructor stub
    }

    /* (non-Javadoc)
     * @see org.jdesktop.swingx.decorator.AbstractHighlighter#doHighlight(java.awt.Component, org.jdesktop.swingx.decorator.ComponentAdapter)
     */
    @Override
    protected Component doHighlight(Component component, ComponentAdapter adapter) {
        component.setFont(component.getFont().deriveFont(Font.BOLD));
        return component;
    }

}