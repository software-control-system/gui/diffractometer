package fr.soleil.bean.diffractometer.gui.util;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This code was generated using CloudGarden's Jigloo SWT/Swing GUI Builder,
 * which is free for non-commercial use. If Jigloo is being used commercially
 * (ie, by a corporation, company or business for any purpose whatever) then you
 * should purchase a license for each developer using Jigloo. Please visit
 * www.cloudgarden.com for details. Use of Jigloo implies acceptance of these
 * licensing terms. ************************************* A COMMERCIAL LICENSE
 * HAS NOT BEEN PURCHASED for this machine, so Jigloo or this code cannot be
 * used legally for any corporate or commercial purpose.
 * *************************************
 */
public class DiffDialog extends javax.swing.JDialog {

    private static final long serialVersionUID = 6006781577371716274L;

    private static final Logger LOGGER = LoggerFactory.getLogger(DiffDialog.class);

    private final JPanel myPanel;
    private OKCancelPanel myOkCancelPanel;
    private boolean ok;

    /**
     * Auto-generated main method to display this JDialog
     */
    public static void main(final String[] args) {
        final JFrame frame = new JFrame();
        final DiffDialog inst = new DiffDialog(frame, "DiffDialog", new JPanel());
        inst.setVisible(true);
    }

    public DiffDialog(final JFrame frame, final String myTitle, final JPanel myPanel) {
        super(frame, myTitle, true);
        this.myPanel = myPanel;
        initGUI();
    }

    private void initGUI() {
        try {
            getContentPane().add(myPanel, BorderLayout.NORTH);

            myOkCancelPanel = new OKCancelPanel(this);
            getContentPane().add(myOkCancelPanel, BorderLayout.SOUTH);
            pack();
        } catch (final Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    public void okPerfomed() {
        ok = true;
        dispose();
    }

    public void cancelPerfomed() {
        ok = false;
        dispose();
    }

    public boolean showDialog() {
        setVisible(true);
        return ok;
    }

    /**
     * @return Returns the myPanel.
     */
    public JPanel getMyPanel() {
        return myPanel;
    }
}
