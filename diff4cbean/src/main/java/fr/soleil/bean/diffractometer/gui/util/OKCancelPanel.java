package fr.soleil.bean.diffractometer.gui.util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.swing.Button;

/**
 * This code was generated using CloudGarden's Jigloo SWT/Swing GUI Builder,
 * which is free for non-commercial use. If Jigloo is being used commercially
 * (ie, by a corporation, company or business for any purpose whatever) then you
 * should purchase a license for each developer using Jigloo. Please visit
 * www.cloudgarden.com for details. Use of Jigloo implies acceptance of these
 * licensing terms. ************************************* A COMMERCIAL LICENSE
 * HAS NOT BEEN PURCHASED for this machine, so Jigloo or this code cannot be
 * used legally for any corporate or commercial purpose.
 * *************************************
 */
public class OKCancelPanel extends JPanel {

    private static final long serialVersionUID = -7101477731797550771L;

    private static final Logger LOGGER = LoggerFactory.getLogger(OKCancelPanel.class);

    private Button buttonOk;
    private Button buttonCancel;
    private final DiffDialog parentDialog;

    /**
     * Auto-generated main method to display this JPanel inside a new JFrame.
     */
    public static void main(final String[] args) {
        final JFrame frame = new JFrame();
        final OKCancelPanel okCancel = new OKCancelPanel(new DiffDialog(frame, "OKCancelPanel", new JPanel()));
        frame.getContentPane().add(okCancel);
        frame.pack();
        frame.setVisible(true);
    }

    public OKCancelPanel(final DiffDialog parentDialog) {
        super();
        initGUI();
        this.parentDialog = parentDialog;
    }

    private void initGUI() {
        try {
            setPreferredSize(new java.awt.Dimension(300, 50));
            setLayout(null);
            setSize(300, 50);
            buttonOk = new Button();
            add(buttonOk);
            buttonOk.setText("Ok");
            buttonOk.setBounds(20, 10, 110, 30);
            buttonOk.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent evt) {
                    parentDialog.okPerfomed();
                }
            });
            buttonCancel = new Button();
            add(buttonCancel);
            buttonCancel.setText("Cancel");
            buttonCancel.setBounds(170, 10, 110, 30);
            buttonCancel.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent evt) {
                    parentDialog.cancelPerfomed();
                }
            });
        } catch (final Exception e) {
            LOGGER.error(e.getMessage());
        }
    }
}