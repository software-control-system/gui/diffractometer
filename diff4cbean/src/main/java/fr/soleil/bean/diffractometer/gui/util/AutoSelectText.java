package fr.soleil.bean.diffractometer.gui.util;

import java.awt.KeyboardFocusManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.text.JTextComponent;

public class AutoSelectText {
    private static FocusHandler installedInstance;

    /**
     * Install an PropertyChangeList listener to the default focus manager and
     * selects text when a text component is focused.
     */
    public static void install() {
        // already installed
        if (installedInstance != null) {
            return;
        }

        installedInstance = new FocusHandler();

        final KeyboardFocusManager kfm = KeyboardFocusManager.getCurrentKeyboardFocusManager();

        kfm.addPropertyChangeListener("focusOwner", installedInstance);
    }

    public static void uninstall() {
        if (installedInstance != null) {
            final KeyboardFocusManager kfm = KeyboardFocusManager.getCurrentKeyboardFocusManager();
            kfm.removePropertyChangeListener("focusOwner", installedInstance);
        }
    }

    private static class FocusHandler implements PropertyChangeListener {

        @Override
        public void propertyChange(final PropertyChangeEvent evt) {
            if (evt.getNewValue() instanceof JTextComponent) {
                final JTextComponent text = (JTextComponent) evt.getNewValue();
                // select text if the component is editable
                // and the caret is at the end of the text
                if (text.isEditable() && text.getDocument().getLength() == text.getCaretPosition()) {

                    text.selectAll();
                }
            }
        }

    }
}
