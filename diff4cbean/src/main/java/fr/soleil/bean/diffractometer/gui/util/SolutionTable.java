package fr.soleil.bean.diffractometer.gui.util;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JScrollPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.AttributeProxy;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.DeviceProxyFactory;
import fr.soleil.bean.diffractometer.gui.DiffractometerBean;
import fr.soleil.comete.swing.NumberMatrixTable;
import fr.soleil.data.target.scalar.INumberTarget;

public class SolutionTable extends NumberMatrixTable implements INumberTarget, TangoConstants {

    private static final long serialVersionUID = -675542121267464893L;

    private static final Logger LOGGER = LoggerFactory.getLogger(SolutionTable.class);

    private DiffractometerBean diffBean;

    public SolutionTable(DiffractometerBean diffBean) {
        super();
        this.diffBean = diffBean;
        initSpecificListeners();
    }

    private void updateAttributeValue(short selectedIndex) {
        DeviceAttribute attr;
        try {

            DeviceProxy device = DeviceProxyFactory.get(diffBean.firstPseudoAxis);
            AttributeProxy attProxy = new AttributeProxy(
                    diffBean.firstPseudoAxis + TANGO_SEPARATOR + DiffractometerBean.ATTRIBUTE_SOLUTIONS_IDX);
            attr = new DeviceAttribute(DiffractometerBean.ATTRIBUTE_SOLUTIONS_IDX, selectedIndex);
            attProxy.write(attr);
            device.command_inout("ApplySolutionAngle");
        } catch (final DevFailed e) {
            LOGGER.error(DevFailedUtils.toString(e), e);
        }
    }

    private void initSpecificListeners() {
        MouseAdapter tableAdapter = new MouseAdapter() {
            @Override
            public void mouseClicked(final MouseEvent arg0) {
                if (arg0.getClickCount() == 2) {
                    final short index = (short) getSelectedRow();
                    updateAttributeValue(index);
                }
            }
        };

        MouseAdapter rowHeaderAdapter = new MouseAdapter() {
            @Override
            public void mouseClicked(final MouseEvent arg0) {
                if (arg0.getClickCount() == 2) {
                    final short index = (short) table.getRowHeaderTable().getSelectedRow();
                    updateAttributeValue(index);
                }
            }
        };

        table.addMouseListener(tableAdapter);
        table.getRowHeaderTable().addMouseListener(rowHeaderAdapter);
    }

    @Override
    public void setNumberMatrix(Object[] value) {
        JScrollPane scrollPane = diffBean.getSolutionScrollPanel();
        scrollPane.removeAll();
        super.setNumberMatrix(value);

        if (value == null) {
            JLabel warningLabel = new JLabel("No Remaining Solutions");
            warningLabel.setFont(warningLabel.getFont().deriveFont(20f));
            scrollPane.add(warningLabel);
        } else {
            scrollPane.add(this);
        }
        setColumnControlVisible(value != null);
        scrollPane.repaint();
    }

    @Override
    public void setSelectedRow(int selectedRow) {
        try {
            table.getRowHeaderTable().setRowSelectionInterval(selectedRow, selectedRow);
        } catch (IllegalArgumentException iae) {
            //
        }
    }

    @Override
    public Number getNumberValue() {
        return null;
    }

    @Override
    public void setNumberValue(Number value) {
        setSelectedRow(value.intValue());
    }

    public void setColumnControlVisible(boolean visible) {
        table.setColumnControlVisible(visible);
    }
}
