package fr.soleil.bean.diffractometer.gui.crystal;

import java.awt.Component;

import javax.swing.JOptionPane;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.DeviceProxyFactory;
import fr.soleil.lib.project.ObjectUtils;

/**
 * 
 * This class shows data of reflection from 1 crystal.
 * 
 * Only compatible with Diffractometer device v2
 * 
 * @author Fourneau
 * @version 3.0
 * 
 */

public class ReflectionTableModel extends ADoubleMatrixAttributeTableModel {

    private static final long serialVersionUID = -5433703061236269555L;

    private static final String REFLECTION_MODIFICATION_ERROR = "Reflection Modification Error";
    private static final String DESCRIPTION = "Description";
    private static final String ADD_REFLECTION = "AddReflection";
    private static final String REMOVE_REFLECTION = "RemoveReflection";
    private static final String GOTO_REFLECTION = "GotoReflection";

    private static final Logger LOGGER = LoggerFactory.getLogger(ReflectionTableModel.class);

    public ReflectionTableModel(final Component parent, final String device, final String shortNameAttribute,
            final String[] titles) throws DevFailed {
        super(parent, device, shortNameAttribute, titles);
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        Class<?> result = Double.class;
        if (columnIndex == 4) {
            result = Boolean.class;
        }
        return result;
    }

    @Override
    public int getColumnCount() {
        return titles.length;
    }

    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
        TableData lastData = this.lastData;
        Object result;
        try {
            final double[] data = lastData.getValue();
            int columnIndexToUse = columnIndex;
            if (columnIndex >= 1) {
                columnIndexToUse += 2;
            }
            double dataValue = data[lastData.getDimX() * rowIndex + columnIndexToUse];
            // result = String.valueOf(dataValue);
            if (columnIndex == 4) {
                result = dataValue > 0 ? Boolean.TRUE : Boolean.FALSE;
            } else {
                result = Double.valueOf(dataValue);
            }
        } catch (Exception e) {
            result = null;
        }
        return result;
    }

    @Override
    public String getColumnName(final int column) {
        String name;
        if ((column > -1) && (titles != null) && (column < titles.length)) {
            name = titles[column];
        } else {
            name = ObjectUtils.EMPTY_STRING;
        }
        return name;
    }

    @Override
    public boolean isCellEditable(final int rowIndex, final int columnIndex) {
        return true;
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Override
    protected AValueWriter generateValueWriter(Object value, int row, int column) {
        return new ValueWriter(value, row, column);
    }

    @Override
    protected String getWriteErrorMessage() {
        return REFLECTION_MODIFICATION_ERROR;
    }

    public void addReflection(String pseudoAxesDeviceName) {
        if (device != null) {
            DeviceData data;
            try {
                data = new DeviceData();
                String[] defaultData = new String[1];
                defaultData[0] = DESCRIPTION;
                data.insert(defaultData);
                final DeviceProxy dp = DeviceProxyFactory.get(pseudoAxesDeviceName);
                dp.command_inout(ADD_REFLECTION, data);
                fireTableDataChanged();
            } catch (final DevFailed e) {
                JOptionPane.showMessageDialog(null, DevFailedUtils.toString(e), REFLECTION_MODIFICATION_ERROR,
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    public void removeReflection(final int index) {
        if (device != null) {
            DeviceData data;
            try {
                data = new DeviceData();
                short[] values = { (short) (index) };
                data.insert(values);
                final DeviceProxy dp = DeviceProxyFactory.get(device);
                dp.command_inout(REMOVE_REFLECTION, data);
                fireTableRowsDeleted(index, index);
            } catch (final DevFailed e) {
                JOptionPane.showMessageDialog(null, DevFailedUtils.toString(e), REFLECTION_MODIFICATION_ERROR,
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    public void gotoReflection(final int index) {
        if (device != null) {
            DeviceData data;
            try {
                data = new DeviceData();

                data.insert((short) index);
                final DeviceProxy dp = DeviceProxyFactory.get(device);
                dp.command_inout(GOTO_REFLECTION, data);

            } catch (final DevFailed e) {
                JOptionPane.showMessageDialog(null, DevFailedUtils.toString(e), REFLECTION_MODIFICATION_ERROR,
                        JOptionPane.WARNING_MESSAGE);
                LOGGER.error(DevFailedUtils.toString(e), e);
            }
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class ValueWriter extends AValueWriter {

        public ValueWriter(Object value, int row, int column) {
            super(value, row, column);
        }

        @Override
        protected Void doInBackground() throws Exception {
            final DeviceAttribute da = attrProxy.read();
            if (!canceled) {
                double[] data = da.extractDoubleArray();
                if (!canceled) {
                    final int dimX = da.getDimX();
                    final int dimY = da.getDimY();
                    final int readSize = dimX * dimY;
                    if (!canceled) {
                        // suppress write part
                        data = ArrayUtils.subarray(data, 0, readSize);
                        int columnIndexToUse = column;
                        if (column >= 1) {
                            columnIndexToUse += 2;
                        }
                        double dataToInsert = 0;
                        if (column == 4) {
                            dataToInsert = ((Boolean) value) ? 1.0 : 0.0;
                        } else {
                            dataToInsert = (Double) value; // Double.parseDouble((String) aValue);
                        }
                        data[dimX * row + columnIndexToUse] = dataToInsert;
                        if (!canceled) {
                            da.insert(data, dimX, dimY);
                            if (!canceled) {
                                attrProxy.write(da);
                            }
                        }
                    }
                }
            }
            return null;
        }

    }

}
