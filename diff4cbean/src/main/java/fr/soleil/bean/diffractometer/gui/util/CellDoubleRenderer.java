package fr.soleil.bean.diffractometer.gui.util;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellRenderer;

public class CellDoubleRenderer extends JTextField implements TableCellRenderer {

    private static final long serialVersionUID = 2459332976387758964L;

    public CellDoubleRenderer() {
        super();
    }

    public JTextField getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int col) {

        if (isSelected) {
            this.setBackground(table.getSelectionBackground());
        } else {
            setBackground(table.getBackground());
        }

        String doubleValue = ((Double) value).toString();
        this.setText(doubleValue);
        this.setHorizontalAlignment(JTextField.RIGHT);
        this.setEditable(false);
        return this;
    }
}