package fr.soleil.bean.diffractometer.gui.util;

import fr.soleil.comete.swing.Label;

public class FormattedLabel extends Label {

    private static final long serialVersionUID = -2650878767316762976L;

    private String format;

    public void setFormat(String format) {
        this.format = format;
    }

    @Override
    public void setText(String text) {
        if (text != null && format != null) {
            String formattedValue = text;
            Double dValue = null;
            try {
                dValue = Double.valueOf(text);
                formattedValue = String.format(format, dValue);
            } catch (Exception e) {
            }
            super.setText(formattedValue);
        } else {
            super.setText(text);
        }

    }
}
