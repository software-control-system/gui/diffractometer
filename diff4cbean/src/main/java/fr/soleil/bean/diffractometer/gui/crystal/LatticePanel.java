package fr.soleil.bean.diffractometer.gui.crystal;

import java.awt.Font;
import java.util.prefs.Preferences;

import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.scalarbox.BooleanScalarBox;
import fr.soleil.comete.swing.CheckBox;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.tango.data.service.TangoKey;
import net.miginfocom.swing.MigLayout;

/**
 * 
 * @author Fourneau
 */
public class LatticePanel extends AbstractTangoBox {

    private static final long serialVersionUID = -2203882957997558846L;

    private CheckBox aCheckBox;
    private Label aLabel;
    private Label aViewer;
    private CheckBox alphaCheckBox;
    private Label alphaLabel;
    private Label alphaViewer;
    private CheckBox bCheckBox;
    private Label bLabel;
    private Label bViewer;
    private CheckBox betaCheckBox;
    private Label betaLabel;
    private Label betaViewer;
    private CheckBox cCheckBox;
    private Label cLabel;
    private Label cViewer;
    private CheckBox gammaCheckBox;
    private Label gammaLabel;
    private Label gammaViewer;
    private Label vLabel;
    private Label vViewer;

    private final BooleanScalarBox booleanBox = new BooleanScalarBox();

    /** Creates new form LatticePanel */
    public LatticePanel() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed"
    // desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new MigLayout());

        aLabel = new Label();
        aViewer = new Label();
        aViewer.setFont(new Font("Dialog", Font.PLAIN, 12));
        aCheckBox = new CheckBox();
        aLabel.setText("a");
        add(aLabel);
        add(aViewer);
        add(aCheckBox, "wrap");

        bLabel = new Label();
        bViewer = new Label();
        bViewer.setFont(new Font("Dialog", Font.PLAIN, 12));
        bCheckBox = new CheckBox();
        bLabel.setText("b");
        add(bLabel);
        add(bViewer);
        add(bCheckBox, "wrap");

        cLabel = new Label();
        cViewer = new Label();
        cViewer.setFont(new Font("Dialog", Font.PLAIN, 12));
        cCheckBox = new CheckBox();
        cLabel.setText("c");
        add(cLabel);
        add(cViewer);
        add(cCheckBox, "wrap");

        alphaLabel = new Label();
        alphaViewer = new Label();
        alphaViewer.setFont(new Font("Dialog", Font.PLAIN, 12));
        alphaCheckBox = new CheckBox();
        alphaLabel.setText("alpha");
        add(alphaLabel);
        add(alphaViewer);
        add(alphaCheckBox, "wrap");

        betaLabel = new Label();
        betaViewer = new Label();
        betaViewer.setFont(new Font("Dialog", Font.PLAIN, 12));
        betaCheckBox = new CheckBox();
        betaLabel.setText("beta");
        add(betaLabel);
        add(betaViewer);
        add(betaCheckBox, "wrap");

        gammaLabel = new Label();
        gammaViewer = new Label();
        gammaViewer.setFont(new Font("Dialog", Font.PLAIN, 12));
        gammaCheckBox = new CheckBox();
        gammaLabel.setText("gamma");
        add(gammaLabel);
        add(gammaViewer);
        add(gammaCheckBox, "wrap");

        vLabel = new Label();
        vViewer = new Label();
        vViewer.setFont(new Font("Dialog", Font.PLAIN, 12));
        vLabel.setText("v");
        add(vLabel);
        add(vViewer);

    }

    @Override
    protected void clearGUI() {
        cleanWidget(aViewer);
        cleanWidget(bViewer);
        cleanWidget(cViewer);
        cleanWidget(alphaViewer);
        cleanWidget(betaViewer);
        cleanWidget(gammaViewer);
        cleanWidget(vViewer);
        cleanWidget(aCheckBox);
        cleanWidget(bCheckBox);
        cleanWidget(cCheckBox);
        cleanWidget(alphaCheckBox);
        cleanWidget(betaCheckBox);
        cleanWidget(gammaCheckBox);
    }

    @Override
    protected void refreshGUI() {
        final TangoKey aVKey = generateReadOnlyAttributeKey("a");
        setWidgetModel(aViewer, stringBox, aVKey);
        stringBox.setUnitEnabled(aViewer, false);
        final TangoKey bVKey = generateReadOnlyAttributeKey("b");
        setWidgetModel(bViewer, stringBox, bVKey);
        stringBox.setUnitEnabled(bViewer, false);
        final TangoKey cVKey = generateReadOnlyAttributeKey("c");
        setWidgetModel(cViewer, stringBox, cVKey);
        stringBox.setUnitEnabled(cViewer, false);
        final TangoKey alphaVKey = generateReadOnlyAttributeKey("alpha");
        setWidgetModel(alphaViewer, stringBox, alphaVKey);
        stringBox.setUnitEnabled(alphaViewer, false);
        final TangoKey betaVKey = generateReadOnlyAttributeKey("beta");
        setWidgetModel(betaViewer, stringBox, betaVKey);
        stringBox.setUnitEnabled(betaViewer, false);
        final TangoKey gammaVKey = generateReadOnlyAttributeKey("gamma");
        setWidgetModel(gammaViewer, stringBox, gammaVKey);
        stringBox.setUnitEnabled(gammaViewer, false);
        final TangoKey vVKey = generateReadOnlyAttributeKey("v");
        setWidgetModel(vViewer, stringBox, vVKey);
        stringBox.setUnitEnabled(vViewer, false);

        final TangoKey aCBKey = generateAttributeKey("aFit");
        setWidgetModel(aCheckBox, booleanBox, aCBKey);
        final TangoKey bCBKey = generateAttributeKey("bFit");
        setWidgetModel(bCheckBox, booleanBox, bCBKey);
        final TangoKey cCBKey = generateAttributeKey("cFit");
        setWidgetModel(cCheckBox, booleanBox, cCBKey);
        final TangoKey alphaCBKey = generateAttributeKey("alphaFit");
        setWidgetModel(alphaCheckBox, booleanBox, alphaCBKey);
        final TangoKey betaCBKey = generateAttributeKey("betaFit");
        setWidgetModel(betaCheckBox, booleanBox, betaCBKey);
        final TangoKey gammaCBKey = generateAttributeKey("gammaFit");
        setWidgetModel(gammaCheckBox, booleanBox, gammaCBKey);
    }

    @Override
    protected void onConnectionError() {
        // throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void loadPreferences(final Preferences arg0) {
        // NOT USE
    }

    @Override
    protected void savePreferences(final Preferences arg0) {
        // NOT USE
    }

}
