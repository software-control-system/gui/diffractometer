/*
 * Created on 10 juin 2005
 * with Eclipse
 */
package fr.soleil.bean.diffractometer.gui.action;

import java.awt.Component;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.DeviceProxyFactory;

public class StopAction extends AbstractAction {

    private static final long serialVersionUID = 8841178402907215848L;

    private static final Logger LOGGER = LoggerFactory.getLogger(StopAction.class);

    public DeviceProxy model = null;

    /**
     * @throws DevFailed
     * 
     */
    public StopAction(final String device) throws DevFailed {
        super();
        model = DeviceProxyFactory.get(device);
        // This is an instance initializer; it is executed just after the
        // constructor of the superclass is invoked

        // The following values are completely optional
        putValue(Action.NAME, "Stop motors");
        // Set tool tip text
        putValue(Action.SHORT_DESCRIPTION, "Stop Motors");

        // This text is not directly used by any Swing component;
        // however, this text could be used in a help system
        putValue(Action.LONG_DESCRIPTION, "Stop all diffractometer's motors ");

        // Set a mnemonic character. In most look and feels, this causes the
        // specified character to be underlined This indicates that if the
        // component
        // using this action has the focus and In some look and feels, this
        // causes
        // the specified character in the label to be underlined and
        putValue(Action.MNEMONIC_KEY, new Integer(java.awt.event.KeyEvent.VK_ESCAPE));

        // Set an accelerator key; this value is used by menu items
        putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ESCAPE"));
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        if (model != null) {
            try {
                model.command_inout("Abort");
            } catch (final DevFailed e1) {
                LOGGER.error(DevFailedUtils.toString(e1), e1);
                JOptionPane.showMessageDialog((Component) e.getSource(), DevFailedUtils.toString(e1), "Stop",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }
}