/**
 * 
 */
package fr.soleil.gui.capoeira;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.util.prefs.Preferences;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.bean.motor.UserMotorBean;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.scalar.IBooleanTarget;
import fr.soleil.gui.capoeira.model.CapoeiraModel;

/**
 * @author HARDION
 * 
 */
public class ControlBean extends AbstractTangoBox {

    private static final long serialVersionUID = 1255396499102027995L;

    private static final String STATE = "State";
    private static final String STATUS = "Status";
    private static final Dimension MINIMUM_SIZE_BEAN = new Dimension(100, 200);

    public static final String ENABLE_MOTOR_BEANS = "ENABLE_MOTOR_BEANS";

    private static final Logger LOGGER = LoggerFactory.getLogger(ControlBean.class);

    public ControlBean() {
        super();
        initialize();
    }

    @Override
    public void stop() {
        // Motors
        if (omegaMotorBean != null) {
            omegaMotorBean.stop();
        }
        if (chiMotorBean != null) {
            chiMotorBean.stop();
        }
        if (phiMotorBean != null) {
            phiMotorBean.stop();
        }
        if (twothetaMotorBean != null) {
            twothetaMotorBean.stop();
        }
        if (psiPseudoMotorBean != null) {
            psiPseudoMotorBean.stop();
        }
    }

    @Override
    public void start() {
        // Motors
        if (omegaMotorBean != null && omegaMotorBean.getModel() != null && !omegaMotorBean.getModel().isEmpty()) {
            omegaMotorBean.start();
        }
        if (chiMotorBean != null && chiMotorBean.getModel() != null && !chiMotorBean.getModel().isEmpty()) {
            chiMotorBean.start();
        }
        if (phiMotorBean != null && phiMotorBean.getModel() != null && !phiMotorBean.getModel().isEmpty()) {
            phiMotorBean.start();
        }
        if (twothetaMotorBean != null && twothetaMotorBean.getModel() != null
                && !twothetaMotorBean.getModel().isEmpty()) {
            twothetaMotorBean.start();
        }
        if (psiPseudoMotorBean != null && psiPseudoMotorBean.getModel() != null
                && !psiPseudoMotorBean.getModel().isEmpty()) {
            psiPseudoMotorBean.start();
        }
        if (analyzerAxisBeans != null) {
            for (final AbstractTangoBox atb : analyzerAxisBeans) {
                if (atb != null) {
                    atb.start();
                }
            }
        }
        if (detectorRealAxisBeans != null) {
            for (final AbstractTangoBox atb : detectorRealAxisBeans) {
                if (atb != null) {
                    atb.start();
                }
            }
        }
        if (eulerianPseudoAxisBeans != null) {
            for (final AbstractTangoBox atb : eulerianPseudoAxisBeans) {
                if (atb != null) {
                    atb.start();
                }
            }
        }
        if (kappaRealAxisBeans != null) {
            for (final AbstractTangoBox atb : kappaRealAxisBeans) {
                if (atb != null) {
                    atb.start();
                }
            }
        }
        if (pseudoAxisBeans != null) {
            for (final AbstractTangoBox atb : pseudoAxisBeans) {
                if (atb != null) {
                    atb.start();
                }
            }
        }
    }

    private UserMotorBean chiMotorBean = null;

    private JPanel motorPanel = null;

    private UserMotorBean omegaMotorBean = null;

    private UserMotorBean phiMotorBean = null;

    private UserMotorBean psiPseudoMotorBean = null;

    private UserMotorBean twothetaMotorBean = null;

    private CapoeiraModel model;

    private JPanel kappaPanel = null;

    private JPanel kappaAxisPanel = null;

    private JPanel pseudoPanel = null;

    private JPanel analyzerPanel = null;

    private JPanel eulerianAndDetectorPanel = null;

    private JPanel eulerianPanel = null;

    private JPanel detectorPanel = null;

    private AbstractTangoBox[] analyzerAxisBeans = null;
    private Label analyzerAxisErrorLabel = null;

    private AbstractTangoBox[] pseudoAxisBeans = null;
    private Label pseudoAxisErrorLabel = null;

    private AbstractTangoBox[] eulerianPseudoAxisBeans = null;
    private Label eulerianPseudoAxisErrorLabel = null;

    private AbstractTangoBox[] kappaRealAxisBeans = null;
    private Label kappaRealAxisErrorLabel = null;

    private AbstractTangoBox[] detectorRealAxisBeans = null;
    private Label detectorRealAxisErrorLabel = null;

    /**
     * This method initializes this
     * 
     * @return void
     */
    private void initialize() {

        setLayout(new BorderLayout());
        add(getDeviceStatusLabel(), BorderLayout.SOUTH);
        setPreferredSize(new Dimension(600, 500));

        // Build object
        // getMotorPanel();
    }

    /**
     * active MotorBean
     * 
     * @return boolean
     **/
    public boolean isUserMotorBeanEnabled() {
        boolean result = false;
        String propertyStr = System.getProperty(ENABLE_MOTOR_BEANS, "false");
        if (propertyStr.isEmpty()) {
            result = true;
        } else {
            result = Boolean.valueOf(propertyStr);
        }
        return result;
    }

    /**
     * This method initializes userMotorBean
     * 
     * @return userMotorBean.UserMotorBean
     */
    private UserMotorBean getChiMotorBean() {
        if (chiMotorBean == null) {
            chiMotorBean = new UserMotorBean();
            chiMotorBean.setExecutedBatchFile("atkpanel");
        }
        return chiMotorBean;
    }

    public JPanel getCenterPanel() {
        if (motorPanel != null) {
            return motorPanel;
        }
        // kappaPanel.setBackground(Color.blue);
        return kappaPanel;
    }

    /**
     * This method initializes motorPanel
     * 
     * @return Panel
     */
    public JPanel getMotorPanel() {
        if (motorPanel == null) {
            final GridBagConstraints gridBagConstraints11 = new GridBagConstraints();
            gridBagConstraints11.gridx = 0;
            gridBagConstraints11.gridy = 2;
            final GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
            gridBagConstraints3.insets = new java.awt.Insets(5, 5, 0, 0);
            gridBagConstraints3.gridx = 1;
            gridBagConstraints3.weightx = 1.0D;
            gridBagConstraints3.weighty = 1.0D;
            gridBagConstraints3.gridy = 1;
            // gridBagConstraints3.ipadx = 92;
            final GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
            gridBagConstraints2.insets = new java.awt.Insets(5, 0, 0, 5);
            gridBagConstraints2.gridx = 0;
            gridBagConstraints2.weightx = 1.0D;
            gridBagConstraints2.weighty = 1.0D;
            gridBagConstraints2.gridy = 1;
            // gridBagConstraints2.ipadx = 92;
            final GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
            gridBagConstraints1.insets = new java.awt.Insets(0, 5, 5, 0);
            gridBagConstraints1.gridx = 1;
            gridBagConstraints1.weightx = 1.0D;
            gridBagConstraints1.weighty = 1.0D;
            gridBagConstraints1.gridy = 0;
            // gridBagConstraints1.ipadx = 92;
            final GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 5);
            gridBagConstraints.gridx = 0;
            gridBagConstraints.weightx = 1.0D;
            gridBagConstraints.weighty = 1.0D;
            gridBagConstraints.gridy = 0;
            // gridBagConstraints.ipadx = 92;
            // final GridLayout gridLayout1 = new GridLayout();
            // gridLayout1.setRows(2);
            // gridLayout1.setHgap(10);
            // gridLayout1.setVgap(10);
            // gridLayout1.setColumns(2);

            motorPanel = new JPanel();
            motorPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Motors",
                    javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
                    javax.swing.border.TitledBorder.DEFAULT_POSITION, null, null));
            motorPanel.setPreferredSize(new java.awt.Dimension(1200, 326));
            motorPanel.setLayout(new GridBagLayout());
            motorPanel.add(getOmegaMotorBean(), gridBagConstraints);
            motorPanel.add(getChiMotorBean(), gridBagConstraints1);
            motorPanel.add(getPhiMotorBean(), gridBagConstraints2);
            motorPanel.add(getTwothetaMotorBean(), gridBagConstraints3);
            motorPanel.add(getPsiPseudoMotorBean(), gridBagConstraints11);
        }

        return motorPanel;
    }

    /**
     * This method initializes userMotorBean2
     * 
     * @return userMotorBean.UserMotorBean
     */
    private UserMotorBean getOmegaMotorBean() {
        if (omegaMotorBean == null) {
            omegaMotorBean = new UserMotorBean(true);
            omegaMotorBean.setExecutedBatchFile("atkpanel");
        }
        return omegaMotorBean;
    }

    /**
     * This method initializes userMotorBean1
     * 
     * @return userMotorBean.UserMotorBean
     */
    private UserMotorBean getPhiMotorBean() {
        if (phiMotorBean == null) {
            phiMotorBean = new UserMotorBean();
            phiMotorBean.setExecutedBatchFile("atkpanel");
        }
        return phiMotorBean;
    }

    /**
     * This method initializes psiPseudoMotorBean
     * 
     * @return fr.soleil.bean.galilaxis.UserMotorBean
     */
    private UserMotorBean getPsiPseudoMotorBean() {
        if (psiPseudoMotorBean == null) {
            psiPseudoMotorBean = new UserMotorBean();
            psiPseudoMotorBean.setExecutedBatchFile("atkpanel");
        }
        return psiPseudoMotorBean;
    }

    /**
     * This method initializes twothetaMotorBean
     * 
     * @return userMotorBean.UserMotorBean
     */
    private UserMotorBean getTwothetaMotorBean() {
        if (twothetaMotorBean == null) {
            twothetaMotorBean = new UserMotorBean();
            twothetaMotorBean.setExecutedBatchFile("atkpanel");
        }
        return twothetaMotorBean;
    }

    private IBooleanTarget generateVirtualBooleanTarget() {
        IBooleanTarget result = new IBooleanTarget() {
            @Override
            public void addMediator(Mediator<?> arg0) {
            }

            @Override
            public void removeMediator(Mediator<?> arg0) {
            }

            @Override
            public boolean isSelected() {
                return false;
            }

            @Override
            public void setSelected(boolean arg0) {
                if (arg0) {
                    getPseudoPanel().setBorder(BorderFactory.createTitledBorder(
                            BorderFactory.createLineBorder(Color.red, 5), "PSEUDO AXIS FROZEN", TitledBorder.CENTER,
                            TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 15), new Color(255, 0, 0)));

                } else {
                    getPseudoPanel().setBorder(BorderFactory.createTitledBorder(
                            BorderFactory.createLineBorder(Color.gray, 5), "PSEUDO AXIS", TitledBorder.CENTER,
                            TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), new Color(51, 51, 51)));
                }
            }

        };
        return result;

    }

    @Override
    protected void refreshGUI() {
        String deviceName = model.getDiffractometer();
        TangoKey freezeKey = new TangoKey();
        TangoKeyTool.registerAttribute(freezeKey, deviceName, "freeze");
        IBooleanTarget virtualTarget = generateVirtualBooleanTarget();
        booleanBox.connectWidget(virtualTarget, freezeKey);
        if (motorPanel != null) {
            motorPanel.removeAll();
            remove(motorPanel);
            motorPanel = null;
        }
        if (kappaPanel != null) {
            kappaAxisPanel.removeAll();
            kappaPanel.removeAll();
            kappaPanel.validate();
            pseudoPanel.removeAll();
            analyzerPanel.removeAll();
            eulerianAndDetectorPanel.removeAll();
            eulerianPanel.removeAll();
            detectorPanel.removeAll();
            remove(kappaPanel);
            kappaPanel = null;
        }
        revalidate();
        if (model.getType().equalsIgnoreCase("E4CV")) {
            // Motor beans
            getOmegaMotorBean().setAxisHorizontal(true);
            getChiMotorBean().setAxisHorizontal(true);
            getPhiMotorBean().setAxisHorizontal(true);
            getTwothetaMotorBean().setAxisHorizontal(true);
            getPsiPseudoMotorBean().setAxisHorizontal(true);
            final String[][] realAxis = model.getRealAxis();
            getOmegaMotorBean().setModel(realAxis[0][1]);
            getChiMotorBean().setModel(realAxis[1][1]);
            getPhiMotorBean().setModel(realAxis[2][1]);
            getTwothetaMotorBean().setModel(realAxis[3][1]);
            final String[][] pseudoAxis = model.getPseudoAxis();
            if (pseudoAxis != null) {
                getPsiPseudoMotorBean().setModel(pseudoAxis[0][1]);
            } else {
                LOGGER.error("Null property PseudoAxisProxies");
            }
            add(getMotorPanel());
        } else if (model.getType().contains("K6C") || model.getType().toLowerCase().contains("kappa")
                || model.getType().toLowerCase().contains("turret")) {
            updatePseudoAxisBeans(model.getPseudoAxis());
            updateKappaRealAxisBeans(model.getRealAxis());
            updateAnalyzerAxisBeans(model.getAnalyzerAxis());
            updateDetectorRealAxisBeans(model.getDetectorRealAxis());
            updateEulerianPseudoAxisBeans(model.getEulerianPseudoAxis());
            add(getKappaPanel());
        } else {
            updatePseudoAxisBeans(model.getPseudoAxis());
            updateKappaRealAxisBeans(model.getRealAxis());
            updateAnalyzerAxisBeans(model.getAnalyzerAxis());
            updateDetectorRealAxisBeans(model.getDetectorRealAxis());
            add(getKappaPanel());
        }
        setStatusModel();
        setStateModel();
        repaint();

    }

    @Override
    protected void setStatusModel() {
        getDeviceStatusLabel().setDeviceName(model.getDiffractometer());
        TangoKey statusKey = new TangoKey();
        TangoKeyTool.registerAttribute(statusKey, model.getDiffractometer(), STATUS);
        stringBox.connectWidget(statusLabel, statusKey);
        stringBox.connectWidget(getDeviceStatusLabel(), statusKey);
    }

    @Override
    protected void setStateModel() {
        TangoKey stateKey = new TangoKey();
        TangoKeyTool.registerAttribute(stateKey, model.getDiffractometer(), STATE);
        stringBox.connectWidget(getStateLabel(), stateKey);
    }

    /**
     * @param model The model to set.
     */
    public void setLPSModel(final CapoeiraModel model) {
        if (this.model == null || !this.model.equals(model)) {
            if (this.model != null) {
                // Unregistering listener from model
                stop();
            }
            this.model = model;
            if (this.model == null) {
                LOGGER.error("model is null");
            } else {
                refreshGUI();
            }
        }
    }

    /**
     * 
     * @return LPSModel
     */
    public CapoeiraModel getLPSModel() {
        return model;
    }

    /**
     * This method initializes kappaPanel
     * 
     * @return Panel
     */
    private JPanel getKappaPanel() {
        if (kappaPanel == null) {
            kappaPanel = new JPanel();
            kappaPanel.setLayout(new BorderLayout());
            kappaPanel.setSize(new Dimension(800, 400));
            kappaPanel.setPreferredSize(new Dimension(800, 400));
            kappaPanel.add(getEulerianAndDetectorPanel());
            kappaPanel.add(getPseudoPanel(), BorderLayout.SOUTH);
        }
        revalidate();
        repaint();
        return kappaPanel;
    }

    /**
     * This method initializes kaxxisPanel
     * 
     * @return Panel
     */
    private JPanel getKappaAxisPanel() {
        if (kappaAxisPanel == null) {
            final GridLayout gridLayout6 = new GridLayout();
            gridLayout6.setRows(4);
            gridLayout6.setColumns(1);
            kappaAxisPanel = new JPanel();
            kappaAxisPanel.setLayout(gridLayout6);
            kappaAxisPanel.setPreferredSize(new Dimension(200, 300));
            kappaAxisPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray, 5),
                    "KAPPA", TitledBorder.CENTER, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12),
                    new Color(51, 51, 51)));
            kappaAxisPanel.add(getKappaRealAxisErrorLabel(), null);
        }
        return kappaAxisPanel;
    }

    /**
     * This method initializes pseudoPanel
     * 
     * @return Panel
     */
    private JPanel getPseudoPanel() {
        if (pseudoPanel == null) {
            final GridLayout gridLayout = new GridLayout();
            gridLayout.setRows(1);
            pseudoPanel = new JPanel();
            pseudoPanel.setLayout(gridLayout);
            pseudoPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray, 5),
                    "PSEUDO AXIS", TitledBorder.CENTER, TitledBorder.DEFAULT_POSITION,
                    new Font("Dialog", Font.BOLD, 12), new Color(51, 51, 51)));
            pseudoPanel.setPreferredSize(new Dimension(800, 200));
            pseudoPanel.add(getPseudoAxisErrorLabel(), null);
        }
        return pseudoPanel;
    }

    /**
     * This method initializes analyzerPanel
     * 
     * @return JPanel
     */
    private JPanel getAnalyzerPanel() {
        if (analyzerPanel == null) {
            final GridLayout analyzerLayout = new GridLayout();
            analyzerLayout.setRows(4);
            analyzerLayout.setColumns(1);
            analyzerPanel = new JPanel();
            analyzerPanel.setLayout(analyzerLayout);
            analyzerPanel.setPreferredSize(new Dimension(200, 300));
            analyzerPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray, 5),
                    "ANALYZER", TitledBorder.CENTER, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12),
                    new Color(51, 51, 51)));
            analyzerPanel.add(getAnalyzerAxisErrorLabel(), null);
        }
        return analyzerPanel;
    }

    /**
     * This method initializes eulerianAndDetectorPanel
     * 
     * @return Panel
     */
    private JPanel getEulerianAndDetectorPanel() {
        if (eulerianAndDetectorPanel == null) {
            eulerianAndDetectorPanel = new JPanel();
            eulerianAndDetectorPanel.setLayout(new BoxLayout(eulerianAndDetectorPanel, BoxLayout.X_AXIS));
            eulerianAndDetectorPanel.add(getKappaAxisPanel(), null);
            eulerianAndDetectorPanel.add(getEulerianPanel(), null);
            eulerianAndDetectorPanel.add(getDetectorPanel(), null);
            eulerianAndDetectorPanel.add(getAnalyzerPanel(), null);
            setPreferredSize(new Dimension(800, 800));
        }
        revalidate();
        repaint();
        return eulerianAndDetectorPanel;
    }

    /**
     * This method initializes eulerianPanel
     * 
     * @return Panel
     */
    private JPanel getEulerianPanel() {
        if (eulerianPanel == null) {
            final GridLayout gridLayout5 = new GridLayout();
            gridLayout5.setRows(4);
            gridLayout5.setColumns(1);
            eulerianPanel = new JPanel();
            eulerianPanel.setLayout(gridLayout5);
            eulerianPanel.setPreferredSize(new Dimension(200, 300));
            eulerianPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray, 5),
                    "EULERIAN", TitledBorder.CENTER, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12),
                    new Color(51, 51, 51)));
            eulerianPanel.add(getEulerianPseudoAxisErrorLabel(), null);
        }
        return eulerianPanel;
    }

    /**
     * This method initializes detectorPanel
     * 
     * @return Panel
     */
    private JPanel getDetectorPanel() {
        if (detectorPanel == null) {
            final GridLayout gridLayout4 = new GridLayout();
            gridLayout4.setRows(4);
            gridLayout4.setColumns(1);
            detectorPanel = new JPanel();
            detectorPanel.setLayout(gridLayout4);
            detectorPanel.setPreferredSize(new Dimension(200, 300));
            detectorPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray, 5),
                    "DETECTOR", TitledBorder.CENTER, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12),
                    new Color(51, 51, 51)));
            detectorPanel.add(getDetectorRealAxisErrorLabel(), null);
        }
        return detectorPanel;
    }

    private void updateAnalyzerAxisBeans(final String[][] analyzerAxis) {
        if (analyzerAxisBeans != null) {
            // cleaning...
            for (int i = 0; i < analyzerAxisBeans.length; i++) {
                if (analyzerAxisBeans[i] != null) {
                    analyzerAxisBeans[i].setModel(null);
                    analyzerAxisBeans[i] = null;
                }
            }
        }
        getAnalyzerPanel().removeAll();
        getAnalyzerPanel().revalidate();
        if (analyzerAxis == null || analyzerAxis.length == 0 || analyzerAxis[0].length < 2) {
            // getAnalyzerPanel().add(getAnalyzerAxisErrorLabel(), null);
            getEulerianAndDetectorPanel().remove(getAnalyzerPanel());
        } else {
            analyzerAxisBeans = new AbstractTangoBox[analyzerAxis.length];
            for (int i = 0; i < analyzerAxis.length; i++) {
                try {
                    analyzerAxisBeans[i] = new UserMotorBean(analyzerAxis[i][0]);
                    // analyzerAxisBeans[i] = new
                    // CommonDeviceAttributeBean();
                    // analyzerAxisBeans[i].setModel(analyzerAxis[i][0]);
                    // analyzerAxisBeans[i].setAttributeList(model.getNormalAttributeList());
                    if (analyzerAxisBeans[i].getModel() == null || analyzerAxisBeans[i].getModel().trim().isEmpty()) {
                        if (analyzerAxis[i][1] == null || analyzerAxis[i][1].trim().isEmpty()) {
                            LOGGER.error("Error with motor model: analyzerAxis[" + i + "][1] = '" + analyzerAxis[i][1]
                                    + "'");
                        } else {
                            analyzerAxisBeans[i].setModel(analyzerAxis[i][1]);
                            // analyzerAxisBeans[i].start();
                        }
                    }
                    analyzerAxisBeans[i].setMinimumSize(MINIMUM_SIZE_BEAN);
                    getAnalyzerPanel().add(analyzerAxisBeans[i]);
                } catch (final Throwable t) {
                    LOGGER.error(t.getMessage());
                    analyzerAxisBeans[i] = null;
                    final Label failedAnalyzerLabel = new Label();
                    failedAnalyzerLabel.setText("Failed to create Bean for " + analyzerAxis[i][1]);
                    getAnalyzerPanel().add(failedAnalyzerLabel);
                }
            }
        }
        final GridLayout analyzerLayout = new GridLayout();
        analyzerLayout.setColumns(1);
        if (getAnalyzerPanel().getComponentCount() < 4) {
            analyzerLayout.setRows(4);
        } else {
            analyzerLayout.setRows(getAnalyzerPanel().getComponentCount());
        }
        getAnalyzerPanel().setLayout(analyzerLayout);
        getAnalyzerPanel().revalidate();
    }

    private void updatePseudoAxisBeans(final String[][] pseudoAxis) {
        if (pseudoAxisBeans != null) {
            // cleaning...
            for (int i = 0; i < pseudoAxisBeans.length; i++) {
                if (pseudoAxisBeans[i] != null) {
                    pseudoAxisBeans[i].setModel(null);
                    pseudoAxisBeans[i] = null;
                }
            }
        }
        getPseudoPanel().removeAll();
        getPseudoPanel().revalidate();
        if (pseudoAxis == null || pseudoAxis.length == 0 || pseudoAxis[0].length < 2) {
//            getPseudoPanel().add(getPseudoAxisErrorLabel(), null);
            getKappaPanel().remove(getPseudoPanel());
        } else {
            pseudoAxisBeans = new AbstractTangoBox[pseudoAxis.length];

            for (int i = 0; i < pseudoAxis.length; i++) {
                try {
                    pseudoAxisBeans[i] = new UserMotorBean(pseudoAxis[i][0]);
                    // pseudoAxisBeans[i].setAttributeList(model.getNormalAttributeList());
                    if (pseudoAxisBeans[i].getModel() == null || pseudoAxisBeans[i].getModel().trim().isEmpty()) {
                        if (pseudoAxis[i][1] == null || pseudoAxis[i][1].trim().isEmpty()) {
                            LOGGER.error(
                                    "Error with motor model: pseudoAxis[" + i + "][1] = '" + pseudoAxis[i][1] + "'");
                        } else {
                            pseudoAxisBeans[i].setModel(pseudoAxis[i][1]);
                            if (!isUserMotorBeanEnabled()) {
                                ((UserMotorBean) pseudoAxisBeans[i]).disableBean();
                            } // pseudoAxisBeans[i].start();
                        }
                    }
                    pseudoAxisBeans[i].setMinimumSize(MINIMUM_SIZE_BEAN);
                    getPseudoPanel().add(pseudoAxisBeans[i]);
                } catch (final Throwable t) {
                    LOGGER.error(t.getMessage());
                    pseudoAxisBeans[i] = null;
                    final Label failedPseudoAxisLabel = new Label();
                    failedPseudoAxisLabel.setText("Failed to create Bean for " + pseudoAxis[i][1]);
                    getPseudoPanel().add(failedPseudoAxisLabel);
                }
            }
        }
        final GridLayout pseudoLayout = new GridLayout();
        pseudoLayout.setRows(1);
        if (getPseudoPanel().getComponentCount() < 1) {
            pseudoLayout.setColumns(1);
        } else {
            pseudoLayout.setColumns(getPseudoPanel().getComponentCount());
        }
        getPseudoPanel().setLayout(pseudoLayout);
        getPseudoPanel().revalidate();
    }

    private void updateEulerianPseudoAxisBeans(final String[][] eulerianPseudoAxis) {
        if (eulerianPseudoAxisBeans != null) {
            // cleaning...
            for (int i = 0; i < eulerianPseudoAxisBeans.length; i++) {
                if (eulerianPseudoAxisBeans[i] != null) {
                    eulerianPseudoAxisBeans[i].setModel(null);
                    eulerianPseudoAxisBeans[i] = null;
                }
            }
        }
        getEulerianPanel().removeAll();
        getEulerianPanel().revalidate();
        if (eulerianPseudoAxis == null || eulerianPseudoAxis.length == 0 || eulerianPseudoAxis[0].length < 2) {
//            getEulerianPanel().add(getEulerianPseudoAxisErrorLabel(), null);
            getEulerianAndDetectorPanel().remove(getEulerianPanel());
        } else {
            eulerianPseudoAxisBeans = new AbstractTangoBox[eulerianPseudoAxis.length];
            for (int i = 0; i < eulerianPseudoAxis.length; i++) {
                try {
                    eulerianPseudoAxisBeans[i] = new UserMotorBean(eulerianPseudoAxis[i][0]);
                    if (!isUserMotorBeanEnabled()) {
                        ((UserMotorBean) eulerianPseudoAxisBeans[i]).disableBean();
                    } // eulerianPseudoAxisBeans[i].setAttributeList(model
                      // .getNormalAttributeList());
                    if (eulerianPseudoAxisBeans[i].getModel() == null
                            || eulerianPseudoAxisBeans[i].getModel().trim().isEmpty()) {
                        if (eulerianPseudoAxis[i][1] == null || eulerianPseudoAxis[i][1].trim().isEmpty()) {
                            LOGGER.error("Error with motor model: eulerianPseudoAxis[" + i + "][1] = '"
                                    + eulerianPseudoAxis[i][1] + "'");
                        } else {
                            eulerianPseudoAxisBeans[i].setModel(eulerianPseudoAxis[i][1]);
                            // eulerianPseudoAxisBeans[i].start();
                        }
                    }
                    eulerianPseudoAxisBeans[i].setMinimumSize(MINIMUM_SIZE_BEAN);
                    getEulerianPanel().add(eulerianPseudoAxisBeans[i]);
                } catch (final Throwable t) {
                    LOGGER.error(t.getMessage());
                    eulerianPseudoAxisBeans[i] = null;
                    final Label failedEulerianLabel = new Label();
                    failedEulerianLabel.setText("Failed to create Bean for " + eulerianPseudoAxis[i][1]);
                    getEulerianPanel().add(failedEulerianLabel);
                }
            }
        }
        final GridLayout eulerianPseudoLayout = new GridLayout();
        eulerianPseudoLayout.setColumns(1);
        if (getEulerianPanel().getComponentCount() < 4) {
            eulerianPseudoLayout.setRows(4);
        } else {
            eulerianPseudoLayout.setRows(getEulerianPanel().getComponentCount());
        }
        getEulerianPanel().setLayout(eulerianPseudoLayout);
        getEulerianPanel().revalidate();
    }

    private void updateKappaRealAxisBeans(final String[][] kappaRealAxis) {
        if (kappaRealAxisBeans != null) {
            // cleaning...
            for (int i = 0; i < kappaRealAxisBeans.length; i++) {
                if (kappaRealAxisBeans[i] != null) {
                    kappaRealAxisBeans[i].setModel(null);
                    kappaRealAxisBeans[i] = null;
                }
            }
        }
        getKappaAxisPanel().removeAll();
        getKappaAxisPanel().revalidate();
        if (kappaRealAxis == null || kappaRealAxis.length == 0 || kappaRealAxis[0].length < 2) {
//            getKappaAxisPanel().add(getKappaRealAxisErrorLabel(), null);
            getEulerianAndDetectorPanel().remove(getKappaAxisPanel());
        } else {
            kappaRealAxisBeans = new AbstractTangoBox[kappaRealAxis.length];
            for (int i = 0; i < kappaRealAxis.length; i++) {
                try {
                    kappaRealAxisBeans[i] = new UserMotorBean(kappaRealAxis[i][0]);
                    // kappaRealAxisBeans[i]
                    // .setAttributeList(model.getNormalAttributeList());
                    if (kappaRealAxisBeans[i].getModel() == null || kappaRealAxisBeans[i].getModel().trim().isEmpty()) {
                        if (kappaRealAxis[i][1] == null || kappaRealAxis[i][1].trim().isEmpty()) {
                            LOGGER.error("Error with motor model: kappaRealAxis[" + i + "][1] = '" + kappaRealAxis[i][1]
                                    + "'");
                        } else {
                            kappaRealAxisBeans[i].setModel(kappaRealAxis[i][1]);
                            // kappaRealAxisBeans[i].start();
                            if (!isUserMotorBeanEnabled()) {
                                ((UserMotorBean) kappaRealAxisBeans[i]).disableBean();
                            }
                        }
                    }
                    kappaRealAxisBeans[i].setMinimumSize(MINIMUM_SIZE_BEAN);
                    getKappaAxisPanel().add(kappaRealAxisBeans[i]);
                } catch (final Throwable t) {
                    LOGGER.error(t.getMessage());
                    kappaRealAxisBeans[i] = null;
                    final Label failedKappaLabel = new Label();
                    failedKappaLabel.setText("Failed to create Bean for " + kappaRealAxis[i][1]);
                    getKappaAxisPanel().add(failedKappaLabel);
                }
            }
        }
        final GridLayout kappaLayout = new GridLayout();
        kappaLayout.setColumns(1);
        if (getKappaAxisPanel().getComponentCount() < 4) {
            kappaLayout.setRows(4);
        } else {
            kappaLayout.setRows(getKappaAxisPanel().getComponentCount());
        }
        getKappaAxisPanel().setLayout(kappaLayout);

    }

    private void updateDetectorRealAxisBeans(final String[][] detectorRealAxis) {
        if (detectorRealAxisBeans != null) {
            // cleaning...
            for (int i = 0; i < detectorRealAxisBeans.length; i++) {
                if (detectorRealAxisBeans[i] != null) {
                    detectorRealAxisBeans[i].setModel(null);
                    detectorRealAxisBeans[i] = null;
                }
            }
        }
        getDetectorPanel().removeAll();
        if (detectorRealAxis == null || detectorRealAxis.length == 0 || detectorRealAxis[0].length < 2) {
//            getDetectorPanel().add(getDetectorRealAxisErrorLabel(), null);
            getEulerianAndDetectorPanel().remove(getDetectorPanel());
        } else {
            detectorRealAxisBeans = new AbstractTangoBox[detectorRealAxis.length];
            for (int i = 0; i < detectorRealAxis.length; i++) {
                try {
                    detectorRealAxisBeans[i] = new UserMotorBean(detectorRealAxis[i][0]);
                    if (detectorRealAxisBeans[i].getModel() == null
                            || detectorRealAxisBeans[i].getModel().trim().isEmpty()) {
                        if (detectorRealAxis[i][1] == null || detectorRealAxis[i][1].trim().isEmpty()) {
                            LOGGER.error("Error with motor model: detectorRealAxis[" + i + "][1] = '"
                                    + detectorRealAxis[i][1] + "'");
                        } else {
                            detectorRealAxisBeans[i].setModel(detectorRealAxis[i][1]);
                            // detectorRealAxisBeans[i].start();
                        }
                    }
                    detectorRealAxisBeans[i].setMinimumSize(MINIMUM_SIZE_BEAN);
                    if (!isUserMotorBeanEnabled()) {
                        ((UserMotorBean) detectorRealAxisBeans[i]).disableBean();
                    }
                    getDetectorPanel().add(detectorRealAxisBeans[i]);
                } catch (final Throwable t) {
                    LOGGER.error(t.getMessage(), t);
                    detectorRealAxisBeans[i] = null;
                    final Label failedDetectorLabel = new Label();
                    failedDetectorLabel.setText("Failed to create Bean for " + detectorRealAxis[i][1]);
                    getDetectorPanel().add(failedDetectorLabel);
                }
            }
        }
        final GridLayout detectorLayout = new GridLayout();
        detectorLayout.setColumns(1);
        if (getDetectorPanel().getComponentCount() < 4) {
            detectorLayout.setRows(4);
        } else {
            detectorLayout.setRows(getDetectorPanel().getComponentCount());
        }
        getDetectorPanel().setLayout(detectorLayout);
    }

    /**
     * @return the analyzerErrorLabel
     */
    public Label getAnalyzerAxisErrorLabel() {
        if (analyzerAxisErrorLabel == null) {
            analyzerAxisErrorLabel = new Label();
            analyzerAxisErrorLabel.setText("Problem with AnalyzerAxisProxies property");
        }
        return analyzerAxisErrorLabel;
    }

    /**
     * @return the pseudoAxisErrorLabel
     */
    public Label getPseudoAxisErrorLabel() {
        if (pseudoAxisErrorLabel == null) {
            pseudoAxisErrorLabel = new Label();
            pseudoAxisErrorLabel.setText("Problem with PseudoAxisProxies property");
        }
        return pseudoAxisErrorLabel;
    }

    /**
     * @return the eulerianPseudoAxisErrorLabel
     */
    public Label getEulerianPseudoAxisErrorLabel() {
        if (eulerianPseudoAxisErrorLabel == null) {
            eulerianPseudoAxisErrorLabel = new Label();
            eulerianPseudoAxisErrorLabel.setText("Problem with EulerianPseudoAxisProxies property");
        }
        return eulerianPseudoAxisErrorLabel;
    }

    /**
     * @return the kappaRealErrorLabel
     */
    public Label getKappaRealAxisErrorLabel() {
        if (kappaRealAxisErrorLabel == null) {
            kappaRealAxisErrorLabel = new Label();
            kappaRealAxisErrorLabel.setText("Problem with RealAxisProxies property");
        }
        return kappaRealAxisErrorLabel;
    }

    /**
     * @return the detectorRealErrorLabel
     */
    public Label getDetectorRealAxisErrorLabel() {
        if (detectorRealAxisErrorLabel == null) {
            detectorRealAxisErrorLabel = new Label();
            detectorRealAxisErrorLabel.setText("No gamma/mu to display");
        }
        return detectorRealAxisErrorLabel;
    }

    @Override
    protected void clearGUI() {
        omegaMotorBean = null;
        chiMotorBean = null;
        phiMotorBean = null;
        twothetaMotorBean = null;
        psiPseudoMotorBean = null;
        kappaPanel = null;
        kappaAxisPanel = null;
        pseudoPanel = null;
        analyzerPanel = null;
        eulerianAndDetectorPanel = null;
        eulerianPanel = null;
        detectorPanel = null;
    }

    @Override
    protected void loadPreferences(final Preferences arg0) {
        // NOT USE
    }

    @Override
    protected void onConnectionError() {
        // NOT USE
    }

    @Override
    protected void savePreferences(final Preferences arg0) {
        // NOT USE
    }
}