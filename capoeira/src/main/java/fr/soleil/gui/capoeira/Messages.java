package fr.soleil.gui.capoeira;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Messages {
    private static final Logger LOGGER = LoggerFactory.getLogger(Messages.class);
    private static final String BUNDLE_NAME = "fr.soleil.lps.messages";
    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

    private Messages() {
    }

    public static String getString(final String key) {
	try {
	    return RESOURCE_BUNDLE.getString(key);
	} catch (final MissingResourceException e) {
	    LOGGER.error(e.getMessage());
	    return '!' + key + '!';
	}
    }
}
