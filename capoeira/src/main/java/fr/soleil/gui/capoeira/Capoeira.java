package fr.soleil.gui.capoeira;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.prefs.Preferences;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevFailed;
import fr.soleil.bean.diffractometer.gui.DiffractometerBean;
import fr.soleil.bean.diffractometer.model.ContextModel;
import fr.soleil.comete.swing.TabbedPane;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.data.source.BasicDataSource;
import fr.soleil.gui.capoeira.action.PreferencesDisplayingAction;
import fr.soleil.gui.capoeira.model.CapoeiraModel;
import fr.soleil.gui.capoeira.util.CapoeiraConstants;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.application.Application;
import fr.soleil.lib.project.application.logging.LogViewer;
import fr.soleil.lib.project.application.logging.LogbackUtils;

public class Capoeira extends Application implements CapoeiraConstants {

    private static final long serialVersionUID = 7313646182678860975L;

    private static final Logger LOGGER = LoggerFactory.getLogger(Capoeira.class);
    private static final LogViewer LOG_VIEWER = LogbackUtils.getConfiguredLogViewer(Capoeira.class.getName(), true);
    static {
        LOG_VIEWER.setAutoResizeColumns(true);
    }

    private static final String DEFAULT_DOC_URL = "http://ctrl/doc/HKL/";
    private static final String URL_DOCUMENTATION = "URL_DOCUMENTATION";
    private static final String GAP_REFLEXION = "GAP_REFLEXION";

    public static final int DEFAULT_WINDOW_X = 0;
    public static final int DEFAULT_WINDOW_Y = 0;
    public static final int DEFAULT_WINDOW_WIDTH = 1024;
    public static final int DEFAULT_WINDOW_HEIGHT = 768;
    public static final String COMPTER_DEVICE = "CompterDevice";

    private TabbedPane applicationTabbedPane;
    private ControlBean controlBean;
    private DiffractometerBean hklPanel;

    protected CapoeiraModel model;

    /**
     * This is the default constructor
     */
    public Capoeira() {
        super();
        initializeCapoeira();
    }

    private void clearGUI() {
        // Control
        controlBean.stop();
    }

    /**
     * This method initializes applicationTabbedPane
     * 
     * @return TabbedPane
     */
    private TabbedPane getApplicationTabbedPane() {
        if (applicationTabbedPane == null) {
            applicationTabbedPane = new TabbedPane();
            applicationTabbedPane.setSize(new Dimension(1237, 702));
            applicationTabbedPane.addTab(ContextModel.PILOT_LABEL, null, getControlBean(), null);
            applicationTabbedPane.addTab(ContextModel.PREPARE_LABEL, null, getHklPanel(), null);
            applicationTabbedPane.addTab("Logs", null, LOG_VIEWER, "Application logs");
        }
        return applicationTabbedPane;
    }

    /**
     * This method initializes controlBean
     * 
     * @return fr.soleil.lps.ControlBean
     */
    private ControlBean getControlBean() {
        if (controlBean == null) {
            controlBean = new ControlBean();
        }
        return controlBean;
    }

    /**
     * This method initializes hklPanel
     * 
     * @return Diff4cBean.Diff4cJPanel
     */
    private DiffractometerBean getHklPanel() {
        if (hklPanel == null) {
            hklPanel = new DiffractometerBean();
        }
        return hklPanel;
    }

    /**
     * @return Returns the model.
     */
    public CapoeiraModel getModel() {
        return model;
    }

    private void initializeDocumentationMenu() {
        final JMenu helpMenu = getHelpMenu();
        final JMenuItem helpMenuItem = new JMenuItem();
        helpMenuItem.setText("Documentation");
        helpMenuItem.addActionListener(e -> {
            if (Desktop.isDesktopSupported()) {
                if (Desktop.getDesktop().isSupported(java.awt.Desktop.Action.BROWSE)) {
                    try {
                        Desktop.getDesktop().browse(new URI(model.getUrl()));
                    } catch (final IOException ex) {
                        LOGGER.error(ex.getMessage());
                    } catch (final URISyntaxException uri) {
                        LOGGER.error(uri.getMessage());
                    }
                } else {
                    LOGGER.error("The browse action is not supported by your operating system");
                }
            } else {
                LOGGER.error("Desktop is not supported by your operating system");
            }
        });
        helpMenu.add(helpMenuItem);
    }

    /**
     * This method initializes this
     * 
     * @return void
     */
    private void initializeCapoeira() {
        initializeDocumentationMenu();
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(getApplicationTabbedPane(), java.awt.BorderLayout.CENTER);
        // loadPreferences();
    }

    private void refreshGUI() throws DevFailed {
        // preference menu
        getPreferenceMenuItem().setAction(new PreferencesDisplayingAction(controlBean, hklPanel));
        // Motor beans
        controlBean.stop();
        controlBean.setLPSModel(model);
        hklPanel.setGapReflexion(model.getGap());
    }

    /**
     * @param model The model to set.
     * @throws DevFailed
     */
    public void setModel(final CapoeiraModel model) throws DevFailed {
        if (this.model == null || !this.model.equals(model)) {
            if (this.model != null) {
                // Unregistering listener from model
                this.model.setDiffractometer(null);
            }
            this.model = model;
            loadPreferences();
            if (this.model != null) {
                refreshGUI();
            } else {
                clearGUI();
            }
        }
    }

    @Override
    protected Preferences recoverPreferences() {
        return Preferences.userNodeForPackage(Capoeira.class);
    }

    @Override
    protected void savePreferences() {
        super.savePreferences();
        if (prefs != null) {
            prefs.putInt(STATE, getExtendedState());
            setExtendedState(Frame.NORMAL);
            Rectangle bounds = getBounds();
            prefs.putInt(X, bounds.x);
            prefs.putInt(Y, bounds.y);
            prefs.putInt(W, bounds.width);
            prefs.putInt(H, bounds.height);
            getHklPanel().savePreferences(prefs);
            prefs.put(DIFFRACTOMETER_DEVICE, model.getDiffractometer());
            prefs.put(DIFFRACTOMETER_PREPARE_DEVICE, getHklPanel().getModel());
            // prefs.put(COMPTER_DEVICE, model.getCounter());
            prefs.put(GAP_REFLEXION, Double.toString(model.getGap()));
            prefs.put(URL_DOCUMENTATION, model.getUrl());
        }
    }

    @Override
    protected void loadPreferences() {
        super.loadPreferences();
        if ((prefs != null) && (model != null)) {
            final int x = prefs.getInt(X, DEFAULT_WINDOW_X);
            final int y = prefs.getInt(Y, DEFAULT_WINDOW_Y);
            setLocation(x, y);
            final int w = prefs.getInt(W, DEFAULT_WINDOW_WIDTH);
            final int h = prefs.getInt(H, DEFAULT_WINDOW_HEIGHT);
            setSize(w, h);
            final int state = prefs.getInt(STATE, Frame.NORMAL);
            setExtendedState(state);
            getHklPanel().loadPreferences(prefs);
            final double gap = prefs.getDouble(GAP_REFLEXION, Double.NaN);
            model.setGap(gap);
            final String url = prefs.get(URL_DOCUMENTATION, DEFAULT_DOC_URL);
            model.setUrl(url);
            // getModel().setDiffractometer(prefs.get(DIFFRACTOMETER_DEVICE,"not initialized"));
        }
    }

    @Override
    protected String getApplicationTitle() {
        return "Capoeira";
    }

    @Override
    protected String getCopyright() {
        return "Synchrotron SOLEIL";
    }

    @Override
    protected Image getDefaultIconImage() {
        final ImageIcon icone = new ImageIcon(Capoeira.class.getResource("/diffracto/logo-ZEsoleil-25x23.png"));
        return icone.getImage();
    }

    @Override
    protected ImageIcon getSplashImage() {
        final ImageIcon result = new ImageIcon(Capoeira.class.getResource("/diffracto/splash-11986-2.png"));
        return result;
    }

    // Utility method only used in main method
    private static GridBagConstraints generateConstraints(int fill, int gridx, int gridy, double weightx,
            double weighty) {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = fill;
        constraints.gridx = gridx;
        constraints.gridy = gridy;
        constraints.weightx = weightx;
        constraints.weighty = weighty;
        return constraints;
    }

    /**
     * @param args
     */
    public static void main(final String[] args) {
        BasicDataSource<String> pilotSource = new BasicDataSource<>(null);
        BasicDataSource<String> prepareSource = new BasicDataSource<>(null);

        if (args != null) {
            for (String arg : args) {
                if (arg != null && !arg.trim().isEmpty()) {
                    if (pilotSource.getData() == null) {
                        pilotSource.setData(arg);
                    } else if (prepareSource.getData() == null) {
                        prepareSource.setData(arg);
                        break;
                    }
                }
            }
        }
        pilotSource.setData(pilotSource.getData() == null ? ObjectUtils.EMPTY_STRING : pilotSource.getData().trim());
        prepareSource
                .setData(prepareSource.getData() == null ? ObjectUtils.EMPTY_STRING : prepareSource.getData().trim());

        if (pilotSource.getData().isEmpty() || prepareSource.getData().isEmpty()
                || pilotSource.getData().equalsIgnoreCase(prepareSource.getData())) {
            // Construct and display device choosing dialog
            System.out.println("Missing argument(s). Preparing " + DIFFRACTOMETER + " selection tool...");
            String[] devices = TangoDeviceHelper.getExportedDevicesOfClass(DIFFRACTOMETER);
            if ((devices == null) || (devices.length == 0)) {
                System.err.println("No exported " + DIFFRACTOMETER + " device found. Can't launch capoeira.");
                System.exit(1);
            } else if (devices.length == 1) {
                System.err.println("Capoeira needs 2 different " + DIFFRACTOMETER + " devices.");
                System.err.println("Only one exported " + DIFFRACTOMETER + " device was found.");
                System.exit(1);
            } else {
                String cancelled = DIFFRACTOMETER + " selection cancelled";
                JDialog deviceSelectionDialog = new JDialog((JFrame) null, "Choose " + DIFFRACTOMETER + " devices",
                        true);
                deviceSelectionDialog.setAlwaysOnTop(true);
                deviceSelectionDialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
                deviceSelectionDialog.setResizable(false);
                deviceSelectionDialog.addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosing(WindowEvent e) {
                        System.out.println(cancelled);
                        System.exit(0);
                    }
                });
                JPanel deviceSelectionPanel = new JPanel(new GridBagLayout());
                JComboBox<String> pilotDevices = new JComboBox<>(devices);
                JComboBox<String> prepareDevices = new JComboBox<>(devices);
                JLabel pilotLabel = new JLabel("Pilot " + DIFFRACTOMETER + ": ");
                JLabel prepareLabel = new JLabel("Preparation " + DIFFRACTOMETER + ": ");
                Insets buttonMargin = new Insets(2, 2, 2, 2);
                JButton okButton = new JButton("OK");
                okButton.setMargin(buttonMargin);
                okButton.addActionListener(e -> {
                    deviceSelectionDialog.setVisible(false);
                });
                okButton.setEnabled(false);
                JButton cancelButton = new JButton("Cancel");
                cancelButton.setMargin(buttonMargin);
                cancelButton.addActionListener(e -> {
                    System.out.println(cancelled);
                    System.exit(0);
                });
                JLabel warningLabel = new JLabel("Please choose 2 different devices!");
                warningLabel.setFont(warningLabel.getFont().deriveFont(Font.ITALIC));
                warningLabel.setForeground(Color.RED);
                Font basicFont = pilotDevices.getFont().deriveFont(Font.PLAIN);
                pilotDevices.setFont(basicFont);
                prepareDevices.setFont(basicFont);
                ItemListener comboListener = e -> {
                    if (e.getStateChange() == ItemEvent.SELECTED) {
                        if (e.getSource() == pilotDevices) {
                            pilotSource.setData((String) pilotDevices.getSelectedItem());
                        } else if (e.getSource() == prepareDevices) {
                            prepareSource.setData((String) prepareDevices.getSelectedItem());
                        }
                        okButton.setEnabled(!ObjectUtils.sameObject(pilotDevices.getSelectedItem(),
                                prepareDevices.getSelectedItem()));
                        warningLabel.setVisible(!okButton.isEnabled());
                        deviceSelectionDialog.pack();
                    }
                };
                pilotDevices.addItemListener(comboListener);
                prepareDevices.addItemListener(comboListener);
                for (String device : devices) {
                    if (device.equalsIgnoreCase(pilotSource.getData())) {
                        pilotSource.setData(device);
                    }
                    if (device.equalsIgnoreCase(prepareSource.getData())) {
                        prepareSource.setData(device);
                    }
                }
                if (!pilotSource.getData().isEmpty()) {
                    pilotDevices.setSelectedItem(pilotSource.getData());
                }
                if (!prepareSource.getData().isEmpty()) {
                    prepareDevices.setSelectedItem(prepareSource.getData());
                }
                int x = 0, y = 0;
                GridBagConstraints constraints = generateConstraints(GridBagConstraints.HORIZONTAL, x++, y, 0.5, 0);
                deviceSelectionPanel.add(pilotLabel, constraints);
                constraints = generateConstraints(GridBagConstraints.HORIZONTAL, x, y++, 0.5, 0);
                constraints.insets = new Insets(0, 5, 0, 0);
                deviceSelectionPanel.add(prepareLabel, constraints);
                x = 0;
                constraints = generateConstraints(GridBagConstraints.HORIZONTAL, x++, y, 0.5, 0);
                deviceSelectionPanel.add(pilotDevices, constraints);
                constraints = generateConstraints(GridBagConstraints.HORIZONTAL, x, y++, 0.5, 0);
                constraints.insets = new Insets(0, 5, 0, 0);
                deviceSelectionPanel.add(prepareDevices, constraints);
                x = 0;
                constraints = generateConstraints(GridBagConstraints.HORIZONTAL, x, y++, 1, 0);
                constraints.gridwidth = GridBagConstraints.REMAINDER;
                deviceSelectionPanel.add(warningLabel, constraints);
                JPanel buttonPanel = new JPanel(new GridBagLayout());
                constraints = generateConstraints(GridBagConstraints.NONE, x++, 0, 0, 0);
                constraints.anchor = GridBagConstraints.CENTER;
                buttonPanel.add(okButton, constraints);
                constraints = new GridBagConstraints();
                constraints = generateConstraints(GridBagConstraints.NONE, x++, 0, 0, 0);
                constraints.fill = GridBagConstraints.NONE;
                constraints.anchor = GridBagConstraints.CENTER;
                constraints.insets = new Insets(0, 10, 0, 0);
                buttonPanel.add(cancelButton, constraints);
                x = 0;
                constraints = new GridBagConstraints();
                constraints = generateConstraints(GridBagConstraints.HORIZONTAL, x, y++, 1, 0);
                constraints.gridwidth = GridBagConstraints.REMAINDER;
                constraints.insets = new Insets(5, 0, 0, 0);
                deviceSelectionPanel.add(buttonPanel, constraints);
                deviceSelectionDialog.setContentPane(deviceSelectionPanel);
                deviceSelectionDialog.pack();
                deviceSelectionDialog.setLocationRelativeTo(null);
                deviceSelectionDialog.setVisible(true);
            } // end if ((devices == null) || (devices.length == 0)) ... else if ... else
        } // end if (pilotSource.getData().isEmpty() || prepareSource.getData().isEmpty()
          // || pilotSource.getData().equalsIgnoreCase(prepareSource.getData()))

        Capoeira application = new Capoeira();
        application.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        application.getSplash().setVisible(true);

        SwingUtilities.invokeLater(() -> {
            try {
                final CapoeiraModel model = new CapoeiraModel();
                model.setDiffractometer(pilotSource.getData());
                application.setModel(model);
                application.getHklPanel().setModel(prepareSource.getData());
                application.getControlBean().start();
                application.getHklPanel().start();
                application.setVisible(true);
                if (ContextModel.getInstance().isVirtual()) {
                    application.getApplicationTabbedPane().setTitleAt(1, ContextModel.VIRTUAL_LABEL);
                }
            } catch (final DevFailed e) {
                application.getSplash().setVisible(false);
                final String error = "Capoeira error at application start:\n" + TangoExceptionHelper.getErrorMessage(e);
                LOGGER.error(error);
                JOptionPane.showMessageDialog(null, error, Capoeira.class.getSimpleName() + " Error!",
                        JOptionPane.ERROR_MESSAGE);
                System.exit(1);
            }
        });
    } // end main

}
