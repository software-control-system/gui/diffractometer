package fr.soleil.gui.capoeira.log;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableModel;

import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.SearchPredicate;

/**
 * @deprecated This class is never used to trace errors.
 *             A common logging UI should be used instead, as proposed in JAVAAPI-597.
 */
@Deprecated
public class ErrorPanel extends JPanel {

    private static final long serialVersionUID = 1405260417033621196L;

    private final JXTable jxtable;
    private final JButton clear;

    public ErrorPanel() {
        super(new BorderLayout());
        clear = new JButton("Clear traces");
        clear.addActionListener(e -> {
            clearTraces();
        });
        final Dimension sizeButton = new Dimension(20, 20);
        clear.setMinimumSize(sizeButton);
        clear.setMaximumSize(sizeButton);
        clear.setPreferredSize(sizeButton);
        jxtable = new JXTable(new DefaultTableModel(new String[] { "logs" }, 0));
        jxtable.setEditable(false);
        jxtable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

        jxtable.getModel().addTableModelListener(e -> {
            if (e.getType() == TableModelEvent.INSERT) {
                SwingUtilities.invokeLater(() -> {
                    scrollRectToVisible(jxtable.getCellRect(e.getLastRow(), 0, false));
                });
            }
        });
        final ColorHighlighter errorHighlight = new ColorHighlighter(HighlightPredicate.ALWAYS, Color.WHITE, null);
        final HighlightPredicate error = new SearchPredicate(Pattern.compile("ERROR"));
        errorHighlight.setHighlightPredicate(error);

        jxtable.addHighlighter(errorHighlight);
        final JScrollPane scrollPane = new JScrollPane(jxtable);
        scrollPane.setAutoscrolls(true);

        add(clear, BorderLayout.NORTH);
        add(scrollPane, BorderLayout.CENTER);
    }

    public void addTrace(final String trace) {
        final Pattern patternLogFilter = Pattern.compile("ERROR", Pattern.CASE_INSENSITIVE);
        final Matcher matcher = patternLogFilter.matcher(trace);
        if (matcher.find()) {
            ((DefaultTableModel) jxtable.getModel()).addRow(new Object[] { trace });

        }
        SwingUtilities.invokeLater(() -> {
            jxtable.scrollRectToVisible(jxtable.getCellRect(jxtable.getRowCount() - 1, jxtable.getColumnCount(), true));
        });
    }

    public void clearTraces() {
        final Vector<String> vector = new Vector<>();
        vector.add("logs");
        ((DefaultTableModel) jxtable.getModel()).setDataVector(new Vector<String>(), vector);
    }
}
