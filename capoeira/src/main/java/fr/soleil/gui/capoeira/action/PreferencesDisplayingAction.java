/*
 * Created on 10 juin 2005
 * with Eclipse
 */
package fr.soleil.gui.capoeira.action;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.bean.diffractometer.gui.DiffractometerBean;
import fr.soleil.gui.capoeira.ControlBean;
import fr.soleil.gui.capoeira.preferences.PreferencesFrame;

public class PreferencesDisplayingAction extends AbstractAction {

    private static final long serialVersionUID = -679081875418874532L;

    private static final Logger LOGGER = LoggerFactory.getLogger(PreferencesDisplayingAction.class);

    private final ControlBean controlBean;
    private final DiffractometerBean diff4cJPanel;

    public PreferencesDisplayingAction(final ControlBean controlBean, final DiffractometerBean diff4cJPanel) {
        super();
        this.diff4cJPanel = diff4cJPanel;
        this.controlBean = controlBean;
        // This is an instance initializer; it is executed just after the
        // constructor of the superclass is invoked

        // The following values are completely optional
        putValue(Action.NAME, "Preferences");
        // Set tool tip text
        putValue(Action.SHORT_DESCRIPTION, "Show Preferences");

        // This text is not directly used by any Swing component;
        // however, this text could be used in a help system
        putValue(Action.LONG_DESCRIPTION, "Show Preferences");

        // Set an icon
        // Icon icon = new ImageIcon("icon.gif");
        // putValue(Action.SMALL_ICON, icon);

        // Set a mnemonic character. In most look and feels, this causes the
        // specified character to be underlined This indicates that if the
        // component
        // using this action has the focus and In some look and feels, this
        // causes
        // the specified character in the label to be underlined and
        putValue(Action.MNEMONIC_KEY, new Integer(KeyEvent.VK_P));

        // Set an accelerator key; this value is used by menu items
        putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("alt enter"));

    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        try {
            new PreferencesFrame(controlBean, diff4cJPanel).setVisible(true);
        } catch (final Exception e1) {
            LOGGER.error(e1.getMessage());
        }
    }
}