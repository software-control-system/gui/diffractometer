package fr.soleil.gui.capoeira.preferences;

import java.awt.Component;

/**
 * Preference interface
 * 
 * @author HARDION
 * 
 */
public interface Preference {

    public String getTitle();

    public String getIconUrl();

    public Component getComponent();

    public String getIconTitle();
}
