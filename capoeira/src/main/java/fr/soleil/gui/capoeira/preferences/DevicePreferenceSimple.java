package fr.soleil.gui.capoeira.preferences;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.util.Hashtable;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.soleil.bean.diffractometer.gui.DiffractometerBean;
import fr.soleil.gui.capoeira.ControlBean;
import fr.soleil.gui.capoeira.model.CapoeiraModel;
import fr.soleil.gui.capoeira.util.CapoeiraConstants;

/**
 * 
 * @author HARDION
 * 
 */
public class DevicePreferenceSimple extends JPanel implements Preference, CapoeiraConstants {

    private static final long serialVersionUID = -1520635687047102290L;

    private static final Logger LOGGER = LoggerFactory.getLogger(DevicePreferenceSimple.class);

    private CapoeiraModel model;
    protected JTableX table;
    private final TableModel tableModel;
    private ControlBean controlBean;
    private DiffractometerBean diff4cJPanel;

    public DevicePreferenceSimple(final ControlBean controlBean, final DiffractometerBean diff4cJPanel) {
        super();
        this.controlBean = controlBean;
        this.diff4cJPanel = diff4cJPanel;
        model = controlBean.getLPSModel();
        setLayout(new BorderLayout());
        tableModel = new TableModel();
        table = new JTableX(tableModel);
        table.setRowSelectionAllowed(false);
        table.setColumnSelectionAllowed(false);
        table.setValueAt(model.getDiffractometer(), 0, 1);

        tableModel.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(final TableModelEvent e) {
                final TableModel tempModel = (TableModel) e.getSource();
                final String name = (String) (tempModel).getValueAt(e.getFirstRow(), TableModel.SALSA_MODEL);

                if (DIFFRACTOMETER.equalsIgnoreCase(name)) {
                    try {
                        getModel().setDiffractometer(
                                (String) tempModel.getValueAt(e.getFirstRow(), TableModel.CHOOSE_DEVICE));
                        getControlBean().setLPSModel(getControlBean().getLPSModel());
                        getDiff4cJPanel()
                                .setModel((String) tempModel.getValueAt(e.getLastRow(), TableModel.CHOOSE_DEVICE));

                    } catch (final DevFailed e1) {
                        LOGGER.error(DevFailedUtils.toString(e1));
                    }
                }
            }
        });

        final RowEditorModel rowModel = new RowEditorModel();
        table.setRowEditorModel(rowModel);
        final JScrollPane jsp = new JScrollPane(table);
        final Dimension preferredSize = new Dimension(350, 200);
        jsp.setPreferredSize(preferredSize);
        add(jsp, BorderLayout.CENTER);
    }

    public void validateTable() {
        if (table != null && table.getCellEditor() != null)
            table.getCellEditor().stopCellEditing();
    }

    /**
     * 
     * @return ControlBean
     */
    public ControlBean getControlBean() {
        return controlBean;
    }

    /**
     * 
     * @param bean
     */
    public void setControlBean(final ControlBean bean) {
        this.controlBean = bean;
        this.model = bean.getLPSModel();
    }

    /**
     * 
     * @return Diff4cJPanel
     */
    public DiffractometerBean getDiff4cJPanel() {
        return diff4cJPanel;
    }

    /**
     * 
     * @param bean
     */
    public void setDiff4cJPanel(final DiffractometerBean bean) {
        this.diff4cJPanel = bean;
    }

    /**
     * @return Returns the model.
     */
    public CapoeiraModel getModel() {
        return model;
    }

    /**
     * @param model
     *            The model to set.
     */
    public void setModel(final CapoeiraModel model) {
        this.model = model;
    }

    @Override
    public Component getComponent() {
        return this;
    }

    @Override
    public String getIconTitle() {
        return "Devices";
    }

    @Override
    public String getIconUrl() {
        return "icons/devices32x32.png";
    }

    @Override
    public String getTitle() {
        return "Devices Preferences";
    }
}

class TableModel extends AbstractTableModel {

    private static final long serialVersionUID = 8856220603500836775L;

    public boolean initGetValue = true;
    public static final int SALSA_MODEL = 0;

    public static final int CHOOSE_DEVICE = 1;

    public final static String[] columnNames = { "Diffractometer Model", "Choose Device" };

    public Object[][] values = { { "Diffractometer PILOT", "" }, { "Diffractometer PREPARE", "" } };

    // FIN ALIKE GROUP EBO

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public int getRowCount() {
        return values.length;
    }

    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {

        return values[rowIndex][columnIndex];
    }

    @Override
    public void setValueAt(final Object aValue, final int r, final int c) {
        values[r][c] = aValue;
        fireTableCellUpdated(r, c);
    }

    @Override
    public String getColumnName(final int column) {
        return columnNames[column];
    }

    @Override
    public boolean isCellEditable(final int r, final int c) {
        return c == CHOOSE_DEVICE;
    }
}

class RowEditorModel {
    private final Hashtable<Integer, TableCellEditor> data;

    public RowEditorModel() {
        data = new Hashtable<>();
    }

    public void addEditorForRow(final int row, final TableCellEditor e) {
        data.put(Integer.valueOf(row), e);
    }

    public void removeEditorForRow(final int row) {
        data.remove(Integer.valueOf(row));
    }

    public TableCellEditor getEditor(final int row) {
        return data.get(Integer.valueOf(row));
    }
}

class JTableX extends JTable {

    private static final long serialVersionUID = -1514296739163569129L;

    protected RowEditorModel rowModel;

    public JTableX(final TableModel tableModel) {
        super(tableModel);
    }

    public void setRowEditorModel(final RowEditorModel rowModel) {
        this.rowModel = rowModel;
    }

    public RowEditorModel getRowEditorModel() {
        return rowModel;
    }

    @Override
    public TableCellEditor getCellEditor(final int row, final int col) {
        TableCellEditor tmpEditor = null;
        if (rowModel != null) {
            tmpEditor = rowModel.getEditor(row);
        }
        if (tmpEditor != null) {
            return tmpEditor;
        } else {
            return super.getCellEditor(row, col);
        }
    }
}
