package fr.soleil.gui.capoeira.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.StringTokenizer;
import java.util.prefs.Preferences;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevError;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.ErrSeverity;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.DeviceProxyFactory;
import fr.soleil.gui.capoeira.Capoeira;
import fr.soleil.lib.project.ObjectUtils;

public class CapoeiraModel {

    public static final String DIFFRACTOMETER_DEVICE = "DiffractometerDevice";
    public static final String DIFFRACTOMETER_PREPARE_DEVICE = "DiffractometerPrepareDevice";
    public static final String COMPTER_DEVICE = "CompterDevice";

    private static final String ATTRIBUTE_DIFFRACTOMETER_TYPE = "DiffractometerType";
    private static final String PROPERTY_REAL_AXIS_PROXIES = "RealAxisProxies";
    private static final String PROPERTY_PSEUDO_AXIS_PROXIES = "PseudoAxisProxies";
    private static final String PROPERTY_EULERIAN_PSEUDO_AXIS_PROXIES = "GUI_EulerianPseudoAxisProxies";
    private static final String PROPERTY_ANALYZER_AXIS_PROXIES = "GUI_AnalyzerAxisProxies";

    private static final Logger LOGGER = LoggerFactory.getLogger(CapoeiraModel.class);

    private Preferences prefs;

    // Device name in PILOT mode
    private String diffractometer;

    private double gap;

    // ??
    private String[][] realAxis;

    // KAPPA kappa, komega, kphi, mu
    private String[][] kappaRealAxis;

    // DETECTOR: gamma, delta
    private String[][] detectorRealAxis;

    // PSEUDO: psi, q, H, K, L
    private String[][] pseudoAxis;

    // EULERIAN: omega, chi, phi
    private String[][] eulerianPseudoAxis;

    // ANALYZER: thetaa, detaa, etaa
    private String[][] analyzerAxis;

    private String type;

    private String url;

    public CapoeiraModel() {
        super();
        prefs = Preferences.userNodeForPackage(Capoeira.class);
        diffractometer = ObjectUtils.EMPTY_STRING;
        gap = Double.NaN;
        type = ObjectUtils.EMPTY_STRING;
        cleanAxis();
    }

    protected void cleanAxis() {
        realAxis = kappaRealAxis = detectorRealAxis = pseudoAxis = eulerianPseudoAxis = analyzerAxis = null;
    }

    /**
     * @return Returns the diffractometer.
     */
    public String getDiffractometer() {
        return diffractometer;
    }

    /**
     * @param diffractometer The diffractometer to set.
     * @throws ConnectionException
     * @throws DevFailed
     */
    public void setDiffractometer(final String diffractometer) throws DevFailed {
        String effectiveDiffracto = diffractometer == null ? ObjectUtils.EMPTY_STRING : diffractometer;
        if (!this.diffractometer.equals(effectiveDiffracto)) {
            if (!this.diffractometer.isEmpty()) {
                // Clean axis
                cleanAxis();
            }

            this.diffractometer = effectiveDiffracto;

            if (!this.diffractometer.isEmpty()) {
                // Registering listener from model
                // try {
                final DeviceProxy diffracto = DeviceProxyFactory.get(this.diffractometer);
                // set the type of diffractometer to control (== class)
                // fix #7071

                DeviceAttribute attr = diffracto.read_attribute(ATTRIBUTE_DIFFRACTOMETER_TYPE);

                if (attr != null) {
                    String str = attr.extractString();
                    if (str != null) {
                        type = str;
                    }
                }
                // try to retrieve motor proxies by the property of
                // Diffractometer device
                DbDatum datum = diffracto.get_property(PROPERTY_REAL_AXIS_PROXIES);
                if (datum != null && !datum.is_empty()) {
                    Collection<String> kappaList = new ArrayList<>();
                    Collection<String> detectorList = new ArrayList<>();
                    final String[] realAxisProxies = datum.extractStringArray();
                    realAxis = new String[realAxisProxies.length][2];
                    LOGGER.debug("realAxis: " + Arrays.toString(realAxis));
                    for (int i = 0; i < realAxisProxies.length; i++) {
                        final String lower = realAxisProxies[i].toLowerCase();
                        if (lower.startsWith("delta") || lower.startsWith("gamma")) {
                            detectorList.add(realAxisProxies[i]);
                        } else {
                            kappaList.add(realAxisProxies[i]);
                        }
                        final StringTokenizer tokens = new StringTokenizer(realAxisProxies[i], ":");
                        if (!tokens.hasMoreTokens()) {
                            final DevError[] errors = new DevError[] {
                                    new DevError("Undefined property RealAxisProxies", ErrSeverity.ERR,
                                            "The property \"RealAxisProxies is undefined on the diffractometer device",
                                            "LPSModel") };
                            throw new DevFailed(errors);
                        }
                        realAxis[i][0] = tokens.nextToken();
                        realAxis[i][1] = tokens.nextToken();
                    }
                    kappaRealAxis = new String[kappaList.size()][2];
                    int i = 0;
                    for (String kappa : kappaList) {
                        final StringTokenizer tokens = new StringTokenizer(kappa, ":");
                        kappaRealAxis[i][0] = tokens.nextToken();
                        kappaRealAxis[i][1] = tokens.nextToken();
                    }
                    kappaList.clear();
                    kappaList = null;
                    i = 0;
                    detectorRealAxis = new String[detectorList.size()][2];
                    for (String detector : detectorList) {
                        final StringTokenizer tokens = new StringTokenizer(detector, ":");
                        detectorRealAxis[i][0] = tokens.nextToken();
                        detectorRealAxis[i][1] = tokens.nextToken();
                    }
                    detectorList.clear();
                    detectorList = null;
                }
                LOGGER.debug("realAxis: " + Arrays.deepToString(realAxis));

                datum = diffracto.get_property(PROPERTY_PSEUDO_AXIS_PROXIES);
                if (datum != null && !datum.is_empty()) {
                    final String[] pseudoAxisProxies = datum.extractStringArray();
                    pseudoAxis = new String[pseudoAxisProxies.length][2];
                    LOGGER.debug("pseudoAxis: " + Arrays.toString(pseudoAxis));
                    for (int i = 0; i < pseudoAxisProxies.length; i++) {
                        final StringTokenizer tokens = new StringTokenizer(pseudoAxisProxies[i], ":");
                        pseudoAxis[i][0] = tokens.nextToken();
                        pseudoAxis[i][1] = tokens.nextToken();
                    }
                }
                LOGGER.debug("pseudoAxis: " + Arrays.deepToString(pseudoAxis));

                datum = diffracto.get_property(PROPERTY_EULERIAN_PSEUDO_AXIS_PROXIES);
                if (datum != null && !datum.is_empty()) {
                    final String[] eulerianPseudoAxisProxies = datum.extractStringArray();
                    eulerianPseudoAxis = new String[eulerianPseudoAxisProxies.length][2];
                    LOGGER.debug("eulerianPseudoAxis: " + Arrays.toString(eulerianPseudoAxisProxies));
                    for (int i = 0; i < eulerianPseudoAxisProxies.length; i++) {
                        final StringTokenizer tokens = new StringTokenizer(eulerianPseudoAxisProxies[i], ":");
                        eulerianPseudoAxis[i][0] = tokens.nextToken();
                        eulerianPseudoAxis[i][1] = tokens.nextToken();
                    }
                }
                LOGGER.debug("eulerianPseudoAxis: " + Arrays.deepToString(eulerianPseudoAxis));

                datum = diffracto.get_property(PROPERTY_ANALYZER_AXIS_PROXIES);
                if (datum != null && !datum.is_empty()) {
                    final String[] analyzerAxisProxies = datum.extractStringArray();
                    analyzerAxis = new String[analyzerAxisProxies.length][2];
                    LOGGER.debug("analyzerAxisProxies: " + Arrays.toString(analyzerAxisProxies));
                    for (int i = 0; i < analyzerAxisProxies.length; i++) {
                        final StringTokenizer tokens = new StringTokenizer(analyzerAxisProxies[i], ":");
                        analyzerAxis[i][0] = tokens.nextToken();
                        analyzerAxis[i][1] = tokens.nextToken();
                    }
                }
                LOGGER.debug("analyzerAxis: " + Arrays.deepToString(analyzerAxis));
            }
        }
    }

    public void loadDevicesPreferences() throws DevFailed {
        setDiffractometer(prefs.get(DIFFRACTOMETER_DEVICE, "not initialized"));
        setDiffractometer(prefs.get(DIFFRACTOMETER_PREPARE_DEVICE, "not initialized"));
    }

    /**
     * @return the realAxis
     */
    public String[][] getRealAxis() {
        return realAxis;
    }

    /**
     * @param realAxis
     *            the realAxis to set
     */
    public void setRealAxis(final String[][] proxies) {
        this.realAxis = proxies;
    }

    /**
     * @return the pseudoAxis
     */
    public String[][] getPseudoAxis() {
        return pseudoAxis;
    }

    /**
     * @param pseudoAxis
     *            the pseudoAxis to set
     */
    public void setPseudoAxis(final String[][] pseudoAxis) {
        this.pseudoAxis = pseudoAxis;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @return the eulerianPseudoAxis
     */
    public String[][] getEulerianPseudoAxis() {
        return eulerianPseudoAxis;
    }

    /**
     * @return the analyzerAxis
     */
    public String[][] getAnalyzerAxis() {
        return analyzerAxis;
    }

    /**
     * @return the kappaRealAxis
     */
    public String[][] getKappaRealAxis() {
        return kappaRealAxis;
    }

    /**
     * @return the detectorRealAxis
     */
    public String[][] getDetectorRealAxis() {
        return detectorRealAxis;
    }

    public double getGap() {
        return gap;
    }

    public void setGap(final double gap) {
        this.gap = gap;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

}
