package fr.soleil.gui.capoeira.preferences;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.bean.diffractometer.gui.DiffractometerBean;
import fr.soleil.comete.swing.AutoScrolledTextField;
import fr.soleil.comete.swing.Label;
import fr.soleil.gui.capoeira.ControlBean;

/**
 * 
 * @author HARDION
 * 
 */
public class PreferencesFrame extends JDialog {

    private static final long serialVersionUID = 3024311294489483887L;

    private static final Logger LOGGER = LoggerFactory.getLogger(PreferencesFrame.class);

    private ControlBean controlBean = null;
    private DiffractometerBean diff4cJPanel = null;
    private JPanel prefsPanel = null;
    // private DevicePreferenceSimple dp;

    /**
     * This is the default constructor
     */
    public PreferencesFrame() throws Exception {
        super();
        initialize();
    }

    /**
     * 
     * @param bean
     * @throws Exception
     */
    public PreferencesFrame(final ControlBean controlBean, final DiffractometerBean Diff4cJPanel) throws Exception {
        super();

        initialize();
        setModal(true);
        setBean(controlBean, Diff4cJPanel);
        setReflectionsPreferencePanel();
        setUrlHelpPanel();
        setCloseButton();
        pack();
    }

    private void setCloseButton() {
        final JPanel p = new JPanel();
        final JButton closeButton = new JButton("Close");
        closeButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                dispose();
            }
        });
        p.add(closeButton);
        getContentPane().add(p);
    }

    /**
     * This method initializes this
     * 
     * @return void
     * @throws UnsupportedLookAndFeelException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws ClassNotFoundException
     */
    private void initialize() throws ClassNotFoundException, InstantiationException, IllegalAccessException,
            UnsupportedLookAndFeelException {
        setTitle("Preferences");
        getContentPane().setLayout(null);
        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        prefsPanel = new JPanel();
        getContentPane().add(prefsPanel);
    }

    private void setReflectionsPreferencePanel() {
        final JPanel refPanel = new JPanel();
        refPanel.setBorder(BorderFactory.createTitledBorder("Reflection preference"));

        final Label gapLabel = new Label();
        gapLabel.setText("Default exceeded gap : ");
        refPanel.add(gapLabel, BorderLayout.CENTER);
        final AutoScrolledTextField gapTextField = new AutoScrolledTextField();
        gapTextField.setColumns(10);
        gapTextField.setText(Double.toString(getControlBean().getLPSModel().getGap()));
        refPanel.add(gapTextField, BorderLayout.CENTER);
        gapTextField.getDocument().addDocumentListener(new DocumentListener() {

            private void updateData() {
                double gapValue = Double.NaN;
                try {
                    final String gapText = gapTextField.getText().trim();
                    if (!gapText.isEmpty()) {
                        gapValue = Double.parseDouble(gapText);

                    }
                    getControlBean().getLPSModel().setGap(gapValue);
                    getDiff4cJPanel().setGapReflexion(gapValue);
                } catch (final NumberFormatException e) {
                    LOGGER.error(e.getMessage());
                    return;
                }
            }

            @Override
            public void removeUpdate(final DocumentEvent e) {
                updateData();
            }

            @Override
            public void insertUpdate(final DocumentEvent e) {
                updateData();
            }

            @Override
            public void changedUpdate(final DocumentEvent e) {
                // NOT USE
            }
        });
        getContentPane().add(refPanel);
    }

    private void setUrlHelpPanel() {
        final JPanel p = new JPanel();
        p.setBorder(BorderFactory.createTitledBorder("Documentation preference"));
        final Label docTextField = new Label();
        docTextField.setText("Documentation's url: ");
        p.add(docTextField, BorderLayout.WEST);
        final JTextField urlTextField = new JTextField(50);
        urlTextField.setText(getControlBean().getLPSModel().getUrl());
        p.add(urlTextField, BorderLayout.CENTER);
        urlTextField.getDocument().addDocumentListener(new DocumentListener() {
            private void updateData() {
                final String url = urlTextField.getText().trim();
                getControlBean().getLPSModel().setUrl(url);
            }

            @Override
            public void removeUpdate(final DocumentEvent e) {
                updateData();
            }

            @Override
            public void insertUpdate(final DocumentEvent e) {
                updateData();
            }

            @Override
            public void changedUpdate(final DocumentEvent e) {
                // NOT USE

            }
        });
        getContentPane().add(p);
    }

    public void setBean(final ControlBean controlBean, final DiffractometerBean diff4cJPanel) {
        this.controlBean = controlBean;
        this.diff4cJPanel = diff4cJPanel;
        // dp = new DevicePreferenceSimple(controlBean, diff4cJPanel);
        // dp.setModel(controlBean.getLPSModel());
        // prefsPanel.add(dp);

    }

    /**
     * @return Returns the model.
     */
    public ControlBean getControlBean() {
        return controlBean;
    }

    /**
     * @param controlBean
     *            The model to set.
     */
    public void setControlBean(final ControlBean controlBean) {
        this.controlBean = controlBean;
    }

    /**
     * 
     * @return Diff4cJPanel
     */
    public DiffractometerBean getDiff4cJPanel() {
        return diff4cJPanel;
    }

    /**
     * 
     * @param bean
     */
    public void setDiff4cJPanel(final DiffractometerBean bean) {
        this.diff4cJPanel = bean;
    }
}
