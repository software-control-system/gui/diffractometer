package fr.soleil.gui.capoeira.util;

public interface CapoeiraConstants {

    public static final String DIFFRACTOMETER = "Diffractometer";
    public static final String DIFFRACTOMETER_DEVICE = "DiffractometerDevice";
    public static final String DIFFRACTOMETER_PREPARE_DEVICE = "DiffractometerPrepareDevice";

}
