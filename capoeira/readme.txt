
 __       ____         _______.
|  |     |   _ `      /       |
|  |     |  |_)  )   |   (----`
|  |     |   __ ,     \   \    
|  `----.|  |     .----)   |   
|_______||__|     |_______/    
                                                              
01/03/2006	Developped by ICA TEAM


DESCRIPTION
-----------
This application is a front end for Lps control & acquisition

DEPENDENCES
-----------
This source code release is based on :
	* JDK 1.4.2
	* ATKCore-2.1.24.jar
	* ATKWidget-2.1.24.jar
	* TangORB-4.5.0.jar
	* Diff4cBean.jar
	* UserMotorBean.jar
    * OrtecBean.jar
    * Salsa-2.0.jar

The application has dependance with :
	* ds_Galilbox 1.0
	* ds_Ortec 1.0
	* ds_DiffractometerEulerian4Circles 2.0
	
PLATEFORM
---------
Any operating system where run a JVM.

WHAT'S INSIDE
-------------

SOURCE : /src
	* source files

DOC : /doc
	TODO
	
	
PREREQUISITES
-------------

Before compiling and installing this application you need to install :
	* see the prerequisites of tango distribution
	* JRE 1.4.2 or more

MAKE
----
TODO ant compilation

INSTALLING
----------
No installation needed.

CONFIGURING
-----------
The devices can be changed into script file.

RUNNING
-------
Windows :
	!!!  not from network (ie "\\...") !!!
	* Click on start-salsa.bat
Linux :
	* Launch start-salsa.sh with or without devices address

QUESTIONS
---------

You will definitely have some ! Send questions to :
	vincent.hardion@synchrotron-soleil.fr

URL
---



CHANGES
-------
	* see ReleaseNotes.txt
