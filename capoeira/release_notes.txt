release 4.1.3:

- bug#0022940 
Capoeira essaira d'utiliser l'alias du device d�finit via Jive, si il n'existe pas il utilisera le nom du device par d�fault

- bug#0022959 
Suppression du panel Counter
Suppression des pr�f�rences et du rafraichissement li�
Modification du main pour supprimer l'argument counter

- bug#0015060
Ajout des min/max pour la position moteurs, aussi bien pour les devices simul� que pour les attributs dans le diffacto
[UPDATE] D�sormais il n'est plus possible de modifier la valeur min/max

- bug#0022939
Ajout d'un champs dans la fen�tre des pr�f�rences pour param�tr� l'url de documentation
Ajout d'un menu item "Documentation" dans l'onglet "Help" qui permet d'ouvrir la doc dans le navigateur WEB par d�fault

- bug#0022938
Modification du format d'affichage des double dans la table ReflectionList pour qu'il corresponde � : 4 chiffres avantt la virgule et 4 chiffres apr�s

 - bug#0022963
Permet de modifier la visibilit� des colonnes dans la table Reflection list via l'icone en haut � droite de la table

 - bug#0013273
 Suppression de tout ce qui concerne "maquette 3D" ainsi que l'anticollision
 
 - bug#0019025
 Le add pour ajouter des refl�ctions prend maintenant les valeurs du diffracto peut importe le mode dans lequel il se trouve.
 
  - bug#0022961
 Le bouton "ModeInit" devient orange quand on change le mode pour signaler qu'il faut activer le mode en cliquant sur le bouton
 

 - bug#0016848
 Ajout d'une gestion d'erreur sous forme de pop up si une propri�t� est manquante
 
  - bug#0022991 && 0022991
correction d'un bug lors du changement du device diffractom�tre dans l'application, d�sormais elle d�construit tout ce qui concernait l'ancien device 

 -bug#0023082
 R�ajustement de la taille dans l'onglet control afin d'�viter que des composants soit �cras�s
 
 - bug#0022941
 La barre de s�paration ne se fige plus dans l'onglet hkl
 
 - bug#0014464
 Modification du bean "GoToPosition" afin d'�claircir la panel et d'indiquer si un moteur est arriv� � la position voulu ou pas
 Ajout d'une l�gende
 
 - bug#0022994
 Le probl�me de raffraichissement de la table de r�fl�ctions est PARTIELLEMENT r�gl�. Ajout d'un sleep pour que les modifications soient prise en compte par le device avant de relire les valeurs dessus
 
 - bug#0023142
 D�sormais la prtie lecture de l'attribut est pris en compte au lieu de prendre la prtie �criture qui n'�tait pas toujours initialis�.
 
  - bug#0023141
  D�sormais le bouton apply devient orange si on change une ou plusieurs valeur(s). Cependant m�me si on remet la m�me valeur que celle pr�c�demment entr�e, le bouton devient orange quand m�me
  
  - bug#0023143
  R��criture de UserMotorBean afin de prendre en compte les XPSGroup ("Hack")
  
  - bug#0023140
  La vue controle ne prenait pas en compte tous les diffractom�tre, ajout d'un cas par d�faut
  
  - bug #0023197
  La colonne "sim write" est d�sormais �ditable est le fait de valider l'entr� correspond � faire un "Apply"
 
 
 
 